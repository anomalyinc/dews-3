<?php get_header();  ?>

<?php if(is_home()): 
$args = array('category_name' => 'topicsoftopics','posts_per_page' => 4,'post_type' => array('post','movie','event'));
$my_query = new WP_Query($args); if ($my_query->have_posts()) : 
?>

    <div class="swiper-container">
        <div class="swiper-wrapper">
		<?php while($my_query->have_posts()): $my_query->the_post();?>
            <div class="swiper-slide" >
                <div class="content-inner" >
                  <div class="main-post entries">
                      <div class="imageWrapper">
                      <a href="<?php the_permalink() ?>">
                      <span class="rank-number"><?php if(in_category('pr')): $category = get_the_category(); ?>PR<?php else : ?><?php endif; ?></span>
                          <div class="entries--title-wrapper">
                              <h3 class="entries--title"><?php the_title(); ?></h3>
                          </div>
                          <div class="image">
                              <?php the_post_thumbnail("full"); ?>
                          </div>
                      </a>
                      </div>
                  </div>
                </div>
            </div>  
		<?php endwhile; ?>
        </div>  
       <!--  <div class="swiper-pagination"></div> -->
    </div>
<?php endif; wp_reset_postdata(); endif; ?>
<?php if(is_home()): 
$args = array( 'posts_per_page' => 3 ,'post_type' => array('post','movie','event','music','fashion','column') ,'category_name' => 'special');
$my_query = new WP_Query($args); if ($my_query->have_posts()) : 
?>
<div id="special">
	<div class="entries">
		<h2 class="section--title">PICK UP<br><span>イチオシの特集記事</span></h2>
		<ul class="sp-grid">
		<?php while($my_query->have_posts()): $my_query->the_post();?>
			<li class="sp-grid__item">
				<a href="<?php the_permalink() ?>">
					<div class="sp-imageWrapper">
					<span class="rank-number"><?php if(in_category('pr')): $category = get_the_category(); ?>PR<?php else : ?>特集<?php endif; ?></span>
					<div class="image"><?php the_post_thumbnail("medium"); ?></div></div>
					<div class="sp-text"><p><?php the_title(); ?></p></div>
				</a>
			</li>
		

		<?php endwhile; ?>
			<li class="sp-grid__item">
				<a href="http://dews365.com/news/gzeye-ariya.html" onclick="ga('send','event','top','click','gseye');">
					<div class="sp-imageWrapper">
						<div class="image">
							<img src="https://dews365.com/wp-content/uploads/2018/03/be71e80f9b05d492ead46ca9aedc75e1.jpg" alt="" onload="ga('send','event','top','imp','gseye');">
						</div>
						<div class="sp-text"><p>[PR]最高にタフなカメラ「G’z EYE」。世界で活躍するBBOYクルーが試すとこうなる</p></div>
					</div>
				</a>
			</li>
		</ul>
	</div>
<!-- 	<div class="pickup-banner">
	<a href="http://dews365.com/category/jk" title=""><img src="<?php echo get_template_directory_uri(); ?>/images/bn_jkpost.gif" alt=""></a>
</div> -->
</div>


<?php endif; wp_reset_postdata(); endif; ?>

		<?php 
			$paged = get_query_var('paged');
			global $mainNotin;
			$args = array( 
				'post_type' => array('post','movie','music','fashionsnap','event','column','audition','dancersgram'), 
				'posts_per_page' => 15 ,
				'post__not_in' => $mainNotin ,
				'category__in' => array(9,12,16,117,13,15,14,9,8),
				'paged' => $paged
				);
			$my_query_main = new WP_Query($args); if ($my_query_main->have_posts()) :

		 ?>

<div id="maincol">
	<div class="content-inner">
		<section class="entries">
			<h2 class="section--title">TOPICS<br><span>新着記事</span></h2>
			<ul class="entries--list">
				<?php 
					while($my_query_main->have_posts()): $my_query_main->the_post();
					$cats = get_the_category();
					$catNameArray = array();
					$catSlugArray = array();
					foreach($cats as $category ) { 
						if(($category->category_nicename != "news") && ($category->category_nicename != "topics") && ($category->category_nicename != "special")){
							$catNameArray[] .= $category->cat_name;
							$catSlugArray[] .= $category->category_nicename;
						}
					} 
					if(!$catNameArray[0]){
						$catName = "その他";
						$catSlug = "other";
					}else{
						$catName = $catNameArray[0];
						$catSlug = $catSlugArray[0];
					}
				 ?>
				<li>
					<a href="<?php the_permalink(); ?>">
						<div class="imageWrapper"><div class="image"><?php if(has_post_thumbnail()): the_post_thumbnail("medium"); else: echo '<img src="' . get_template_directory_uri().'/images/noimg.jpg" alt="no image">'; endif; ?><span class="cat"><?php echo esc_html($catName); ?></span></div></div>
						<div class="text"><time datetime="<?php the_time("c"); ?>"><?php the_time("Y.m.d"); ?></time><p><?php echo mb_substr($post->post_title, 0, 40).'……'; ?></p></div>
					</a>
				</li>
				<?php endwhile; ?>
			</ul>
		</section>

		<?php endif; wp_reset_postdata(); ?>

	
	</div><!--end content-inner -->
</div><!--end maincol -->

	<!-- RecoCro -->


<?php get_footer(); ?>