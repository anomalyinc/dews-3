<?php /* Template Name: RANKING */ ?>
<?php 
get_header(); 
the_post(); 
$page = get_page(get_the_ID());
$pageslug = $page->post_name;
?>

<div id="maincol">

 
 <div id="tab-container" class="tabContainer">
   <ul class="tab">
    <li class="tab__button"><a href="#tab1">DAILY</a></li>
    <li class="tab__button"><a href="#tab2">WEEKLY</a></li>
    <li class="tab__button"><a href="#tab3">MONTHLY</a></li>
  </ul>
  <div class="panel-box">
    <div id="tab1">
      <div class="ranking">
        <section class="inner entries">
          <h2 class="section--title">DAILY RANKING<br><span>ランキング</span></h2>
          <ul class="grid grid-fill cf">
          <?php 
          $posts = wmp_get_popular( array( 'limit' => 10, 'post_type' => array('post','movie','event','column'), 'range' => 'daily' ) );
          global $post; if ( count( $posts ) > 0 ): foreach ( $posts as $post ): setup_postdata( $post ); 
          ?>
            <li class="grid__item--2 has-gutter">
              <a href="<?php the_permalink(); ?>">
                <div class="imageWrapper"><div class="image"><?php if(has_post_thumbnail()): the_post_thumbnail("medium"); else         : echo '<img src="' . get_template_directory_uri().'/images/noimg.jpg" alt="no image">'; endif; ?></div></div>
                <div class="text"><p><?php the_title(); ?></p></div>
              </a>
            </li>
          <?php endforeach; endif; wp_reset_postdata();  ?>
          </ul>
        </section>
      </div> 
    </div>
    <div id="tab2">
      <div class="ranking">
        <section class="inner entries">
          <h2 class="section--title">WEEKLY RANKING<br><span>週間ランキング</span></h2>
          <ul class="grid grid-fill cf">
          <?php 
          $posts = wmp_get_popular( array( 'limit' => 10, 'post_type' => array('post','movie','event','column'), 'range' => 'weekly' ) );
          global $post; if ( count( $posts ) > 0 ): foreach ( $posts as $post ): setup_postdata( $post ); 
          ?>
            <li class="grid__item--2 has-gutter">
              <a href="<?php the_permalink(); ?>">
                <div class="imageWrapper"><div class="image"><?php if(has_post_thumbnail()): the_post_thumbnail("medium"); else: echo '<img src="' . get_template_directory_uri().'/images/noimg.jpg" alt="no image">'; endif; ?></div></div>
                <div class="text"><p><?php the_title(); ?></p></div>
              </a>
            </li>
          <?php endforeach; endif; wp_reset_postdata();  ?>
          </ul>
        </section>
      </div> 
    </div>
    <div id="tab3">
      <div class="ranking">
        <section class="inner entries">
         <h2 class="section--title">MONTHRY RANKING<br><span>月間ランキング</span></h2>
         <ul class="grid grid-fill cf">
         <?php 
         $posts = wmp_get_popular( array( 'limit' => 10, 'post_type' => array('post','movie','event','column'), 'range' => 'monthly' ) );
         global $post; if ( count( $posts ) > 0 ): foreach ( $posts as $post ): setup_postdata( $post ); 
         ?>
           <li class="grid__item--2 has-gutter">
             <a href="<?php the_permalink(); ?>">
               <div class="imageWrapper"><div class="image"><?php if(has_post_thumbnail()): the_post_thumbnail("medium"); else: echo '<img src="' . get_template_directory_uri().'/images/noimg.jpg" alt="no image">'; endif; ?></div></div>
               <div class="text"><p><?php the_title(); ?></p></div>
             </a>
           </li>
         <?php endforeach; endif; wp_reset_postdata();  ?>
         </ul>
        </section>
      </div> 
    </div>
  </div>
</div>


<?php get_footer(); ?>