<?php /* Template Name: EVENT */ ?>
<?php 
get_header(); 
the_post(); 
$page = get_page(get_the_ID());
$pageslug = $page->post_name;
$currnet_date = date_i18n( 'y.m.d' );
$GENRE = $_GET['GENRE'];
$REGION = $_GET['REGION'];
$AREA = $_GET['AREA'];
$WORD = $_GET['WORD'];

?>

<?php

if(!$GENRE && !$REGION && !$AREA && !$WORD){
query_posts( array(
		'post_type' => 'event', 
		'posts_per_page' => 22 , 
		'paged' => get_query_var( 'paged' ),
		'meta_key'=>'DATE', 
		'orderby'=>'meta_value',
		'order' => 'ASC',
		'tax_query' => array(
			array(
			'taxonomy'=>'eventtype',
			'terms'=> 'eventinfo',
			'field'=>'slug',
			'operator'=>'IN'
			)
		),
		'meta_query' =>array(array(
					'key' => 'DATE',
					'value' => $currnet_date,
					'compare' => '>=',
					'type' => 'DATE'
					))
)); 


$h2 = 'ダンスイベント情報';
$GENRE = array();
$REGION = array();
$AREA = array();
$OPTION = array();

}else{

$h2 = 'イベント検索結果';

$arg = array(
'post_type' => 'event' ,
'posts_per_page' => 22 ,
'meta_key'=>'DATE', 
'orderby'=>'meta_value',
'order' => 'ASC',
'paged' => get_query_var( 'paged' ),
'tax_query' => array( array(
	'taxonomy'=>'eventtype',
	'terms'=> 'eventinfo',
	'field'=>'slug',
	'operator'=>'IN'
)),
'meta_query' =>array(array(
	'key' => 'DATE',
	'value' => $currnet_date,
	'compare' => '>=',
	'type' => 'DATE'
)),
's' => $WORD,
);

if($GENRE){
$arggenre = array('tax_query' => array( array(
	'taxonomy'=>'genre',
	'terms'=> $GENRE,
	'field'=>'slug',
	'operator'=>'IN'
	)));
$arg = array_merge_recursive($arg,$arggenre );
}else{
	$GENRE = array();
};

if($REGION){
$argregion = array('meta_query' => array(
	array(
	'key' => 'REGION',
	'value' => $REGION,
	'compare'=>'IN'
	)
	)
	);
$arg = array_merge_recursive($arg,$argregion );
}else{
	$REGION = array();
};

if($AREA){
$argarea = array('meta_query' => array(
	array(
	'key' => 'AREA',
	'value' => $AREA,
	'compare'=>'IN'
	)
	)
	);
$arg = array_merge_recursive($arg,$argarea );
}else{
	$AREA = array();
};

query_posts( $arg );
	
}
?>

<section>
	<h1 class="section--title"><?php echo esc_html($h2); ?></h1>

	<form role="search" method="get" id="search-bar" class="cf" action="<?php echo home_url(); ?>/eventinformation">
		<div id="searchMap" class="formContent">
			<h3>地域</h3>
			<div class="formContent--wd">
				<div class="kanto">
					<span><input type="checkbox" name="REGION[]" value="関東" class="region"  <?php if (in_array("関東", $REGION)) { echo 'checked="checked"'; } ?> /><label >関東</label></span><br>
					<div class="tokyo">
						<span><input type="checkbox" name="AREA[]" value="東京都" class="region" /><label >東京都</label></span>
						<span><input type="checkbox" name="AREA[]" value="渋谷・原宿" /><label >渋谷・原宿</label></span>
						<span><input type="checkbox" name="AREA[]" value="新宿" /><label >新宿</label></span>
						<span><input type="checkbox" name="AREA[]" value="池袋" /><label >池袋</label></span>
						<span><input type="checkbox" name="AREA[]" value="青山・六本木" /><label >青山・六本木</label></span>
						<span><input type="checkbox" name="AREA[]" value="東京その他" /><label >その他</label></span>
						<span><input type="checkbox" name="AREA[]" value="23区外" /><label >23区外</label></span>
					</div>
					<div class="kanagawa">
						<span><input type="checkbox" name="AREA[]" value="神奈川県"  class="region"  <?php if (in_array("神奈川", $REGION)) { echo 'checked="checked"'; } ?> /><label >神奈川県</label></span><br>
						<span><input type="checkbox" name="AREA[]" value="横浜・川崎" /><label>横浜・川崎</label></span>
						<span><input type="checkbox" name="AREA[]" value="神奈川その他" /><label >その他</label></span>
					</div>
					<span><input type="checkbox" name="AREA[]" value="茨城県" /><label >茨城県</label></span>
					<span><input type="checkbox" name="AREA[]" value="栃木県" /><label >栃木県</label></span>
					<span><input type="checkbox" name="AREA[]" value="群馬県" /><label >群馬県</label></span>
					<span><input type="checkbox" name="AREA[]" value="埼玉県" /><label >埼玉県</label></span>
					<span><input type="checkbox" name="AREA[]" value="千葉県" /><label >千葉県</label></span>
				</div>
				<div>
					<div>
						<span><input type="checkbox" name="REGION[]" value="北海道" class="region" <?php if (in_array("北海道", $AREA)) { echo 'checked="checked"'; } ?> /><label >北海道</label></span>
						<span><input type="checkbox" name="REGION[]" value="東北" class="region"  <?php if (in_array("東北", $REGION)) { echo 'checked="checked"'; } ?> /><label>東北</label></span>
						<span><input type="checkbox" name="REGION[]" value="中部" class="region"  <?php if (in_array("中部", $REGION)) { echo 'checked="checked"'; } ?> /><label >中部</label></span>
						<span><input type="checkbox" name="REGION[]" value="関西" class="region" <?php if (in_array("関西", $REGION)) { echo 'checked="checked"'; } ?>  /><label>関西</label></span>
						<span><input type="checkbox" name="REGION[]" value="中国" class="region" <?php if (in_array("中国", $REGION)) { echo 'checked="checked"'; } ?> /><label>中国</label></span>
						<span><input type="checkbox" name="REGION[]" value="四国" class="region"  <?php if (in_array("四国", $REGION)) { echo 'checked="checked"'; } ?> /><label >四国</label></span>
						<span><input type="checkbox" name="REGION[]" value="九州"  class="region" <?php if (in_array("九州", $REGION)) { echo 'checked="checked"'; } ?> /><label>九州</label></span>
						<span><input type="checkbox" name="AREA[]" value="沖縄県" class="region" <?php if (in_array("沖縄", $REGION)) { echo 'checked="checked"'; } ?> /><label >沖縄県</label></span>
					</div>
				</div>
			</div>
		</div>
		<div id="searchStyle" class="formContent">
			<h3>ジャンル</h3>
			<div class="formContent--wd">
				<span><input type="checkbox" name="GENRE[]" value="contest" <?php if (in_array("contest", $GENRE)) { echo 'checked="checked"'; } ?> ><label>コンテスト</label></span>
				<span><input type="checkbox" name="GENRE[]" value="showcase" <?php if (in_array("showcase", $GENRE)) { echo 'checked="checked"'; } ?> ><label>ショーケース</label></span>
				<span><input type="checkbox" name="GENRE[]" value="battle" <?php if (in_array("battle", $GENRE)) { echo 'checked="checked"'; } ?> ><label>ダンスバトル</label></span>
				<span><input type="checkbox" name="GENRE[]" value="キッズ" <?php if (in_array("キッズ", $GENRE)) { echo 'checked="checked"'; } ?> ><label>キッズ</label></span>
				<span><input type="checkbox" name="GENRE[]" value="girl" <?php if (in_array("girl", $GENRE)) { echo 'checked="checked"'; } ?> ><label>女子限定</label></span>
				<span><input type="checkbox" name="GENRE[]" value="学生限定" <?php if (in_array("学生限定", $GENRE)) { echo 'checked="checked"'; } ?> ><label>学生限定</label></span>
				<span><input type="checkbox" name="GENRE[]" value="HIPHOP" <?php if (in_array("HIPHOP", $GENRE)) { echo 'checked="checked"'; } ?> ><label>HIPHOP</label></span>
				<span><input type="checkbox" name="GENRE[]" value="HOUSE" <?php if (in_array("HOUSE", $GENRE)) { echo 'checked="checked"'; } ?> ><label>HOUSE</label></span>
				<span><input type="checkbox" name="GENRE[]" value="BREAKIN" <?php if (in_array("BREAKIN", $GENRE)) { echo 'checked="checked"'; } ?> ><label>BREAKIN</label></span>
				<span><input type="checkbox" name="GENRE[]" value="FREESTYLE" <?php if (in_array("FREESTYLE", $GENRE)) { echo 'checked="checked"'; } ?> ><label>FREESTYLE</label></span>
				<span><input type="checkbox" name="GENRE[]" value="EDM" <?php if (in_array("EDM", $GENRE)) { echo 'checked="checked"'; } ?> ><label>EDM</label></span>
			</div>
		</div>
		<div id="searchKeyword" class="formContent">
			<input type="text" name="WORD" placeholder="キーワード" />
		</div>

		<div id="searchSubmit"  class="formContent">
			<input type="submit" value="検索する" />
		</div>
	</form>


	<?php 
	$genreImp  = implode(" , ", $GENRE); 
	$regionImp  = implode(" , ", $REGION); 
	$areaImp  = implode(" , ", $AREA); 
	?>
	
	<?php if($GENRE || $REGION || $AREA  || $WORD): ?>

	<div class="search-result">
		<p><span style="font-size:25px;vertical-align:baseline"><?php echo $wp_query->found_posts ?></span> 件のダンスイベントがヒットしました</p>
		<?php if($genreImp){echo '<p class="postmeta mark">GENRE : ' . esc_html($genreImp) .'</p>'; }?>
		<?php if($regionImp || $areaImp):?>
		<div class="postmeta mark">
			<?php if($regionImp){echo '<p> 地区 : ' . esc_html($regionImp).'</p>'; }?>
			<?php if($areaImp){echo '<p> 地域 : ' . esc_html($areaImp).'</p>'; }?>
		</div>
		<?php endif;?>
		<?php if($WORD){echo '<p class="postmeta mark">キーワード : ' . esc_html($WORD).'</p>'; }?>
	</div>
	<?php endif;?>

	<div class="pageBx entries cf">
	<?php 
		$cont = 1 ;
		if(have_posts()): while(have_posts()): the_post(); 
		$slug = $post->post_name;
		$costumf  = get_post_custom( $post->ID );
		$region	= $costumf['REGION'];
		$area	= $costumf['AREA'];
		
		if(is_array($region)){ $region = implode("," , $region); };
		if(is_array($area)){ $area = implode("," , $area); };
		
		$date = post_custom("DATE");
		$t_date = mb_strimwidth ($date, 0, 10);
		$t_date = explode(".", $t_date);

		$mainNotin[] .= $post->ID;
		if($cont ==1):
	?>
	<div class="main-post">
		<div class="imageWrapper"><div class="image"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail("large"); ?></a></div></div>
		<time datetime="<?php echo $date; ?>">開催日 : <span class="large"><?php echo ereg_replace("^0+", "", $t_date[1]).'月 '. ereg_replace("^0+", "", $t_date[2]).'日'; ?></span></time>
		<h2 class="entries--title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<?php the_excerpt(); ?>
	</div>
	<ul class="entries--list">
	<?php else: ?>
		<li <?php if($cont > 6): echo 'class="grid__item--3 has-gutter"'; endif;?> >
			<a href="<?php the_permalink(); ?>">
				<div class="imageWrapper"><div class="image"><?php if(has_post_thumbnail()): the_post_thumbnail("medium"); else: echo '<img src="' . get_template_directory_uri().'/images/noimg.jpg" alt="no image">'; endif; ?></div></div>	
				<div class="text"><time datetime="<?php echo $date; ?>">開催日 : <span class="large"><?php echo ereg_replace("^0+", "", $t_date[1]).'月 '. ereg_replace("^0+", "", $t_date[2]).'日'; ?></span></time><p><?php the_title(); ?></p></div>
			</a>
		</li>
		<?php if($cont == 6): echo '</ul></div><div class="entries"><ul class="grid grid-fill cf" style="margin-top:24px">'; endif; ?>

	<?php endif; $cont++; endwhile; ?>

	</ul>
	</div>

	<?php endif; ?>

</section>

<div id="wpnav">
	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
</div>

<?php wp_reset_query(); ?>
<?php get_footer(); ?>