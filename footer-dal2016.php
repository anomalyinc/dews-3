<?php if((is_home() || is_archive() || is_single() || is_search() || is_page('music')) && !is_author()): ?>

<aside id="sub2">
	
	<?php if(is_home() && !wp_is_mobile()): ?>

	<div class="sub--bx">
		<script language="JavaScript" type="text/javascript">
		//<![CDATA[
		(function() {
		  var random = new Date();
		  var so_src = ('https:' == document.location.protocol ? 'https://ssl.socdm.com' : 'http://tg.socdm.com') +
		  '/adsv/v1?posall=dews140604SPN&id=14310&t=js&rnd='+ random.getTime() + '&tp=' + encodeURIComponent(document.location.href) + '&pp=' + encodeURIComponent(document.referrer);
		   document.write('<sc'+'ript language="JavaScript" type="text/javascript" src="'+so_src+'"></sc'+'ript>');
		})();
		//]]>
		</script>
	</div>

	<?php elseif(wp_is_mobile()): ?><?php endif; ?>
	<div class="sub--bx">
	<?php 
	
		wp_list_bookmarks(array(
			'categorize' => 0,
			'title_li' => 0,
			'category_name' => 'sub2',
			'limit' => 1,
			'before' => '<span style="display:inline-block;">',
			'after' => '</span>',
			'show_images' => true,
			'show_description' => 0,
			'orderby' => 'rand'
		)); 
		
	?>

	<?php 
	
		wp_list_bookmarks(array(
			'categorize' => 0,
			'title_li' => 0,
			'category_name' => 'sub3',
			'limit' => 1,
			'before' => '<span style="display:inline-block;">',
			'after' => '</span>',
			'show_images' => true,
			'show_description' => 0,
			'orderby' => 'rand'
		)); 
		
	?>
	</div>
	<div class="sub--bx">
		<!-- i-mobile for SmartPhone client script -->
		<script type="text/javascript">
		   imobile_tag_ver = "0.2";
		   imobile_pid = "37527";
		   imobile_asid = "405791";
		   imobile_type = "inline";
		</script>
		<script type="text/javascript" src="http://spad.i-mobile.co.jp/script/adssp.js?20110215"></script>
	</div>


	<!-- 自動更新バナー -->
	<div class="sub--bx">
		<?php date_default_timezone_set('Asia/Tokyo'); $date = date("md"); ?>
		<?php if($date=="1222"): ?>
			<a href="http://goo.gl/3bgFCF" rel="colleague" target="_blank"><img src="<?php echo esc_url( home_url( '/' ) ); ?>/wp-content/uploads/2015/12/dgs-sub2-01-6.jpg"  alt=""  /></a>
		<?php elseif($date=="1223"): ?>
			<a href="http://goo.gl/3bgFCF" rel="colleague" target="_blank"><img src="<?php echo esc_url( home_url( '/' ) ); ?>/wp-content/uploads/2015/12/dgs-sub2-01-5.jpg"  alt=""  /></a>
		<?php elseif($date=="1224"): ?>
			<a href="http://goo.gl/3bgFCF" rel="colleague" target="_blank"><img src="<?php echo esc_url( home_url( '/' ) ); ?>/wp-content/uploads/2015/12/dgs-sub2-01-4.jpg"  alt=""  /></a>
		<?php elseif($date=="1225"): ?>
			<a href="http://goo.gl/3bgFCF" rel="colleague" target="_blank"><img src="<?php echo esc_url( home_url( '/' ) ); ?>/wp-content/uploads/2015/12/dgs-sub2-01-3.jpg"  alt=""  /></a>
		<?php elseif($date=="1226"): ?>
			<a href="http://goo.gl/3bgFCF" rel="colleague" target="_blank"><img src="<?php echo esc_url( home_url( '/' ) ); ?>/wp-content/uploads/2015/12/dgs-sub2-01-2.jpg"  alt=""  /></a>
		<?php elseif($date=="1227"): ?>
			<a href="http://goo.gl/3bgFCF" rel="colleague" target="_blank"><img src="<?php echo esc_url( home_url( '/' ) ); ?>/wp-content/uploads/2015/12/dgs-sub2-01-1.jpg"  alt=""  /></a>
		<?php elseif($date=="1228"): ?>
			<a href="http://goo.gl/3bgFCF" rel="colleague" target="_blank"><img src="<?php echo esc_url( home_url( '/' ) ); ?>/wp-content/uploads/2015/12/dgs-sub2-01-0.jpg"  alt=""  /></a>
		<?php else: ?>
		<?php endif;?>
	</div>

	<?php if(is_archive('column') || is_page('music')): ?>
	<div class="sub--bx entries">
	<div class="section--title <?php if(is_category('dewmo')) echo 'dewmo' ;?>"><span class="icn <?php if(!is_category('dewmo')) echo 'icn-column' ;?>"></span></div>
		<ul class="user-icon cf">
	<?php
		if(is_category('dewmo') || is_category('dewzan')){
			$users_data = get_users('role=author');
		}else{
			$users_data = get_users('role=contributor');
		}
		foreach ($users_data as $user_data):
		$user_blogctg = $user_data->blog_ctg;
			if(is_category('dewmo') && $user_blogctg != 'dewmo'){
				 continue;
			}elseif(is_category('dewzan') && $user_blogctg != 'dewzan'){
				continue;
			}
		$user_login = $user_data->user_login;
		$user_name = $user_data->user_nicename;
		$user_id = $user_data->ID;
	?>
			<li >
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>author/<?php echo esc_attr($user_login); ?>">
					<div class="imageWrapper"><div class="image"><?php echo get_avatar( $user_id , 100); ?></div></div>
				</a>
			</li>

	<?php endforeach; ?>
		</ul>
	</div>
	<?php endif;?>

	<div class="grid-sp">
		<?php 
		$args = array( 'posts_per_page' => 1 , 'category_name' => 'dewmo');
		$my_query = new WP_Query($args); if ($my_query->have_posts()) :  $my_query->the_post();
		?>
		<div class="sub--bx entries">
			<div class="section--title"><span class="icn icn-flower">Dewmo</span></div>
			<ul class="entries--list list-small">
				<li>
					<a href="<?php the_permalink(); ?>">
						<div class="imageWrapper"><div class="image"><?php the_post_thumbnail("medium"); ?></div></div>
						<div class="text"><?php echo esc_html($date); ?></time><p><?php the_title(); ?></p></div>
					</a>
				</li>
			</ul>
		</div>
		<?php endif; wp_reset_postdata(); ?>
		<?php 
		$currnet_date = date_i18n( 'y.m.d' );
		$args = array( 'posts_per_page' => 1 ,
		'post_type' => 'event',
		'category_name' => 'topics',
		'meta_key'=>'DATE', 
		'orderby'=>'meta_value',
		'order' => 'ASC',
		'tax_query' => array( array(
			'taxonomy'=>'eventtype',
			'terms'=> 'eventinfo',
			'field'=>'slug',
			'operator'=>'IN'
			)),
		'meta_query' =>array(array(
			'key' => 'DATE',
			'value' => $currnet_date,
			'compare' => '>=',
			'type' => 'DATE'
			))
		);
		$my_query = new WP_Query($args); if ($my_query->have_posts()) : $my_query->the_post();
		$date = post_custom("DATE");
		$t_date = mb_strimwidth ($date, 0, 10);
		$t_date = str_replace(".","-",$t_date); 
		?>
		<div class="sub--bx entries">
			<div class="section--title"><span class="icn icn-event">PICKUP EVENT</span></div>
			<ul class="entries--list">
				<li>
					<a href="<?php the_permalink(); ?>">
						<div class="imageWrapper"><div class="image"><?php the_post_thumbnail("medium"); ?></div></div>
						<div class="text"><time datetime="<?php echo esc_html($t_date); ?>"><?php echo esc_html($date); ?></time><p><?php the_title(); ?></p></div>
					</a>
				</li>
			</ul>
		</div>
		<?php endif; wp_reset_postdata(); ?>
	</div>
	<div class="grid-sp">
		<?php 
		$args = array( 'posts_per_page' => 1,
		'post_type' => 'movie',
		'tax_query' => array( array(
			'taxonomy'=>'channel',
			'terms'=> 'danceatv',
			'field'=>'slug',
			'operator'=>'IN'
			))
		);
		$my_query = new WP_Query($args); if ($my_query->have_posts()) : $my_query->the_post();
		?>
		<div class="sub--bx entries">
			<div class="section--title"><span class="icn icn-video">DANCE@TV</span></div>
			<div class="iframeWrap"><iframe width="560" height="315" src="//www.youtube.com/embed/<?php echo esc_html(get_post_meta($post->ID,'VIDEOID',true)); ?>?rel=0" frameborder="0" allowfullscreen></iframe></div>
			<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
		</div>
		<?php endif; wp_reset_postdata(); ?>

		<?php 
		$args = array( 'posts_per_page' => 1,'post_type' => array('fashionsnap','dancersgram'));
		$my_query = new WP_Query($args); if ($my_query->have_posts()) : $my_query->the_post();
		?>
		<div class="sub--bx entries">
			<div class="section--title"><span class="icn icn-fashion">DANCER FASHION SNAP</span></div>
			<ul class="entries--list">
				<li>
					<a href="<?php the_permalink(); ?>">
						<div class="imageWrapper"><div class="image"><?php the_post_thumbnail("medium"); ?></div></div>
						<div class="text"><p><?php the_title(); ?></p></div>
					</a>
				</li>
			</ul>
		</div>
		<?php endif; wp_reset_postdata(); ?>
	</div>


</aside>

<?php endif; ?>

</div><!--end content-inner -->
</main><!--end mainContent -->

</div><!--end content -->

<div id="rankingBx">
	<section class="inner entries">
		<div class="section--title"><span class="icn icn-ranking">WEEKLY RANKING</span></div>
		<ul class="grid grid-fill cf">
		<?php 
		$posts = wmp_get_popular( array( 'limit' => 6, 'post_type' => array('post','movie','event','fashionsnap','music','column'), 'range' => 'weekly' ) );
		global $post; if ( count( $posts ) > 0 ): foreach ( $posts as $post ): setup_postdata( $post ); 
		?>
			<li class="grid__item--2 has-gutter">
				<a href="<?php the_permalink(); ?>">
					<div class="imageWrapper"><div class="image"><?php if(has_post_thumbnail()): the_post_thumbnail("medium"); else: echo '<img src="' . get_template_directory_uri().'/images/noimg.jpg" alt="no image">'; endif; ?></div></div>
					<div class="text"><p><?php the_title(); ?></p></div>
				</a>
			</li>
		<?php endforeach; endif; wp_reset_postdata();  ?>
		</ul>
	</section>
</div>

</div><!--end main -->


<footer id="footer">
<div id="fttop">
	<div class="inner">
		<div>
			<h4><img src="<?php echo get_template_directory_uri(); ?>/images/logo-ft.png" alt="DEWS ロゴ"></h4>
			<p>ダンスの情報ならダンスニュースメディア Dews（デュース）。ダンサー情報やダンス動画、ダンスミュージック、ダンスイベントバトル、コンテスト、オーディション情報をいち早く配信します。また、Dewsでは広告出稿を募集しております。広告出稿以外（タイアップ企画､WEBプロデュース等々）も随時受け付けております｡お気軽にお問い合わせください。</p>
		</div>
	</div>
</div>

<div id="ftmiddle">
	<div class="inner cf">
		<div class="grid grid-fill">

			<div id="ftnav" class="grid__item--4 has-gutter">
				<div class="ftnav">
				<h5 class="section--title"><span>Footer Navigation</span></h5>
				<div class="grid grid-fill cf">
					<ul class="grid__item--6 has-gutter">
						<li><a href="<?php echo esc_url( home_url() ); ?>">トップページ</a></li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>about">Dewsとは？</a></li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>beginner">はじめての方へ</a></li>
					</ul>
					<ul class="grid__item--6 has-gutter">
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>member_regist">新規会員登録</a></li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>mypage">ログイン</a></li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>eventform">イベント掲載フォーム</a></li>		
					</ul>
				</div>
				</div>

				<h5 class="section--title"><span>Follow us</span></h5>
				<ul class="ft-follow socialLink">
					<li class="fb"><a href="https://www.facebook.com/dews365" target="_blank" rel="nofollow">FACEBOOK</a></li>
					<li class="tw"><a href="https://twitter.com/Dews365" target="_blank" rel="nofollow">TWITTER</a></li>
					<li class="rss"><a href="http://dews365.com/feed" target="_blank">RSS</a></li>
					<li class="insta"><a href="http://instagram.com/dews365" target="_blank" rel="nofollow">INSTAGRAM</a></li>
					<li class="linea"><a href="http://accountpage.line.me/danceatv" target="_blank" rel="nofollow">LINE@</a></li>
					<li class="yt"><a href="https://www.youtube.com/user/DANCEAWEB" target="_blank" rel="nofollow">YOUTUBE</a></li>
				</ul>

				<h5 class="section--title"><span>Share</span></h5>
				<div class="socialBtn"> 
					<ul>
					<li><a href="https://twitter.com/share" class="twitter-share-button" data-lang="en" data-count="vertical" data-text="ダンスの情報ならダンスニュースメディア Dews（デュース）" data-url="http://dews365.com" rel="nofollow">ツイート</a></li>
					<li><div class="fb-like" data-href="http://dews365.com" data-send="false" data-layout="box_count" data-show-faces="false" data-colorscheme="light"></div></li>
					<li><div id="gplusone" class="g-plusone" data-size="tall" data-href="http://dews365.com"></div></li>
					<li><a href="http://b.hatena.ne.jp/entry/http://dews365.com" class="hatena-bookmark-button" data-hatena-bookmark-title="ダンスの情報ならダンスニュースメディア Dews（デュース）" data-hatena-bookmark-layout="vertical-balloon" data-hatena-bookmark-lang="ja" title="このエントリーをはてなブックマークに追加" rel="nofollow">はてブする</a></li>
					</ul>
				</div>

			</div>

			<div class="grid__item--4 has-gutter tbhidden">
				<div id="twitterBox">
					<a class="twitter-timeline" href="https://twitter.com/Dews365" data-widget-id="491828174108368897" data-chrome="nofooter transparent" rel="nofollow">@Dews365 からのツイート</a>
				</div>
			</div>

			<div id="ftnav" class="grid__item--4 has-gutter tbhidden">
				<div id="likeBox" >
				<div class="fb-like-box" data-href="https://www.facebook.com/dews365"  data-height="400" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="true" data-show-border="false"></div>
				</div>
			</div>

		</div>
	</div>
</div>

<div id="ftbottom">
	<div class="inner cf">
		<?php wp_nav_menu( array('$container' => '', 'fallback_cb' => '', 'theme_location' => 'footer-nav', 'depth' => '1') ); ?>
		<address>Copyright ©Dews All Rights Reserved.</address>
	</div>
</div>

</footer>

</div><!--wrapper-->

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

<script type="text/javascript">
(function(w,d){
     w.___gcfg={lang:"ja"};
     var s,e = d.getElementsByTagName("script")[0],
     a=function(u,f){if(!d.getElementById(f)){s=d.createElement("script");
     s.src=u;if(f){s.id=f;}e.parentNode.insertBefore(s,e);}};
    a("//apis.google.com/js/plusone.js");
    a("//connect.facebook.net/en_US/all.js#xfbml=1&appId=297078603789517&version=v2.0", "facebook-jssdk");
    a("//platform.twitter.com/widgets.js", "twitter-wjs");
    a("//widgets.getpocket.com/v1/j/btn.js?v=1");
    a("//b.hatena.ne.jp/js/bookmark_button.js");
})(this, document);
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-50144969-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

    //新たなトラッキング
  ga('create', 'UA-68544278-1', 'auto', {'name': 'dews2'});
  ga('require', 'displayfeatures');
  ga('dews2.send', 'pageview');
</script>

<script type="text/javascript">
  var _fout_queue = _fout_queue || {}; if (_fout_queue.segment === void 0) _fout_queue.segment = {};
  if (_fout_queue.segment.queue === void 0) _fout_queue.segment.queue = [];

  _fout_queue.segment.queue.push({
    'user_id': 6145
  });

  (function() {
    var el = document.createElement('script'); el.type = 'text/javascript'; el.async = true;
    el.src = (('https:' == document.location.protocol) ? 'https://' : 'http://') + 'js.fout.jp/segmentation.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(el, s);
  })();
</script>
<?php wp_footer(); ?>
</body>
</html>