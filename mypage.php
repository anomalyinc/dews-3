<?php 
/*
Template Name: MY PAGE
*/

get_header(); 
the_post(); 
$page = get_page(get_the_ID());
$pageslug = $page->post_name;
?>

<section>
	<h1 class="section--title"><span class="icn icn-author"><?php the_title(); ?></span></h1>

	<?php 
	$a = $_GET['a'];
	if (is_user_logged_in() && !($a)): 
	global $current_user; 
	get_currentuserinfo()
	;?>

	<div id="profDiv">
		<div class="image"><?php echo get_avatar( $current_user->id, 144); ?></div>
		<div class="profile">
			<table>
				<caption>登録情報</caption>
				<tr>
					<th>ユーザー ID</th>
					<td><?php echo esc_html($current_user->user_login); ?></td>
				</tr>
				<tr>
					<th>名前</th>
					<td><?php echo esc_html($current_user->last_name). ' '. esc_html($current_user->first_name); ?> </td>
				</tr>
				<tr>
					<th>ダンサーネーム</th>
					<td><?php echo esc_html($current_user->dname); ?></td>
				</tr>
				<tr>
					<th>所属チーム</th>
					<td><?php echo esc_html($current_user->team); ?></td>
				</tr>
				<tr>
					<th>性別</th>
					<td><?php if($current_user->gender == "male"): echo "男"; elseif($current_user->gender == "female"): echo "女"; endif; ?></td>
				</tr>
				<tr>
					<th>誕生日</th>
					<td><?php echo esc_html($current_user->birth_y) . "年 " .esc_html($current_user->birth_m) . "月 " . esc_html($current_user->birth_d) ."日"; ?></td>
				</tr>
				<tr>
					<th>住所</th>
					<td><?php echo esc_html($current_user->pref) . " ". esc_html($current_user->address); ?></td>
				</tr>
				<tr>
					<th>メールアドレス</th>
					<td><?php echo esc_html($current_user->user_email); ?></td>
				</tr>
				<tr>
					<th>電話番号</th>
					<td><?php echo esc_html($current_user->user_tel); ?></td>
				</tr>
				<tr>
					<th>自己紹介</th>
					<td class="description"><?php echo esc_html($current_user->description); ?></td>
				</tr>
				<tr>
					<th>イベント参加予定</th>
					<td class="description"><?php echo do_shortcode('[user_anoma_entry]'); ?></td>
				</tr>
			</table>
			<div id="profpage"><a href="<?php echo esc_url( home_url( '/' ) ); ?>author/<?php echo esc_html($current_user->user_login); ?>">プロフィールページ</a></div>
		</div>

		<div class="edit_prof"><?php the_content(); ?></div>

	</div>

	<?php else:?>
		
		<div class="entry edit_prof"><?php the_content(); ?></div>

	<?php endif; ?>	

</section>

<?php get_footer(); ?>