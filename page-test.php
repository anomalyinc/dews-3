<?php /* Template Name: testtest */ ?>
<?php 
get_header(); 
?>
<?php
$args = array('category_name' => 'topicsoftopics','posts_per_page' => 4);
$my_query = new WP_Query($args); if ($my_query->have_posts()) : 
?>
	
	
<?php
include_once(ABSPATH . WPINC . '/feed.php');
$rss = fetch_feed('https://www.nissin.com/jp/news/feed'); // RSSのURLを指定
if (!is_wp_error( $rss ) ) :
	$maxitems = $rss->get_item_quantity(10); // 表示する記事の最大件数
	$rss_items = $rss->get_items(0, $maxitems); 
endif;
?>
<ul>
<?php
if ($maxitems == 0): echo '<li>表示するものががありません</li>';
else :
date_default_timezone_set('Asia/Tokyo');
foreach ( $rss_items as $item ) : ?>
<li class="hentry">
<a href="<?php echo $item->get_permalink(); ?>" rel="bookmark">
<span class="entry-date published"><?php echo $item->get_date('Y年n月j日'); ?></span>
<span class="entry-title"><?php echo $item->get_title(); ?></span>
</a>
</li>
<?php endforeach; ?>
<?php endif; ?>
</ul>


    <div class="swiper-container">
        <div class="swiper-wrapper">

    <?php while($my_query->have_posts()): $my_query->the_post();?>
            <div class="swiper-slide" >
                <div class="content-inner" >
                  <div class="main-post entries">
                      <div class="imageWrapper">
                      <a href="<?php the_permalink() ?>">
                          <div class="entries--title-wrapper">
                              <h3 class="entries--title"><?php the_title(); ?></h3>
                          </div>
                          <div class="image">
                              <?php the_post_thumbnail("full"); ?>
                          </div>
                      </a>
                      </div>
                  </div>
                </div>
            </div>  

    <?php endwhile; ?>
        </div>  
        <div class="swiper-pagination"></div>
    </div>  




<?php endif; wp_reset_postdata();  ?>


<?php
$args = array( 'posts_per_page' => 4 ,'post_type' => array('post','movie','event','music','fashion','column') ,'category_name' => 'special');
$my_query = new WP_Query($args); if ($my_query->have_posts()) : 
?>
<div id="special">
  <div class="entries">
    <h2 class="section--title"><span class="icn icn-special">Dews 特集</span></h2>
    <ul class="sp-grid">
    <?php while($my_query->have_posts()): $my_query->the_post();?>
      <li class="sp-grid__item">
        <a href="<?php the_permalink() ?>">
          <div class="sp-imageWrapper"><span class="rank-number"><?php if(in_category('pr')): $category = get_the_category(); ?>PR<?php else : ?>特集<?php endif; ?></span>
          <div class="image"><?php the_post_thumbnail("medium"); ?></div></div>
          <div class="sp-text"><p><?php the_title(); ?></p></div>
        </a>
      </li>
    <?php endwhile; ?>
    </ul>
  </div>
</div>
<?php endif; wp_reset_postdata();  ?>

    <?php 
      $paged = get_query_var('paged');
      global $mainNotin;
      $args = array( 
        'post_type' => array('post','movie','music','fashionsnap','event','column','audition','dancersgram'), 
        'posts_per_page' => 15 ,
        'post__not_in' => $mainNotin ,
        'category__in' => array(9,12,16,117,13,15,14,9,8),
        'paged' => $paged
        );
      $my_query_main = new WP_Query($args); if ($my_query_main->have_posts()) :

     ?>

<div id="maincol">
  <div class="content-inner">

    <section class="entries">
      <h2 class="section--title"><span class="icn icn-news">TOPICS</span></h2>
      <ul class="entries--list">
        <?php 
          while($my_query_main->have_posts()): $my_query_main->the_post();
          $cats = get_the_category();
          $catNameArray = array();
          $catSlugArray = array();
          foreach($cats as $category ) { 
            if(($category->category_nicename != "news") && ($category->category_nicename != "topics") && ($category->category_nicename != "special")){
              $catNameArray[] .= $category->cat_name;
              $catSlugArray[] .= $category->category_nicename;
            }
          } 
          if(!$catNameArray[0]){
            $catName = "その他";
            $catSlug = "other";
          }else{
            $catName = $catNameArray[0];
            $catSlug = $catSlugArray[0];
          }
         ?>
        <li>
          <a href="<?php the_permalink(); ?>">
            <div class="imageWrapper"><div class="image"><?php if(has_post_thumbnail()): the_post_thumbnail("medium"); else: echo '<img src="' . get_template_directory_uri().'/images/noimg.jpg" alt="no image">'; endif; ?><span class="cat <?php echo esc_html($catSlug); ?>"><?php echo esc_html($catName); ?></span></div></div>
            <div class="text"><time datetime="<?php the_time("c"); ?>"><?php the_time("Y.m.d"); ?></time><p><?php the_title(); ?></p></div>
          </a>
        </li>
        <?php endwhile; ?>
      </ul>
    </section>

    <?php endif; wp_reset_postdata(); ?>
  </div><!--end content-inner -->
</div><!--end maincol -->



<?php get_footer(); ?>

