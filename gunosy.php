<?php
ob_end_clean();
/**
 * RSS2 Feed Template for displaying RSS2 Posts feed.
 *
 * @package WordPress
 */

header('Content-Type: ' . feed_content_type('rss-http') . '; charset=' . get_option('blog_charset'), true);
$more = 1;

echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';

/**
 * Fires between the xml and rss tags in a feed.
 *
 * @since 4.0.0
 *
 * @param string $context Type of feed. Possible values include 'rss2', 'rss2-comments',
 *                        'rdf', 'atom', and 'atom-comments'.
 */
do_action( 'rss_tag_pre', 'rss2' );
?>
<rss version="2.0"
  xmlns:gnf="http://assets.gunosy.com/media/gnf"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:media="http://search.yahoo.com/mrss/"
	<?php
	/**
	 * Fires at the end of the RSS root to add namespaces.
	 *
	 * @since 2.0.0
	 */
	do_action( 'rss2_ns' );
	?>

>

<channel>
	<title>ダンス情報サイト「Dews  (デュース)」</title>
	<link><?php bloginfo_rss('url') ?></link>
  <language>ja</language>
	<description><?php bloginfo_rss("description") ?></description>
  <image>
    <url>http://dews365.com/images/apple-touch-icon.png</url>
    <title>ダンス情報サイト「Dews (デュース)」</title>
    <link><?php self_link(); ?></link>
  </image>
	<?php
	/**
	 * Fires at the end of the RSS2 Feed Header.
	 *
	 * @since 2.0.0
	 */
	do_action( 'rss2_head');

	while( have_posts()) : the_post();
	?>
<?php 
	$custom_fields_release = get_post_meta( get_the_ID() , 'Gunosy' , true ); 
	$thumbnail_id = get_post_thumbnail_id(); 
	$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );

	$content = get_the_content('','false');
	$content = strip_shortcodes($content);
	$content = preg_replace("/\n/","<br />",$content); 
?>
	<?php if ($custom_fields_release){ ?>
		<item>
			<title><?php the_title_rss() ?></title>
			<link><?php the_permalink_rss() ?></link>
			<comments><?php comments_link_feed(); ?></comments>
			<pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
      		<gnf:modified><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_modified_time('Y-m-d H:i:s', true), false); ?></gnf:modified>
			<dc:creator>Dews staff</dc:creator>
			<category>dance,ダンス,エンタメ</category>
			<status>0</status>
			<guid isPermaLink="false"><?php the_guid(); ?></guid>
      		<gnf:category>entertainment</gnf:category>
      		<gnf:keyword>ダンス,dance,ダンサー,踊る,踊ってみた,ブレイクダンス,ストリートダンス</gnf:keyword>
			<description><![CDATA[<?php  echo esc_html(strip_tags(get_the_excerpt())); ?>]]></description>
      		<content:encoded><![CDATA[<p><?php echo $content; ?><p>]]></content:encoded>
      <?php
$categories = wp_get_post_categories($post->ID, array('orderby'=>'rand')); 

if ($categories) {
	$args = array(
		'category__in' => array($categories[0]), // カテゴリーのIDで記事を取得
		'post__not_in' => array($post->ID), // 表示している記事を除く
		'showposts'=>3, // 取得記事数
		'caller_get_posts'=>1, // 取得した記事の何番目から表示するか
		'orderby'=> 'rand' // 記事をランダムで取得
	); 
	$my_query = new WP_Query($args); 
	if( $my_query->have_posts() ) { 
		$thumbnail_id = get_post_thumbnail_id(); 
		$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
		?>
		<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
			<gnf:relatedLink title="<?php the_title(); ?>" link="<?php the_permalink(); ?>" thumbnail="<?php echo $eye_img[0]; ?>" />
		<?php endwhile;
	}
	wp_reset_query();
}
?>
      <media:status state="active" />
      <gnf:analytics><![CDATA[
            <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-50144969-1', 'auto');
            ga('set', 'appName', 'newspass');
            ga('set', 'referrer', 'https://newspass.jp');
            ga('set', 'location', 'http://foo.com/article/123456/');
            ga('send', 'pageview');
            </script>
        ]]></gnf:analytics>
        <gnf:analytics_gn><![CDATA[
            <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-50144969-1', 'auto');
            ga('set', 'appName', 'gunosy');
            ga('set', 'referrer', 'https://gunosy.com');
            ga('set', 'location', 'http://foo.com/article/123456/');
            ga('send', 'pageview');
            </script>
        ]]></gnf:analytics_gn>
	　<enclosure <?php echo 'url="'.$eye_img[0].'" '; ?> />
			
		
	<?php rss_enclosure(); ?>
		<?php
		/**
		 * Fires at the end of each RSS2 feed item.!
		 *
		 * @since 2.0.0
		 */
		do_action( 'rss2_item' );
		?>

		</item>
	<?php } ?>
	<?php endwhile; ?>
</channel>
</rss>