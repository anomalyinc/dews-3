<!DOCTYPE HTML>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width">
<title><?php wp_title( '|', true, 'right' ); ?><?php if($paged !=0){echo $paged."ページ目｜";} ?>Dews</title>
<!-- グーグルコンソール連携-->
<meta name="google-site-verification" content="45safGZijfme1hAY4Wnra0DHhKPsPxee5swXLAdlEuk" />
<!-- <meta name="apple-itunes-app" content="app-id=1219900771"> -->
<link rel="shortcut icon" href="<?php echo esc_url( home_url( '/' ) ); ?>images/favicon.ico">
<link rel="apple-touch-icon" href="<?php echo esc_url( home_url( '/' ) ); ?>images/apple-touch-icon.png">
<link type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" >
<link type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/glaceausnap.css" rel="stylesheet" >
<link type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/swiper.min.css" rel="stylesheet" >
<link type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/vertical-timeline.css" rel="stylesheet" >
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/icomoon/style.css">

<?php
wp_deregister_script('jquery');
wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js', array(), '1.11.2');
wp_enqueue_script('jqueryui', 'http://code.jquery.com/ui/1.10.2/jquery-ui.js', array(), '1.11.1');
?>

<?php wp_head(); ?>

<!--[if lte IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/selectivizr-min.js"></script>
<![endif]-->

</head>

<body role="document" itemscope="itemscope" itemtype="http://schema.org/WebPage" <?php if(is_single()){ echo' class="single"'; } ?> >
<div id="fb-root"></div>
<div id="wrapper" <?php if(is_home()) echo 'class="animating"'; ?>>

<!-- CMerスマホタグ-->
<script language="JavaScript" type="text/javascript">
//<![CDATA[
(function() {
  var random = new Date();
  
  var so_src = ('https:' == document.location.protocol ? 'https://ssl.socdm.com' : 'http://tg.socdm.com') +
  '/adsv/v1?posall=Dews150731pv&id=26899&t=js&rnd='+ random.getTime() + '&tp=' + encodeURIComponent(document.location.href) + '&pp=' + encodeURIComponent(document.referrer);
  
   document.write('<sc'+'ript language="JavaScript" type="text/javascript" src="'+so_src+'"></sc'+'ript>');
})();
//]]>
</script>

<header id="header" role="banner" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
	<div class="inner cf">
	  <div class="header__top">

		<h1 id="logo"><span>ダンスニュースメディアサイト “デュース”</span>
		<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img itemprop="image" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php bloginfo('name'); ?>"></a>
		</h1>
		<div id="header__adsense">
		<?php if( wp_is_mobile()) : ?>
			<!-- 320×100)DEWS_SPヘッダー	-->
			<script type="text/javascript" src="http://js.sprout-ad.com/t/293/569/a1293569.js"></script>
		<?php else : ?>
			<!--  (728×90)DEWS_PC最ヘッダー	-->
			<script type="text/javascript" src="http://js.sprout-ad.com/t/293/573/a1293573.js"></script>
		<?php endif; ?>
		</div>
		<!-- <div id="header__adsense">
		<?php
		 $bm = get_bookmarks( array(
		   'orderby'=> 'rand', 
		   'limit'  => 1, 
		   'category_name'  => 'toppc'
		 ));

		 foreach ($bm as $bookmark){ 
		   $click="'send','event','top','Click','".$bookmark->link_name."'";
		   $imp = "'send','event','top','imp','".$bookmark->link_name."'";
		   echo '<a href="'.$bookmark->link_url.'" onclick="ga('.$click.');"  target="_blank">';
		   echo '<img src="'.$bookmark->link_image.'" onload="ga('.$imp.');" alt="'.$bookmark->link_name.'">';
		   echo '</a>';
		 }
		?> 
		</div> -->
	   </div>
	</div>

	<div id="header--top" >
		<div class="inner cf">
			<div id="menubtn"></div>
	<!-- 		<ul class="socialLink tbhidden">
				<li class="fb"><a href="https://www.facebook.com/dews365" target="_blank" rel="nofollow">FACEBOOK</a></li>
				<li class="tw"><a href="https://twitter.com/Dews365" target="_blank" rel="nofollow">TWITTER</a></li>
				<li class="rss"><a href="http://dews365.com/feed" target="_blank">RSS</a></li>
				<li class="insta"><a href="http://instagram.com/dews365" target="_blank" rel="nofollow">INSTAGRAM</a></li>
				<li class="linea"><a href="http://accountpage.line.me/danceatv" target="_blank" rel="nofollow">LINE@</a></li>
			</ul> -->	
			<div class="only-pc">
		<ul class="socialLink flex">
        	<li><a href="http://dews365.com/feed" rel="nofollow" class="fb"><i class="fas fa-rss"></i></a></li>
        	<li><a href="https://www.facebook.com/dews365" target="_blank" rel="nofollow" class="fb"><i class="fab fa-facebook-f"></i></a></li>
        	<li><a href="https://twitter.com/Dews365" target="_blank" rel="nofollow"><i class="fab fa-twitter"></i></a></li>
        	<li><a href="http://instagram.com/dews365" target="_blank" rel="nofollow"><i class="fab fa-instagram"></i></a></li>
        	<li><a href="https://line.me/R/ti/p/%40ryh9583h?utm_campaign=single_bnr_lineat&utm_source=dews&utm_medium=dews" class="line"><i class="fab fa-line"></i></a></li>
        	<li><a href="https://www.youtube.com/user/DANCEHEROjp" target="_blank"><i class="fab fa-youtube"></i></a></li>
        </ul> 
        </div>
		</div>
	</div>
	<div id="navigation">
		
	    <nav id="nav-pc" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
	    	<ul id="menu" class="inner flex menu menu--pc">
	    		<li id="newsbtn" class="menu__item"><a href="<?php echo esc_url( home_url( '/' ) ); ?>category/news">ニュース</a></li>
	    		<li id="videobtn" class="menu__item"><a href="<?php echo esc_url( home_url( '/' ) ); ?>movie">ダンス動画</a></li>
	    		<li id="eventbtn" class="menu__item"><a href="<?php echo esc_url( home_url( '/' ) ); ?>event">イベントインフォ</a></li>
	    		<li id="dewzanbtn" class="menu__item"><a href="<?php echo esc_url( home_url( '/' ) ); ?>dblog">Dews Blog</a></li>
	    	</ul>
	     </nav>
	     <nav id="nav-sp" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
	    	<ul class="inner flex flex--around menu menu--sp">
                <li class="menu__item flex__item--2"><a href="http://dews365.com/category/news">新着順</a></li>
                <li class="menu__item flex__item--2"><a href="http://dews365.com/rank">記事ランキング</a></li>
        <!--    <li><a href="http://dews365.com/category/category/dblog">ダンサーブログ</a></li> -->
        	</ul>
        </nav>
    </div>

    
</header>

<div id="main">
<?php if(!is_page( '125885' )): ?>
<div id="content" class="inner cf">
<main id="mainContent" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/WebPageElement">
<div class="content-inner" >
<div id="maincol">
<?php endif; ?>