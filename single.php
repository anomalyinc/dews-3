<?php
get_header();
the_post();
$cat = get_the_category();
$catslug = $cat[0]->slug;
$catname = $cat[0]->name;
$catid = $cat[0]->cat_ID;
$catlink = get_category_link( $catid );

if(get_post_type() == 'music'):
	$term = array_shift(get_the_terms($post->ID, 'djs'));
	$djslug = $term->slug;
	$djname = $term->name;
endif;

?>

    		<div id="social" class="social">
            	<ul class="social__list">
            		 <li class="social__item social__item--home"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="nofollow" class="fb"><i class="fas fa-home"></i></a></li>
            		<li class=" social__item social__item--fb"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" target="_blank" rel="nofollow" class="fb"><i class="fab fa-facebook-f"></i></a></li>
            		<li class=" social__item social__item--tw"><a href="http://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>&tw_p=tweetbutton" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"><i class="fab fa-twitter"></i></a></li>
            		<li class=" social__item social__item--line"><a href="http://line.me/R/msg/text/?<?php the_title(); ?>%0D%0A<?php the_permalink(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" class="	line"><i class="fab fa-line"></i></a></li>
        		</ul> 
 			</div>


		<?php $ua = $_SERVER['HTTP_USER_AGENT']; ?>
		<?php if (is_mobile() && (strpos($ua, 'DewsApp') !== true) ) : ?>
			<div id="app_fixed" class="app">
		        <div class="app__img">
		            <a href="https://goo.gl/5VA1s9?ls=1&mt=8&utm_campaign=app&utm_source=dews&utm_medium=appbanner" target="_blank" title="ios dews アプリ"><img src="<?php echo get_template_directory_uri(); ?>/images/app_banner.jpg" alt=""></a>
		        </div>
		    </div>

		
		    


		
		    <!-- layer_board -->
			<!-- <div id="layer_board_area">
				<div class="layer_board_bg"></div>
				<div class="layer_board">
					<p><a href="https://goo.gl/5VA1s9?ls=1&mt=8&utm_campaign=app&utm_source=dews&utm_medium=apppopup" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/app_popup.jpg" /></a></p>
				</div>
			</div> -->
<!-- //layer_board -->
		<?php else: ?>
		<?php endif; ?>

	
		<div class="section entry">
<div class="section--title">
	<?php if ( in_category('1908')){
	      	echo 'Dews Blog'; 
	      } 
	    elseif(get_post_type() == 'post'){
		echo 'ダンスニュース';
	    }
	    else {
	      	echo esc_html(get_post_type_object(get_post_type())->label );
	      	} 
	       ?>
	     
	</div> 

	<article itemscope <?php  if(get_post_type() =='event'): ?> itemtype="http://schema.org/Event" <?php  else: ?>itemtype="http://schema.org/Article" <?php endif; ?> >
		
		<h1 itemprop="name">
			<?php the_title(); ?>
		</h1>

		<div class="data">
			<div><time datetime="<?php the_time("c"); ?>">Publish / <?php the_time("Y.m.d"); ?></time></div>
			<?php if(get_post_type() == 'fashionsnap' && get_post_meta($post->ID , 'PHOTOGRAPHER' ,true)): ?><div>photo by : <?php echo get_post_meta($post->ID , 'PHOTOGRAPHER' ,true); ?></div><?php endif; ?>
			<?php if(get_post_type() == 'column' || get_post_type() == 'music'): ?><div>Author : <?php the_author_posts_link(); ?></div><?php endif; ?>
			<?php
				if(get_the_category()):
				echo '<div>Category : ';
				$cats = get_the_category();
				$exclude = array(8,9,12,1908,3362);
				foreach((array)$cats as $cat)
				  if(!in_array($cat->cat_ID, $exclude))
				    echo '<a href="' . get_category_link($cat->cat_ID) . '">' . $cat->cat_name . '</a> , ';
				echo '</div>';
				endif;
			?>
			<?php if(get_the_tags()):?>
        	<div id="tag-area">
        		<?php the_tags('<ul><li>投稿タグ</li><li>','</li><li>','</li></ul>');?>
        	</div>
        <?php endif; ?>
		</div>
        <?php if($catid == 1908 or $catid == 1899 or $catid == 1909 or get_post_type() == 'column'): ?>
		<div class="author">
			<div class="author__info author__info-title">
    	        <div class="author__img">
    	            <a href="/author/<?php the_author_login(); ?>" title="<?php the_author_meta( 'dname', $post->post_author ); ?>">
    	            <?php echo get_avatar($post->post_author); ?>
	
    	            </a>
    	        </div>
    	        <div class="author__profile">
    	        	  <span class="author__type"></span>
    	            <a href="/author/<?php the_author_login(); ?>" class="author__name">
    	            	<?php the_author(); ?>
    	      			<?php 
    	            	if(get_the_author_meta('team',$post->post_author)): ?>(<?php the_author_meta( 'team', $post->post_author ); ?> )
    	            	<?php endif; ?>
    	            </a>
    	        </div>
    	        <div class="author-sns">
    	        	<?php if(get_the_author_meta('twitter',$post->post_author)): ?>
    	            <div class="author-sns__item author-sns__item--twitter">
    	                <a href="https://twitter.com/<?php the_author_meta( 'twitter', $post->post_author ); ?>" target="_blank">
    	                    <i class="fab fa-twitter" aria-hidden="true"></i>
    	                </a>
    	            </div>
    	        	<?php endif; ?>
    	            <?php if(get_the_author_meta('facebook',$post->post_author)): ?>
    	            <div class="author-sns__item author-sns__item--facebook">
    	                <a href="https://www.facebook.com/<?php the_author_meta( 'facebook', $post->post_author ); ?>" target="_blank">
    	                    <i class="fab fa-facebook-f" aria-hidden="true"></i>
    	                </a>
    	            </div>
    	            <?php endif; ?>
    	            <?php if(get_the_author_meta('Instagram',$post->post_author)): ?>
    	             <div class="author-sns__item author-sns__item--instagram">
    	                <a href="https://www.instagram.com/<?php the_author_meta( 'Instagram', $post->post_author ); ?>" target="_blank">
    	                    <i class="fab fa-instagram" aria-hidden="true"></i>
    	                </a>
    	            </div>
    	            <?php endif; ?>
    	        </div>
    	    </div>
    	</div>
		<?php endif; ?>
		<div class="socialBtn">

			<ul>
				<li ><a href="https://twitter.com/share" class="twitter-share-button" data-text="" data-url="<?php the_permalink(); ?>" data-related="@dews365" data-lang="ja" data-show-count="false">ツイート</a></li>
				<li ><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" target="_blank" class="facebook-share-button" rel="nofollow"><i class="fab fa-facebook-f"></i> <span>シェア</span></a></li>
			</ul>


		</div>


		 <?php
		   $bm = get_bookmarks( array(
		     'orderby'=> 'rand', 
		     'limit'  => 1, 
		     'category_name'  => 'single-1'
		   ));

		   foreach ($bm as $bookmark){ 
		     $click="'send','event','single','click','".$bookmark->link_name."'";
		     $imp = "'send','event','single','imp','".$bookmark->link_name."'";
		     echo '<p><a href="'.$bookmark->link_url.'" onclick="ga('.$click.');"  target="_blank">';
		     echo '<img src="'.$bookmark->link_image.'" onload="ga('.$imp.');" alt="'.$bookmark->link_name.'">';
		     echo '</a></p>';
		   }
		?> 


		<?php
		if(get_post_type() == 'event'): //---------------------- case : EVENT
		$date = post_custom("DATE");
		$t_date = mb_strimwidth ($date, 0, 10);
		$t_date = explode(".", $t_date);
		?>

		<div class="detail grid grid-fill cf">
			<div class="image grid__item--8 has-gutter">
				<?php the_post_thumbnail("large") ?>
			</div>
			<div class="grid__item--4 has-gutter">
				<dl>
					<dt>開催日</dt><dd itemprop="startDate" content="<?php echo $t_date[0].'-'. $t_date[1].'-'. $t_date[2]; ?>"><?php echo $t_date[0].'年 '. $t_date[1].'月 '. $t_date[2].'日'; ?></dd>
					<?php if(get_post_meta($post->ID , 'VENUE' ,true)): ?><dt>場所  </dt><dd itemprop="location" itemscope itemtype="http://schema.org/Place"><?php echo get_post_meta($post->ID , 'VENUE' ,true); ?> </dd><?php endif; ?>
					<?php if(get_post_meta($post->ID , 'OPEN' ,true)): ?><dt>時間  </dt><dd>OPEN : <?php echo get_post_meta($post->ID , 'OPEN' ,true); ?> / CLOSE : <?php echo get_post_meta($post->ID , 'CLOSE' ,true); ?></dd><?php endif; ?>
					<?php if(get_post_meta($post->ID , 'FEE' ,true)): ?><dt>料金  </dt><dd itemprop="offers" itemscope itemtype="http://schema.org/Offer"><?php echo get_post_meta($post->ID , 'FEE' ,true); ?></dd><?php endif; ?>
					<dt>ジャンル</dt>
					<dd class="genre">
					<?php
					if ($terms = get_the_terms($post->ID, 'genre')) {
					    foreach ( $terms as $term ) {
					        echo '<a href="' . esc_url(home_url( '/' )) . 'genre/'.esc_html($term->slug) . '">' . esc_html($term->name) . '</a>';
					    }
					}
					?>
					</dd>
				</dl>
			</div>
		</div>

		<?php the_content(); echo "\n"; ?>

		<?php
		elseif(get_post_type() == 'music'): // -------------------- case : MUSIC
		echo '<div class="image"><img src="'.esc_url(home_url('/')).'images/music/'.esc_html($post->post_name).'/00.jpg"></div>'."\n";
		?>

		<?php the_content(); echo "\n"; ?>

		<section>
			<h3>Recomend New Blast</h3>
			<?php
			$costumf  = get_post_custom( $post->ID );
			$mtitle	= $costumf['MUSIC TITLE'];
			$martist = $costumf['MUSIC ARTIST'];
			$mtext = $costumf['MUSIC TEXT'];
			$affcode = $costumf['AFFCODE'];
			$video = $costumf['VIDEOID'];
			$soundcloud = $costumf['SOUND CLOUD'];
			$length = count( $mtitle );

			for( $i = 0; $i < $length; $i ++ ){
				$num = $i+1;
				echo "\t\t\t\t\t".'<div class="musicBox">'."\n";
				echo "\t\t\t\t\t".'<div class="image"><img src="'.esc_url(home_url('/')).'images/music/'.esc_html($post->post_name).'/0'.esc_html($num).'.jpg">'."\n";
				if($affcode[$i]): echo '<div><a href="'.esc_html($affcode[$i]).'" target="itunes_store"><img src="https://linkmaker.itunes.apple.com/htmlResources/assets//images/web/linkmaker/badge_itunes-sm.png" alt="iTunes"></a>'.'</div>'."\n"; endif;
				echo '</div>'."\n";
				echo "\t\t\t\t\t".'<h5>'.esc_html($martist[$i]).' / '.esc_html($mtitle[$i]).'</h5>'."\n";
				echo "\t\t\t\t\t".'<p>'.nl2br(esc_html($mtext[$i])).'</p>'."\n";
				if($soundcloud[$i]): echo '<div class="video">'.strip_tags($soundcloud[$i],"<iframe>").'</div>'."\n"; endif;
				if($video[$i]): echo '<div class="video"><div class="iframeWrap"><iframe width="560" height="315" src="//www.youtube.com/embed/'.esc_html($video[$i]).'?rel=0" frameborder="0" allowfullscreen></iframe></div></div>'."\n"; endif;
				echo "\t\t\t\t\t".'</div>'."\n";
			}
			?>
		</section>


		<?php
		elseif(get_post_type() == 'university'): // -------------------- case : university
		?>

		<section class="university">
			<div class="detail grid grid-fill cf">
				<div class="image grid__item--8 has-gutter">
				<?php if(has_post_thumbnail()): ?><p><?php the_post_thumbnail("large") ?></p><?php endif; ?>
					<?php
						$costumf  = get_post_custom( $post->ID );
						$u_region	= $costumf['REGION'];
						$u_area = $costumf['AREA'];
						$u_option = $costumf['OPTION'];
						$u_university = $costumf['university'];
						$u_univHP = $costumf['universityHP'];
						$u_fb = $costumf['Facebook'];
						$u_tw = $costumf['twitter'];
						$u_twsc = $costumf['twitter_script'];
						$u_circleHP = $costumf['HP'];
						$u_blog = $costumf['blog'];
						$u_place = $costumf['place'];
						$u_date = $costumf['date'];
						$u_people = $costumf['people'];
						$u_genre = $costumf['genre'];
						$u_schedule = $costumf['schedule'];
						$u_ob = $costumf['ob'];
						$u_youtube = $costumf['youtube'];
						$length = count( $mtitle );

						echo '<p>';
						if($u_univHP[0]) :
						echo $u_university[0].' HP：<a href="'.$u_univHP[0].'" target="_blank" >'.$u_univHP[0].'</a><br>';
						endif;
						echo '活動場所：';
						if($u_place[0]) : echo esc_html($u_place[0]); else: echo '非公開'; endif;
						echo '<br />';
						echo '活動日時：';
						if($u_date[0]) : echo esc_html($u_date[0]) ; else: echo '非公開'; endif ;
						echo '<br />';
						if($u_people[0])  echo '活動人数：'.esc_html($u_people[0]).'<br>' ;
						if($u_genre[0]){
							echo 'ジャンル：';
							foreach($u_genre as $key => $value) {
								echo $value  ." , " ;
							}
							echo '<br>';
						};
						echo '</p>';
					?>
					<hr class="style-one" />
					<p class="margin0">サークル紹介：
					<?php
						if($u_circleHP[0]) :
						echo '<br />HP：<a href="'.$u_circleHP[0].'" target="_blank" >'.$u_circleHP[0].'</a><br />';
						endif;
					?>
					</p>
					<div class="introduction"><?php the_content(); echo "\n"; ?></div>

						<?php
							if($u_schedule[0])echo '<p class="margin0">年間スケジュール：</p>'.wpautop( esc_html($u_schedule[0]));
							if($u_youtube[0]){
								echo '<div class="iframeWrap">';
								echo '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.esc_html($u_youtube[0]).'" frameborder="0" allowfullscreen></iframe>';
								echo '</div>';
							}
						?>

				</div> <!-- end of grid__item8 -->

				<div class="grid__item--4 has-gutter">
					<div>
					<?php
						if($u_fb[0]){
							$fb_html='
								<div>
								<div id="likeBox" >
								<div class="fb-page" data-href="https://www.facebook.com/'.$u_fb[0].'" data-width="50%" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/'.$u_fb[0].'"><a href="https://www.facebook.com/'.$u_fb[0].'">'.$title.'</a></blockquote></div></div>
								</div>
								</div>
							';
							echo $fb_html;

						}
					?>

					<?php
						if($u_tw[0] && $u_twsc[0]){
							$twitter_html='
								<div>
									<div id="twitterBox">
									'.$u_twsc[0].'
									</div>
								</div>
							';
							echo $twitter_html;
						}
					?>
					</div>
				</div><!-- end of grid__item4 -->
			</div><!-- end of grid -->

		</section>

		<?php elseif(get_post_type() == 'post'):  // ----------------------------- case : POST ?>
		<div class="single__top">
			<?php if(has_post_thumbnail()): ?><?php the_post_thumbnail("large") ?><?php endif; ?>
			<br>
			<?php if(post_custom('source') && post_custom('source_url')): ?>
			<cite class="post-copy"><a href="<?php echo post_custom('source_url'); ?>" target="_blank" rel="nofollow"><?php echo post_custom('source'); ?></a></cite>
			<?php endif; ?>
		</div>
		<?php if( wp_is_mobile()) : ?>
			<!--  DEWS_SP記事ミドル: 320x100 ZoneId:1293571-->
			<div class="ads">
				<div class="ads__item">
					<script type="text/javascript" src="http://js.sprout-ad.com/t/293/571/a1293571.js"></script>
				</div>
			</div>
		<?php endif; ?>
		
		<?php if($catid == 3362) :?>
			<?php $ua = $_SERVER['HTTP_USER_AGENT']; ?>
			<?php if ((strpos($ua, 'DewsApp') !== false)) : ?>
				<?php the_content(); echo "\n"; ?>
			<?php else : ?>
				<?php the_excerpt(); ?>
				<div class="app-button"><a href="http://aaa" rel="noopener" target="_blank" class="app-button__link">続きはアプリで読む</a></div>
			<?php endif; ?>
		<?php else: ?>
			<?php the_content(); echo "\n"; ?>
		<?php endif; ?>	

		<?php elseif(get_post_type() == 'studio'):  // ----------------------------- case : STUDIO ?>

			<div class="detail grid grid-fill cf">
				<div class="grid__item--4 has-gutter"><img src="<?php echo esc_url( home_url( '/' )); ?>images/studio/<?php echo esc_html($post->post_name ); ?>/main.jpg" alt="<?php the_title(); ?>"></div>

				<div class="grid__item--8 has-gutter">
					<dl>
						<?php
						if((get_post_meta($post->ID,'OTHERBLOG',true)) || (get_post_meta($post->ID,'AMEBLO',true)) || (get_post_meta($post->ID,'FACEBOOK',true)) || (get_post_meta($post->ID,'TWITTER',true)) || (get_post_meta($post->ID,'INSTA',true)) || (get_post_meta($post->ID,'TUMBLR',true)) || (get_post_meta($post->ID,'SOUNDCLOUD',true)) ):
						?>
						<dt>SNS : </dt>
						<dd>
						<ul class="ft-follow socialLink">
							<?php if(get_post_meta($post->ID,'OTHERBLOG',true)):?><li class="bl"><a href="<?php echo post_custom("OTHERBLOG"); ?>" target="_blank" rel="nofollow">ブログ</a> <?php endif;?>
							<?php if(get_post_meta($post->ID,'AMEBLO',true)):?><li class="ab"><a href="http://ameblo.jp/<?php echo post_custom("AMEBLO"); ?>" target="_blank" rel="nofollow">アメブロ</a> <?php endif;?>
							<?php if(get_post_meta($post->ID,'FACEBOOK',true)): ?><li class="fb"><a href="https://www.facebook.com/<?php echo post_custom("FACEBOOK"); ?>" target="_blank" rel="nofollow">FACEBOOK</a> <?php endif;?>
							<?php if(get_post_meta($post->ID,'TWITTER',true)): ?><li class="tw"><a href="https://twitter.com/<?php echo post_custom("TWITTER"); ?>" target="_blank" rel="nofollow">TWITTER</a> <?php endif;?>
							<?php if(get_post_meta($post->ID,'instagram',true)): ?><li class="insta"><a href="https://www.instagram.com/<?php echo post_custom("instagram"); ?>" target="_blank" rel="nofollow">Instagram</a> <?php endif;?>
						</ul>
						</dd>
						<?php endif; ?>

						<?php if(get_post_meta($post->ID,'WEBSITE',true)): ?>
						<dt>WEBSITE : </dt>
						<dd><a href="<?php echo post_custom("WEBSITE"); ?>" target="_blank" rel="nofollow"><?php echo post_custom("WEBSITE"); ?></a></dd>
						<?php endif; ?>

						<?php if(get_post_meta($post->ID,'ADDRESS',true)): ?>
						<dt>住所</dt>
						<dd><?php echo nl2br(get_post_meta($post->ID,'ADDRESS',true)); ?></dd>
						<?php endif; ?>

						<?php if ($terms = get_the_terms($post->ID, 'studiogenre')): ?>
						<dt>ジャンル</dt>
						<dd id="tag-area">
							<ul>
							<?php
							    foreach ( $terms as $term ) {
							        echo '<li>' . esc_html($term->name) . '</li>';
							    }
							?>
							</ul>
						</dd>
						<?php endif; ?>
					</dl>
				</div>
			</div>

			<?php the_content(); echo "\n"; ?>

	
		<?php else:  // ----------------------------- case : そのた ?>
			<?php the_content(); echo "\n"; ?>

		<?php endif; ?>



<!-- ページ送り -->
<?php wp_link_pages(array('before' => '<div class="page-links">','after' => '</div>','link_before' => '<span class="page-links_tp">','link_after' => '</span>','next_or_number' => 'next','nextpagelink' => __( '続きを読む ▷' ), 'previouspagelink' => __( '◁ 前のページ' ),) ); ?>

<?php wp_link_pages(array('before' => '<div class="page-links">','after' => '</div>','link_before' => '<span class="page-links_t">','link_after' => '</span>', ) ); ?>

		<?php 
			$costumform  = get_post_custom();
			
			if($costumform['related_dancer'] or $costumform['related_studio'] or $costumform['related_circle'] or $costumform['related_event']):
				$varArray = array(
				    "related_dancer" => $costumform['related_dancer'],
				    "related_studio" => $costumform['related_studio'],
				    "related_circle" => $costumform['related_circle'],
				    "related_event" => $costumform['related_event']
				);
				echo thispage_reration($varArray); 
			endif;?>


	<div class="author">
		<div class="author__info author__info-title">
            <div class="author__img">
                <a href="/author/<?php the_author_login(); ?>" title="<?php the_author_meta( 'dname', $post->post_author ); ?>">
                <?php echo get_avatar($post->post_author); ?>

                </a>
            </div>
            <div class="author__profile">
            	  <span class="author__type"></span>
                <a href="/author/<?php the_author_login(); ?>" class="author__name">
                	<?php the_author(); ?>
          			<?php 
                	if(get_the_author_meta('team',$post->post_author)): ?>(<?php the_author_meta( 'team', $post->post_author ); ?> )
                	<?php endif; ?>
                </a>
            </div>
            <div class="author-sns">
            	<?php if(get_the_author_meta('twitter',$post->post_author)): ?>
                <div class="author-sns__item author-sns__item--twitter">
                    <a href="https://twitter.com/<?php the_author_meta( 'twitter', $post->post_author ); ?>" target="_blank">
                        <i class="fab fa-twitter" aria-hidden="true"></i>
                    </a>
                </div>
            	<?php endif; ?>
                <?php if(get_the_author_meta('facebook',$post->post_author)): ?>
                <div class="author-sns__item author-sns__item--facebook">
                    <a href="https://www.facebook.com/<?php the_author_meta( 'facebook', $post->post_author ); ?>" target="_blank">
                        <i class="fab fa-facebook-f" aria-hidden="true"></i>
                    </a>
                </div>
                <?php endif; ?>
                <?php if(get_the_author_meta('Instagram',$post->post_author)): ?>
                 <div class="author-sns__item author-sns__item--instagram">
                    <a href="https://www.instagram.com/<?php the_author_meta( 'Instagram', $post->post_author ); ?>" target="_blank">
                        <i class="fab fa-instagram" aria-hidden="true"></i>
                    </a>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <p class="author__profile"><?php the_author_meta('user_description'); ?></p>
        <div>
        	<a href="/author/<?php the_author_meta( 'slug', $post->post_author ); ?>" class="author__btn"><?php the_author(); ?>の記事一覧はこちら</a>
        </div>
    </div>

	<!-- 関連記事一覧タグ -->
    <?php 
	//タグ情報から関連記事をランダムに呼び出す
	$tags = wp_get_post_tags($post->ID);
	$tag_ids = array();
	foreach($tags as $tag):
	  array_push( $tag_ids, $tag -> term_id);
	endforeach ;
	$args = array(
	  'post__not_in' => array($post -> ID),
	  'posts_per_page'=> 4,
	  'tag__in' => $tag_ids,
	  'orderby' => 'rand',
	);
	$query = new WP_Query($args); ?>
	<?php if( $query -> have_posts() && !empty($tag_ids) ): ?>
	<div class="related">
		<h3 class="head-word03 related__head">関連記事</h3>
    	<ul class="related__list">
		<?php while ($query -> have_posts()) : $query -> the_post(); ?>
    		<li class="related__item">
    		    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
    		        <div class="related__flex">
    		            <div class="related__img">
    		                <?php if(has_post_thumbnail()): the_post_thumbnail("medium"); else: echo '<img src="' . get_template_directory_uri().'/images/noimg.jpg" alt="no image">'; endif; ?>
    		            </div>
    		            <div class="related__text">
    		                <h5><?php the_title(); ?></h5>
    		                <p><?php echo mb_substr($post->post_title, 0, 40).'……'; ?></p>
    		            </div>
    		    	</div>
    			</a>
    		</li>
    	<?php endwhile; ?>
    	</ul>
    </div>

    <?php
		endif;
		wp_reset_postdata();
	?>
      	    		
    <?php if(get_the_tags()):?>
    <div id="tag-area">
    	<?php the_tags('<ul><li>投稿タグ</li><li>','</li><li>','</li></ul>');?>
    </div>
    <?php endif; ?>


        <div class="single-sns flex flex--sp flex--around">
        	<div class="flex__item--3">
				<a class="single-sns__btn single-sns__btn--facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" target="_blank" rel="nofollow">
				<i class="fab fa-facebook-f"></i><span>この記事をシェア</span>
				</a>
			</div>
			<div class="flex__item--3">
			<a class="single-sns__btn single-sns__btn--twitter" href="http://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>&tw_p=tweetbutton" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
				<i class="fab fa-twitter"></i><span>この記事をツイート</span>
			</a>
			</div>
			<div class="flex__item--3">
				<a class="single-sns__btn single-sns__btn--ad" href="http://dews365.com/adinfo" rel="nofollow">
				<i class="fab fa-adversal"></i> <span>広告掲載について</span>
				</a>
			</div>
		</div>
		<?php if( wp_is_mobile()) : ?>
			<!--  (300×250)DEWS_SPフッター	-->
			<div class="ads">
				<div class="ads__item">
				<script type="text/javascript" src="http://js.sprout-ad.com/t/293/572/a1293572.js"></script>
				</div>
			</div>
		<?php else : ?>
			<div class="ads flex flex--sp">
				<!--  (300×250)DEWS_PC記事下フッター左  -->
				<div class="ads__item">
				<script type="text/javascript" src="http://js.sprout-ad.com/t/293/575/a1293575.js"></script>
				</div>
				<!--  (300×250)DEWS_PC記事下フッター右 -->
				<div class="ads__item">
				<script type="text/javascript" src="http://js.sprout-ad.com/t/293/581/a1293581.js"></script>
				</div>
			</div>
		<?php endif; ?>

		
<div class="guno_columm">
  <div class="GUNO-ADSheader" style="">こちらもオススメ</div>
  <li class="GUNO-ADSarticleArea GUNO-ADSrc_b gunosyads-sdk-container guno-ads-_right" style="display:none;" gunosyads-sdk-frame-id="29335">
   <a href="${ url }" target="_blank">
      <div class="GUNO-ADSimgThum">
        <img gunosyads-sdk-src="${ small_image }">
      </div>
      <div class="GUNO-ADSarticleTitle">
        ${ title }
      </div>
      <div class="GUNO-ADSsiteTitle">
        [PR] ${ pr_sponsor_text }
      </div>
    </a>
  </li>
  <li class="GUNO-ADSarticleArea GUNO-ADSrc_b gunosyads-sdk-container guno-ads-_right" style="display:none;" gunosyads-sdk-frame-id="29335">
   <a href="${ url }" target="_blank">
      <div class="GUNO-ADSimgThum">
        <img gunosyads-sdk-src="${ small_image }">
      </div>
      <div class="GUNO-ADSarticleTitle">
        ${ title }
      </div>
      <div class="GUNO-ADSsiteTitle">
        [PR] ${ pr_sponsor_text }
      </div>
    </a>
  </li>
</div>
  
		</article>
	</div><!-- SECTION -->
	

<!-- 
<?php
	// PAGE NAVI
	$prevpost = get_adjacent_post(false, '', true);
	$nextpost = get_adjacent_post(false, '', false);
	if( $prevpost or $nextpost ):
	?>
	<div id="page-nav" class="entries">
	<ul class="grid grid-fill cf entries--list">
	<li class="grid__item--6 has-gutter">
	<?php if( $prevpost ):
	$prevthumb = get_the_post_thumbnail($prevpost->ID,'medium');
	$pattern= "/(?<=src=['|\"])[^'|\"]*?(?=['|\"])/i";
	preg_match($pattern, $prevthumb, $prevPath);
	$prevthumb = $prevPath[0];
	if(!$prevthumb){
		if(get_post_type() == 'movie'): $prevthumb = 'http://img.youtube.com/vi/'.get_post_meta($prevpost->ID,'VIDEOID',true).'/1.jpg';
		elseif(get_post_type() == 'music'): $prevthumb = esc_url(home_url('/')).'images/music/'.$prevpost->post_name.'/00.jpg';
		else: $prevthumb = get_template_directory_uri().'/images/noimg.jpg' ;
		endif;
	}
	?>
		<a href="<?php echo get_permalink($prevpost->ID); ?>">
			<div class="imageWrapper"><div class="image"><img src="<?php echo esc_url($prevthumb) ?>"></div></div>
			<div class="text"><p><span>&laquo; 前の記事</span><?php echo get_the_title($prevpost->ID); ?></p></div>
		</a>
	<?php endif; ?>
	</li>
	<li class="grid__item--6 has-gutter">
	<?php if( $nextpost ):
	$nextthumb = get_the_post_thumbnail($nextpost->ID,'medium');
	$pattern= "/(?<=src=['|\"])[^'|\"]*?(?=['|\"])/i";
	preg_match($pattern, $nextthumb, $nextPath);
	$nextthumb = $nextPath[0];
	if(!$nextthumb){
		if(get_post_type() == 'movie'): $nextthumb = 'http://img.youtube.com/vi/'.get_post_meta($nextpost->ID,'VIDEOID',true).'/1.jpg';
		elseif(get_post_type() == 'music'): $nextthumb = esc_url(home_url('/')).'images/music/'.$nextpost->post_name.'/00.jpg';
		else: $nextthumb = get_template_directory_uri().'/images/noimg.jpg';
		endif;
	}
	?>
		<a href="<?php echo get_permalink($nextpost->ID); ?>">
			<div class="text"><p><span>次の記事 &raquo;</span><?php echo get_the_title($nextpost->ID); ?></p></div>
			<div class="imageWrapper"><div class="image"><img src="<?php echo esc_url($nextthumb) ?>"></div></div>
		</a>
	<?php endif; ?>
	</li>
	</ul>
	</div>
<?php endif; ?> -->

<?php get_footer(); ?>