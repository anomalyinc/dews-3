<?php /* Template Name: FASHION */ ?>
<?php 
get_header(); 
the_post(); 
$pageslug = $page->post_name;
?>

<section class="entries">
	<h1 class="section--title"><span><?php the_title(); ?></span></h1>

	<ul class="entries--list list-middle grid grid-fill cf" >

	<?php 
		$paged = get_query_var('paged') ? get_query_var('paged') : 1;
		$args = array(
		'post_type' => array('fashionsnap','dancersgram') ,
		'posts_per_page' => 30 ,
		'paged' => $paged
		);

		$my_query = new WP_Query($args); if ($my_query->have_posts()):  
		while($my_query->have_posts()): $my_query->the_post();
		$costumf= get_post_custom( $post->ID );
		$team	= $costumf['TEAM'][0];
	?>

		<li class="grid__item--4 has-gutter" >
			<a href="<?php the_permalink(); ?>">
				<div class="imageWrapper"><div class="image"><?php if(has_post_thumbnail()): the_post_thumbnail("medium"); else: echo '<img src="' . get_template_directory_uri().'/images/noimg.jpg" alt="no image">'; endif; ?></div></div>	
				<div class="text"><time datetime="<?php the_time("c"); ?>"><?php the_time("Y.m.d"); ?></time><p><?php the_title(); ?><br><span><?php echo esc_html($team); ?></span></p></div>
			</a>
		</li>

	<?php endwhile; endif;?>

	</ul>

</section>

<div id="wpnav">
	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(array('query' => $my_query)); } ?>
</div>

<?php wp_reset_query(); ?>
<?php get_footer(); ?>