<?php /* Template Name: glaceausnap */ ?>
<?php 
get_header(); 
the_post(); 
$pageslug = $page->post_name;
?>

<section class="entries">


	<h1 class="section--title"><span><?php the_title(); ?></span></h1>

<div class="glaceauContents">
	<ul class="grid grid-fill cf " >

	<?php 
		$paged = get_query_var('paged') ? get_query_var('paged') : 1;
		$args = array(
		'post_type' => array('fashionsnap'),
		'tag__in' => array( 656 ),
		'posts_per_page' => 30 ,
		'paged' => $paged
		);

		$my_query = new WP_Query($args); if ($my_query->have_posts()):  
		while($my_query->have_posts()): $my_query->the_post();
		$costumf= get_post_custom( $post->ID );
		$team	= $costumf['TEAM'][0];?>

		<li class="grid__item--6 has-gutter" >
			<a href="<?php echo get_eyecatch_url('large'); ?>" rel="shadowbox[sbalbum-<?php echo $post->ID;?>]" title="<?php the_title(); ?>&nbsp;/&nbsp;<?php echo esc_html($team); ?>">
				<div class="imageWrapper"><div class="image"><?php if(has_post_thumbnail()): the_post_thumbnail("full"); else: echo '<img src="' . get_template_directory_uri().'/images/noimg.jpg" alt="no image">'; endif; ?></div></div>	
			</a>

    <?php 
		$images = get_children(array(
		'post_parent' => $post->ID,
		'post_type' => 'attachment',
		'exclude' => get_post_thumbnail_id($post->ID),
		'post_mime_type' => 'image',
		'order' => 'ASC',
		'orderby' => 'menu_order'
		));
		echo '<ul class="slide">';
		foreach ( $images as $attachment_id => $attachment ):
			$att_image_link = wp_get_attachment_link($attachment_id, 'full', true);	
			echo '<li>'.$att_image_link. '</li>';

		endforeach;
		echo '</ul>';

	?>
</li>


	<?php endwhile; endif;?>

	</ul>
</div>

</section>

<p>Photo by : AYATO.</p>

<p>#グラソー</p>
<div id="scroller">
  <ul id="instafeed" class="clearfix"></ul>
</div>

<p>グラソーヒタミンウォーターオフィシャルサイト : <a href="">http://glaceau.jp/</a><br>
Twitter : <a href="">https://twitter.com/glaceau_vw?lang=ja</a><br>
Instagram : <a href="">https://instagram.com/glaceau_jp/</a><br>
</p>


<div id="wpnav">
	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(array('query' => $my_query)); } ?>
</div>

<?php wp_reset_query(); ?>

<?php get_footer(); ?>