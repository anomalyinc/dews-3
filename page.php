<?php 
get_header(); 
the_post(); 
$page = get_page(get_the_ID());
$pageslug = $page->post_name;
?>

<section>
	<h1 class="section--title"><?php the_title(); ?></h1>
	<div class="entry">
	<?php the_content(); ?>
	</div>

	<div class="dal2016-thumnail">
		
	</div>
</section>

<?php get_footer(); ?>