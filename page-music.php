<?php /* Template Name: MUSIC */ ?>
<?php 
get_header(); 
the_post(); 
$pageslug = $page->post_name;
?>

<div id="maincol">
<div class="content-inner">

<section class="entries">
	<h1 class="section--title"><span class="icn icn-music"><?php the_title(); ?></span></h1>

	<ul class="entries--list list-small grid grid-fill cf" >

	<?php 
		$paged = get_query_var('paged') ? get_query_var('paged') : 1;
		$args = array(
		'post_type' => 'music' ,
		'posts_per_page' => 40 ,
		'paged' => $paged
		);

		$my_query = new WP_Query($args); if ($my_query->have_posts()):  
		while($my_query->have_posts()): $my_query->the_post();
	?>

		<li class="grid__item--6 has-gutter" >
			<a href="<?php the_permalink(); ?>">
				<div class="imageWrapper"><div class="image">
					<img src="<?php echo esc_url( home_url( '/' )).'images/music/'.esc_html($post->post_name).'/00.jpg'; ?>" alt="<?php the_title() ?>">
				</div></div>	
				<div class="text"><time datetime="<?php the_time("c"); ?>"><?php the_time("Y.m.d"); ?></time><p><?php the_title(); ?></p></div>
			</a>
		</li>

	<?php endwhile; endif;?>

	</ul>

</section>

<div id="wpnav">
	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(array('query' => $my_query)); } ?>
</div>

<?php wp_reset_query(); ?>

</div><!--end content-inner -->
</div><!--end maincol -->

<?php get_footer(); ?>