<?php 
get_header();

if(is_category()):
$info_now_cat = get_category( $cat );
$slug = $info_now_cat->slug;

else:
$slug = get_post_type();
endif;

?>

<div id="maincol">
<div class="content-inner">
	
<?php if(is_category('dblog')) : ?>
<div id="tab-container" class="tabContainer">
    <ul class="tab">
        <li class="tab__button"><a href="#blognew">NEW</a></li>
        <li class="tab__button"><a href="#blogrank">RANKING</a></li>
    </ul>
    <div class="panel-box">
  	    <div id="blognew">
  	        <section class="entries">
	    	    <ul class="entries--list">		
	    	   <?php  
			if ( have_posts() ) : while (have_posts()) : the_post();
			$cats = get_the_category();
			$catNameArray = array();
			$catSlugArray = array();
			foreach($cats as $category ) { 
				if(($category->category_nicename != "news") && ($category->category_nicename != "topics") && ($category->category_nicename != "special")){
					$catNameArray[] .= $category->cat_name;
					$catSlugArray[] .= $category->category_nicename;
				}
			} 
			if(!$catNameArray[0]){
				$catName = "その他";
				$catSlug = "other";
			}else{
				$catName = $catNameArray[0];
				$catSlug = $catSlugArray[0];
			}

			if(get_post_type() == 'event'): 
				$date = post_custom("DATE");
				$t_date = mb_strimwidth ($date, 0, 10);
				$t_date = explode(".", $t_date);
			endif;
		?>
	    		    <li>
	    			    <a href="<?php the_permalink(); ?>">
	    				<div class="imageWrapper">
	    				<div class="image">
	    					<?php if(has_post_thumbnail()): ?>
	    					<?php the_post_thumbnail("medium"); ?>
	    					<?php else: ?>
	    					<img src="<?php echo get_template_directory_uri(); ?>/images/noimg.jpg" alt="no img">
	    					<?php endif; ?>
	    					<?php if(get_post_type() == 'post'): ?><span class="cat other"><?php echo esc_html($catName); ?></span><?php endif;  ?>
	    				</div>
	    				</div>
	    				<div class="text">
	    						<time datetime="<?php the_time("c"); ?>"><?php the_time("Y.m.d"); ?></time>
	    						<?php if($post->post_author) {
	    							if(is_category('dewmo'))
	    							echo 'author : '.get_the_author();  
	    						} ?>
	    					<p><?php the_title(); ?></p>	
					    </div>
				        </a>
			        </li>
		        <?php endwhile; endif; ?>
		        </ul>
	        </section>
	        <div id="wpnav">
		    <?php if(!(is_home()) && (function_exists('wp_pagenavi'))) { wp_pagenavi(); } ?>
	        </div>   
	    </div>

        <div id="blogrank">
          <section class="entries cf">
               <?php global $custom_anomaly_ranking;?>
               <?php $custom_anomaly_ranking->show('weekly','10','1437,1909,2477','1'); ?>
           </section>
        </div>
    </div>
</div>

<?php else: ?>
	<section class="entries">
		<ul class="entries--list">		
		<?php  
			if ( have_posts() ) : while (have_posts()) : the_post();
			$cats = get_the_category();
			$catNameArray = array();
			$catSlugArray = array();
			foreach($cats as $category ) { 
				if(($category->category_nicename != "news") && ($category->category_nicename != "topics") && ($category->category_nicename != "special")){
					$catNameArray[] .= $category->cat_name;
					$catSlugArray[] .= $category->category_nicename;
				}
			} 
			if(!$catNameArray[0]){
				$catName = "その他";
				$catSlug = "other";
			}else{
				$catName = $catNameArray[0];
				$catSlug = $catSlugArray[0];
			}

			if(get_post_type() == 'event'): 
				$date = post_custom("DATE");
				$t_date = mb_strimwidth ($date, 0, 10);
				$t_date = explode(".", $t_date);
			endif;
		?>
			<li>
				<a href="<?php the_permalink(); ?>">

					<div class="imageWrapper"><div class="image">
						<?php if(has_post_thumbnail()): ?>
						<?php the_post_thumbnail("medium"); ?>
						<?php elseif(get_post_type() == 'movie'): ?>
						<img src="http://img.youtube.com/vi/<?php echo esc_html(post_custom('VIDEOID')); ?>/0.jpg" alt="<?php the_title(); ?>" >
						<?php else: ?>
						<img src="<?php echo get_template_directory_uri(); ?>/images/noimg.jpg" alt="no img">
						<?php endif; ?>
						<?php if(get_post_type() == 'post'): ?><span class="cat other"><?php echo esc_html($catName); ?></span><?php endif;  ?>
					</div></div>
					<div class="text">
						<?php if(get_post_type() == 'event'):  ?>
						<time datetime="<?php echo $date; ?>">開催日 : <?php echo esc_html($t_date[0]).'.'. esc_html($t_date[1]).'.'. esc_html($t_date[2]); ?></time>
						<?php else:  ?>
							<time datetime="<?php the_time("c"); ?>"><?php the_time("Y.m.d"); ?></time>
							<?php if($post->post_author) {
								if(is_category('dewmo'))
								echo 'author : '.get_the_author();  
							} ?>
						<?php endif;  ?>
						<p><?php the_title(); ?></p>	
					</div>
				</a>
			</li>

		<?php endwhile; endif; ?>
		</ul>
		
	</section>


	<div id="wpnav">
		<?php if(!(is_home()) && (function_exists('wp_pagenavi'))) { wp_pagenavi(); } ?>
	</div>

	
	<?php if(is_author()): ?>
	<div id="authorbx" class="bx">
		<div class="authorname"><?php echo get_avatar( get_the_author_id(), $size = '40'); ?><div><span>著者 : </span><?php the_author_meta( dname, $author ); ?><br><span><a href="<?php the_author_meta( user_url, $author ); ?>" target="_blank" rel="nofollow"><?php the_author_meta( user_url, $author ); ?></a></span></div></div>
		<p><?php the_author_meta( description, $author ); ?></p>
	</div>
	<?php endif; ?>

<?php endif; ?>
</div><!--end content-inner -->
</div><!--end maincol -->

<?php get_footer(); ?>