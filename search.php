<?php 
get_header();

global $query_string;
query_posts($query_string . "&post_type=post");

?>

<div id="maincol">
<div class="content-inner">
	
	<section class="entries">
		<h1 class="section--title">検索結果</h1>

		<div style="font-size:1.4em;margin-bottom: 24px;">
			<p>検索キーワード : <?php the_search_query();?></p>
		</div>

		<?php  if ( have_posts() ) : while (have_posts()) : the_post(); ?>
		<ul class="entries--list">		
		<?php  
			$cats = get_the_category();
			$catNameArray = array();
			$catSlugArray = array();
			foreach($cats as $category ) { 
				if(($category->category_nicename != "news") && ($category->category_nicename != "topics") && ($category->category_nicename != "special")){
					$catNameArray[] .= $category->cat_name;
					$catSlugArray[] .= $category->category_nicename;
				}
			} 
			if(!$catNameArray[0]){
				$catName = "その他";
				$catSlug = "other";
			}else{
				$catName = $catNameArray[0];
				$catSlug = $catSlugArray[0];
			}

			if(get_post_type() == 'event'): 
				$date = post_custom("DATE");
				$t_date = mb_strimwidth ($date, 0, 10);
				$t_date = explode(".", $t_date);
			endif;
		?>
			<li>
				<a href="<?php the_permalink(); ?>">

					<div class="imageWrapper"><div class="image">
						<?php if(has_post_thumbnail()): ?>
						<?php the_post_thumbnail("medium"); ?>
						<?php elseif(get_post_type() == 'movie'): ?>
						<img src="http://img.youtube.com/vi/<?php echo esc_html(post_custom('VIDEOID')); ?>/0.jpg" alt="<?php the_title(); ?>" >
						<?php else: ?>
						<img src="<?php echo get_template_directory_uri(); ?>/images/noimg.jpg" alt="no img">
						<?php endif; ?>
						<?php if(get_post_type() == 'post'): ?><span class="cat <?php echo esc_html($catSlug); ?>"><?php echo esc_html($catName); ?></span><?php endif;  ?>
					</div></div>
					<div class="text">
						<?php if(get_post_type() == 'event'):  ?>
						<time datetime="<?php echo $date; ?>">開催日 : <?php echo esc_html($t_date[0]).'.'. esc_html($t_date[1]).'.'. esc_html($t_date[2]); ?></time>
						<?php else:  ?>
						<time datetime="<?php the_time("c"); ?>"><?php the_time("Y.m.d"); ?></time>
						<?php endif;  ?>
						<p><?php the_title(); ?></p>	
					</div>
				</a>
			</li>

		<?php endwhile;?>
		</ul>
		<?php else: ?>

			<div class="entry"><p>キーワードは見つかりません。</p></div>

		<?php endif; ?>
		
	</section>

	<div id="wpnav">
		<?php if(!(is_home()) && (function_exists('wp_pagenavi'))) { wp_pagenavi(); } ?>
	</div>

</div><!--end content-inner -->
</div><!--end maincol -->

<?php get_footer(); ?>
	