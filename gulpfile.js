var gulp = require('gulp'),
    autoPre = require("gulp-autoprefixer"), // ベンダープレフィックス
    bSync = require('browser-sync'), // ライブリロード
    plumber = require('gulp-plumber'), // 強制終了防止
    connect = require('gulp-connect-php'),
    sass = require('gulp-sass') // Sass

;


// ライブリロード
gulp.task('browser-sync', function() {
    bSync({
        server: {
            baseDir: "./html"
        }
    });
});
gulp.task('bs-reload', function() {
    bSync.reload()
});


// Sass, ベンダープレフィックス
gulp.task('sass', function() {
    gulp.src('scss/**/*.scss')
        .pipe(plumber())
        .pipe(sass({ outputStyle: 'expanded' }))
        .pipe(autoPre({
            browsers: ['last 2 versions', 'ie 10'],
            cascade: false
        }))
        .pipe(gulp.dest('./css'))
        .pipe(bSync.reload({ stream: true }));
});


// HTML
gulp.task('html', function() {
    gulp.src('./html/**/*.html')
        .pipe(plumber())
        .pipe(gulp.dest('./html/**/*.html'))
        .pipe(bSync.reload({ stream: true }));
});


// JavaScript
gulp.task('js', function() {
    gulp.src('./html/**/*.js')
        .pipe(plumber())
        .pipe(gulp.dest('./html/js'))
        .pipe(bSync.reload({ stream: true }));
});


gulp.task('connect-sync', function() {
  connect.server({
    port:8888,
    base:'www'
  }, function (){
    browserSync({
      proxy: 'localhost:8001'
    });
  });
});


// ファイル監視
gulp.task('default', ['browser-sync'], function() {
    gulp.watch("scss/**/*.scss", ['sass']);
    gulp.watch("html/**/*.html", ['bs-reload']);
    gulp.watch("js/**/*.js", ['bs-reload']);
    gulp.watch("./*.php",["bs-reload"]);
});
