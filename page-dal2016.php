<?php /* Template Name: dal2016 */ ?>
<?php 
get_header("dal2016"); 
the_post(); 
$pageslug = $page->post_name;
?>



<section class="entries">
	
<?php if(has_post_thumbnail()): ?><?php endif; ?>
	<div class="entry dal-2016-contents">
	<?php the_content(); ?>
	</div>


</section>

<?php wp_reset_query(); ?>

<?php get_footer("dal2016"); ?>