/*
 * autoheight.js
 *
 * Copyright (c) 2015 Satoru Mizuno
 *
 * Since:   2015-09-01
 * Update:  2016-01-19
 * version: 1.02
 *
 */

(function($){
jQuery.fn.autoHeight = function(options){

    var options = jQuery.extend({
        column : 0,
        item : '',
        parts: {},
        responsive : false,
        responsiveWidth : 768,
        spCol : 0
    }, options);

    var elm = this;
    var item = this.find(options.item);
    var partsArray = options.parts;

    setBox();

    var ua = navigator.userAgent;
    var isIE6  = ua.match(/msie [6.]/i),
        isIE7  = ua.match(/msie [7.]/i);

    if(item[0]){
        if(isIE6 || isIE7) return false;
        var autoHeightTimer = null;
        $(window).on('load resize orientationchange', function(){
            clearTimeout(autoHeightTimer);
            autoHeightTimer = setTimeout(function() {
                setBox();
            }, 300);
        })
    };

    function setBox() {

        elm.each(function(){
            item = $(this).find(options.item);

            if(options.spCol && (options.responsiveWidth > $(window).width())){
                var gridColumn = options.spCol;
            }else{
                var gridColumn = options.column
            };

            if(!(options.responsive) || (options.responsive && options.responsiveWidth <= $(window).width())){

                jQuery.each(partsArray , function(index) {

                    var itemLength = item.length,
                        line = Math.ceil(itemLength / gridColumn),
                        itemHightArr = [],
                        lineHightArr =[];

                    item.find(partsArray[index]).css('height','auto');

                    for (i = 0; i <= itemLength; i++) {
                        if(item.eq(i).find(partsArray[index]).css('box-sizing') == 'border-box') {
                            itemHightArr[i] = item.eq(i).find(partsArray[index]).outerHeight();
                        }else{
                            itemHightArr[i] = item.eq(i).find(partsArray[index]).height();
                        }

                        if (lineHightArr.length <= Math.floor(i / gridColumn)
                            || lineHightArr[Math.floor(i / gridColumn)] < itemHightArr[i])
                        {
                            lineHightArr[Math.floor(i / gridColumn)] = itemHightArr[i];
                        }
                    }
                    item.each(function(i){
                        item.eq(i).find(partsArray[index]).css('height',lineHightArr[Math.floor(i / gridColumn)] + 'px');
                    });

                });

                var itemLength = item.length,
                    line = Math.ceil(itemLength / gridColumn),
                    itemHightArr = [],
                    lineHightArr =[];
                item.css('height','auto');

                for (i = 0; i <= itemLength; i++) {
                    if((item.eq(i)).css('box-sizing') == 'border-box') {
                        itemHightArr[i] = item.eq(i).outerHeight();
                    }else{
                        itemHightArr[i] = item.eq(i).height();
                    }
                    if (lineHightArr.length <= Math.floor(i / gridColumn)
                        || lineHightArr[Math.floor(i / gridColumn)] < itemHightArr[i])
                    {
                        lineHightArr[Math.floor(i / gridColumn)] = itemHightArr[i];
                    }
                }
                item.each(function(i){
                    $(this).css('height',lineHightArr[Math.floor(i / gridColumn)] + 'px');
                });

            }else{
                item.css('height','auto');
                jQuery.each(partsArray , function(index) {
                    item.find(partsArray[index]).css('height','auto');
                });
            }
        })
    }
}
})(jQuery);
