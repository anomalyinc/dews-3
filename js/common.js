
$(function() {
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: 3000,
        autoplayDisableOnInteraction: false
    });
});


jQuery(document).ready(function() {

    flag = 0;
    jQuery("#menubtn").click(function() {
        if (!flag) {
            flag = 1;
            $("#menubtn").css({ opacity: 0.5 });
            $("#header,#mainContent,#sub-adarea,#special,#rankingBx,#footer").animate({ left: "262px" });
            $("#sub-navigation").animate({ left: "0" });
        } else {
            flag = 0;
            $("#menubtn").css({ opacity: 1 });
            $("#header,#mainContent,#sub-adarea,#special,#rankingBx,#footer").animate({ left: "0" });
            $("#sub-navigation").animate({ left: "-262px" });
        }
    });


    // $('a.scroll[href^=#]').click(function() {
    //     var speed = 400;
    //     var href = $(this).attr("href");
    //     var target = $(href == "#" || href == "" ? 'html' : href);
    //     var position = target.offset().top;
    //     $('body,html').animate({ scrollTop: position }, speed, 'swing');
    //     return false;
    // });

    jQuery('.formContent h3').click(function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('+ .formContent--wd:not(:animated)', this).slideUp('fast');
        } else {
            $('.formContent h3').removeClass('active');
            $('.formContent--wd:not(:animated)').slideUp('fast');
            $(this).toggleClass('active');
            $('+ .formContent--wd', this).slideToggle('fast');
        }
    });

    jQuery('#login--btn').click(function() {
        $(this).next('div').animate({ height: 'toggle' });
    });

    jQuery(".entry_head").click(function() {
        $(this).next(".entry_list").toggle("fast");
    });

});



// //GOOGLE MAP
// function googleMap() {
//     var latlng = new google.maps.LatLng(35.663946, 139.67599);
//     var myOptions = {
//         zoom: 16,
//         center: latlng,
//         scrollwheel: false,
//         mapTypeControlOptions: { mapTypeIds: ['Map', google.maps.MapTypeId.ROADMAP] }
//     };
//     var map = new google.maps.Map(document.getElementById('mapCanvas'), myOptions);

//     var icon = new google.maps.MarkerImage('http://www.anomaly.co.jp/wp/wp-content/themes/anomaly_ver4/images/pin.png',
//         new google.maps.Size(50, 64),
//         new google.maps.Point(0, 0),
//         new google.maps.Point(25, 64)
//     );

//     var markerOptions = {
//         position: latlng,
//         map: map,
//         icon: icon,
//         title: 'ANOMALY INC.'
//     };

//     var marker = new google.maps.Marker(markerOptions);

//     /* スタイル付き地図 */
//     var styleOptions = [{ "featureType": "landscape", "stylers": [{ "saturation": -100 }, { "lightness": 65 }, { "visibility": "on" }] }, { "featureType": "poi", "stylers": [{ "saturation": -100 }, { "lightness": 51 }, { "visibility": "simplified" }] }, { "featureType": "road.highway", "stylers": [{ "saturation": -100 }, { "visibility": "simplified" }] }, { "featureType": "road.arterial", "stylers": [{ "saturation": -100 }, { "lightness": 30 }, { "visibility": "on" }] }, { "featureType": "road.local", "stylers": [{ "saturation": -100 }, { "lightness": 40 }, { "visibility": "on" }] }, { "featureType": "transit", "stylers": [{ "saturation": -100 }, { "visibility": "simplified" }] }, { "featureType": "administrative.province", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "labels", "stylers": [{ "visibility": "on" }, { "lightness": -25 }, { "saturation": -100 }] }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "hue": "#ffff00" }, { "lightness": -25 }, { "saturation": -97 }] }];
//     var styledMapOptions = { name: 'black' }
//     var lopanType = new google.maps.StyledMapType(styleOptions, styledMapOptions);
//     map.mapTypes.set('Map', lopanType);
//     map.setMapTypeId('Map');

// };

/* box固定(satoru) */
$(function() {
    $('.sp-grid').autoHeight({
        responsive: false,
        item: '.sp-grid__item',
        parts: ['.sp-text'],
        column: 4
    });
});

/* SPメニューバーしたシェアボタン */
// $(function() {
// var showFlag = false;
// var topBtn = $('#sharenav_sp');
// topBtn.css('top', '-100px');
// var showFlag = false;
// //スクロールが100に達したらボタン表示
// $(window).scroll(function() {
//     if ($(this).scrollTop() > 700) {

//         if (showFlag == false) {
//             showFlag = true;
//             topBtn.stop().animate({ 'top': '32px' }, 1000);
//         }
//     } else {
//         if (showFlag) {
//             showFlag = false;
//             topBtn.stop().animate({ 'top': '-100px' }, 1000);
//         }
//     }
// });

$(function() {
    if ($('.app').length) {
        var nav = $('.app');
        var sub = $('#sub1');
        //navの位置    
        var navTop = nav.offset().top;
        var subTop = sub.offset().top;
        //スクロールするたびに実行
        $(window).scroll(function() {
            var winTop = $(this).scrollTop();
            //スクロール位置がnavの位置より下だったらクラスfixedを追加
            if (winTop >= navTop) {
                nav.addClass('fixed-app')
            } else if (winTop <= navTop || winTop <= subTop) {
                nav.removeClass('fixed-app')
            }
        });
    }
});



$(document).ready(function() {
    $('#tab-container').easytabs();
});

(adsbygoogle = window.adsbygoogle || []).push({});

$(function() {
    var showFlag = false;
    var topBtn = $('.animate-fixed');
    topBtn.css('bottom', '-100px');
    var showFlag = false;
    //スクロールが100に達したらボタン表示
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            if (showFlag == false) {
                showFlag = true;
                topBtn.stop().animate({ 'bottom': '0' }, 200);
            }
        } else {
            if (showFlag) {
                showFlag = false;
                topBtn.stop().animate({ 'bottom': '-100px' }, 200);
            }
        }
    });

    // #で始まるアンカーをクリックした場合に処理
    $('a[href^=#]').click(function() {
        // スクロールの速度
        var speed = 400; // ミリ秒
        // アンカーの値取得
        var href = $(this).attr("href");
        // 移動先を取得
        var target = $(href == "#" || href == "" ? 'html' : href);
        // 移動先を数値で取得
        var position = target.offset().top;
        // スムーススクロール
        $('body,html').animate({ scrollTop: position }, speed, 'swing');
        return false;
    });
});



var $win = $(window),
    $cloneNav = $('#social').clone().addClass('clone-nav').appendTo('body'),
    showClass = 'social--show';

$win.on('load scroll', function() {
    var value = $(this).scrollTop();
    if (value > 700) {
        $cloneNav.addClass(showClass);
    } else {
        $cloneNav.removeClass(showClass);
    }
});