<?php
ob_end_clean();
/**
 * RSS2 Feed Template for displaying RSS2 Posts feed.
 *
 * @package WordPress
 */

header('Content-Type: ' . feed_content_type('rss-http') . '; charset=' . get_option('blog_charset'), true);
$more = 1;

echo '<?xml version="1.0" encoding="utf-8" ?><YJNewsFeed Version="1.0">';

/**
 * Fires between the xml and rss tags in a feed.
 *
 * @since 4.0.0
 *
 * @param string $context Type of feed. Possible values include 'rss2', 'rss2-comments',
 *                        'rdf', 'atom', and 'atom-comments'.
 */
do_action( 'rss_tag_pre', 'rss2' );
?>

<Identification>
	<MediaId>MediaId</MediaId>
	<DateId>20110221</DateId>
	<NewsItemId>00000001</NewsItemId>
	<RevisionId>1</RevisionId>
</Identification>


<rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	xmlns:oa="http://news.line.me/rss/1.0/oa"
	<?php
	/**
	 * Fires at the end of the RSS root to add namespaces.
	 *
	 * @since 2.0.0
	 */
	do_action( 'rss2_ns' );
	?>
>
<channel>
	<title>ダンス情報サイト「Dews  (デュース)」</title>
	<link><?php bloginfo_rss('url') ?></link>
	<description><?php bloginfo_rss("description") ?></description>
  	<language>ja</language>
  	<copyright>Copyright © Dews All Rights Reserved.</copyright>
  	<lastBuildDate>Mon, 22 Nov 2017 20:00:00 +0900</lastBuildDate>
	<?php while( have_posts()) : the_post(); ?>
<?php 
	$custom_fields_release = get_post_meta( get_the_ID() , 'linenews' , true ); 
	$thumbnail_id = get_post_thumbnail_id(); 
	$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
	$content = get_the_content('','false');
	$content = wp_strip_all_tags($content);
	$content = strip_shortcodes($content);
	$content = preg_replace("/\n/","<br />",$content); 
?>
	<?php if ($custom_fields_release){ ?>
		<item>
			<guid isPermaLink="false"><?php the_guid(); ?></guid>
			<title><![CDATA[<?php the_title_rss() ?> ]]></title>
			<link><?php the_permalink_rss() ?></link>
			<description>
			<![CDATA[<?php the_content(); echo "\n"; ?>]]>
			</description>
			<enclosure url="<?php echo the_post_thumbnail_url( 'full' ); ?>" type="image/jpeg" />
			<pubDate><?php echo mysql2date('D, d M Y H:i:s +0900', get_post_time('Y-M-d H:i:s', true), false); ?></pubDate>
			<oa:imgAuthor>
			<?php if(post_custom('source')): ?>
			<oa:authorName><![CDATA[ <?php echo post_custom('source'); ?> ]]></oa:authorName>
			<?php endif; ?>
			<?php if(post_custom('source_url')): ?>
			<oa:authorUrl><?php echo post_custom('source_url'); ?></oa:authorUrl>
			<?php endif; ?>
			</oa:imgAuthor>
			<oa:lastPubDate><?php echo get_post_modified_time('D, d M Y H:i:s +0900');?></oa:lastPubDate>
			<oa:reflink>
			<oa:refTitle><![CDATA[ <?php echo post_custom('releated_title'); ?> ]]></oa:refTitle>
			<oa:refUrl><?php echo post_custom('releated_links'); ?></oa:refUrl>
			<?php
				$categories = wp_get_post_categories($post->ID, array('orderby'=>'rand')); 
				if ($categories) {
					$args = array(
						'category__in' => array($categories[0]), // カテゴリーのIDで記事を取得
						'category__not_in' => array( 2861 ),
						'post__not_in' => array($post->ID), // 表示している記事を除く
						'showposts'=>2, // 取得記事数
						'caller_get_posts'=>1, // 取得した記事の何番目から表示するか
						'orderby'=> 'rand' // 記事をランダムで取得
					); 
					$my_query = new WP_Query($args); 
					if( $my_query->have_posts() ) { 
						$thumbnail_id = get_post_thumbnail_id(); 
						?>
						<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
							<oa:refTitle><![CDATA[ <?php the_title(); ?> ]]></oa:refTitle>
							<oa:refUrl><?php the_permalink(); ?></oa:refUrl>
						<?php endwhile;
					}
					wp_reset_query();
				}
			?>
			
			</oa:reflink> 

<!-- 			<oa:delStatus></oa:delStatus>    -->        
			<oa:category>12</oa:category>å

		<?php rss_enclosure(); ?>
		</item>
	<?php } ?>
	<?php endwhile; ?>
</channel>
</rss>