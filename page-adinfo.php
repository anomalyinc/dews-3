<?php /* Template Name: ADINFO */ ?>
<?php 
get_header(); 
the_post(); 
$pageslug = $page->post_name;
?>


            <div class="adinfo">
                <div class="adinfo__top">
                    <h2 class="adinfo__head">Dews 広告掲載について</h2>
                </div>
                <div id="adabout" class="adinfo-about" >
                    <div class="inner">
                        <div class="adinfo-about__text">
                            <h3 class="adinfo-about__head">About
                            <br><img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/ad_logo.png" alt=""></h3>
                            <p>2014年より始動したダンスニュースメディア「Dews」は、ダンスにまつわる様々な情報を取り上げてきました。
                                <br>過去には、ダンス系メジャーアーティストのインタビューから女子高生の流行ダンス、アンダーグラウンドのカルチャー事情などダンスというワードを元に幅広く取材し、現在では月間ユニークユーザー40万を超え、業界最大のメディアとしてダンサーから支持されています。
                                <br>
                                <br><span class="bold">十代のダンサーやダンスファンへのアプローチが可能</span>
                                <br>Dewsは十代のダンサー、ダンスファンが多く、ファッション、音楽など最新トレンドへの情報感度が高いユーザーが多く存在します。“ダンス × ◯◯◯”の最適なPR方法を共に考え、新感覚なアプローチを生み出しましょう！</p>
                         <!--    <img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/ad_about.jpg"> -->
                        </div>
                    </div>
                </div>
                <div id="admenu" class="adinfo__menu">
                    <h3 class="adinfo__title">Dews 広告メニュー</h3>
                    <p class="adinfo__text">
                        Dewsでは、お客様のご要望に合わせてフレキシブルに広告メニューを展開しています。 記事広告、バナー広告はもちろんのこと、サイト全体を商品のカラーにするジャック広告や年々増加するダンス系キャンペーンなどを企画段階から打ち合わせし、キャンペーン自体の運用も可能です。
                    </p>
                    <ul class="adinfo__list">
                        <li class="adinfo__item"><a href="#post" title="記事広告">記事広告　<i class="fa fa-caret-down" aria-hidden="true"></i></a></li>
                        <li class="adinfo__item"><a href="#banner" title="バナー広告">バナー広告　<i class="fa fa-caret-down" aria-hidden="true"></i></a></li>
                        <li class="adinfo__item"><a href="#jack" title="ジャック広告">ジャック広告　<i class="fa fa-caret-down" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                
                <section class="adinfo-menu" id="post">
                    <h3 class="adinfo-menu__head">記事広告<br><span>ネイティブアドで記事制作</span></h3>
                    <div class="adinfo-menu__about">
                        <div class="inner adinfo-menu__wrap">
                            <div class="adinfo-menu__item adinfo-menu__item--right">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/ad_menu01.png" class="">
                            </div>
                            <div class="adinfo-menu__item adinfo-menu__item--left adinfo-menu__item--text">
                                <p>記事広告は、御社の商品やサービスをダンサーとマッチさせ記事を制作いたします。
ユーザーはダンサー、ダンスファンが多くを占めるので、興味関心を持つような企画を提案し、コンテンツの一つとして読者に訴求いたします。多くのダンサーとの繋がりを活かし、ダンサーを起用した記事制作も可能です。また、予め企画や素材をご用意いただければ、低予算で出稿も可能です。</p>
                            </div>
                        </div>
                    </div>
                    <div class="inner">
                        <h3 class="adinfo-menu__subhead">記事広告の事例</h3>
                        <div class="flex adinfo-menu__post">
                            <div class="flex__item--3">
                                <a href="http://dews365.com/news/special/koshien-dance.html" title="" target="_blank">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/ad_post09.jpg">
                                    <h4>【＃高校野球ダンスコンテスト】負けたくないのは球児だけじゃない。高校野球を盛り上げる熱いダンスを募集中！<br><span>CL : 朝日新聞社</span></h4>
                                    </a>
                            </div>
                            <div class="flex__item--3">
                                <a href="http://dews365.com/topics/lol-byebye-interview.html" title="" target="_blank">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/ad_post02.jpg">
                                    <h4>失恋エピソードを大暴露！lol-エルオーエル-「 bye bye」リリース記念インタビュー<br><span>CL : エイベックス・グループ・ホールディングス株式会社</span></h4>
                                    </a>
                            </div>
                            <div class="flex__item--3">
                                <a href="http://dews365.com/topics/world-of-red-bull.html" title="" target="_blank">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/ad_post03.jpg">
                                    <h4>Red Bull “偉大なる小さな一歩”キャンペーンスタート！B-Boy ISSEIとマンツーマントレーニングができるチャレンジプログラムも実施！<br><span>CL : レッドブル・ジャパン株式会社</span></h4>
                                    </a>
                            </div>
                            <div class="flex__item--3">
                                <a href="http://dews365.com/news/special/mogood-01.html" title="" target="_blank">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/ad_post04.jpg">
                                    <h4>マッチングアプリって本当に女の子と出会えるの！？飲み会やイベントを開催できる「mogood」リニューアルパーティにMCまーくんが潜入！<br><span>CL : 株式会社ユニゾンベックス</span></h4>
                                    </a>
                            </div>
                            <div class="flex__item--3">
                                <a href="http://dews365.com/news/moussy-studiowear.html" title="" target="_blank">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/ad_post05.jpg">
                                    <h4>女性ダンサーMiu Ideが登場！「MOUSSY」の 新ライン「MOUSSY STUDIOWEAR」イメージムービーが公開<br><span>CL : バロックジャパンリミテッド</span></h4>
                                    </a>
                            </div>
                            <div class="flex__item--3">
                                <a href="http://dews365.com/topics/stkz-msp-workshop.html" title="" target="_blank">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/ad_post06.jpg">
                                    <h4>s**t kingzがワークショップで全国行脚！MOTION SONIC PROJECT × s**t kingz情報解禁<br><span>CL : ソニー株式会社</span></h4>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="adinfo-menu" id="banner">
                    <h3 class="adinfo-menu__head">バナー広告<br><span>バナーデザインから可能</span></h3>
                    <div class="adinfo-menu__about">
                        <div class="inner adinfo-menu__wrap">
                            <div class="adinfo-menu__item adinfo-menu__item--img adinfo-menu__item--left">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/ad_menu02.png">
                            </div>
                            <div class="adinfo-menu__item adinfo-menu__item--right adinfo-menu__item--text">
                                <p>Dewsには全ページのトップに表示させるトップバナー、記事のトップに表示させる記事バナー、サイドに表示させるレクタングルバナーをはじめ、お客様のご要望に応じて広告を設置することが可能です。こちらもお素材をご提供頂く低予算プランから弊社デザイナーが最適な形でデザインし設置するプランと予算に応じて出稿が可能です。</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="adinfo-menu" id="jack">
                    <h3 class="adinfo-menu__head">ジャック広告<br><span>大胆にサイト全体をジャック！</span></h3>
                    <div class="adinfo-menu__about">
                        <div class="inner adinfo-menu__wrap">
                            <div class="adinfo-menu__item adinfo-menu__item--img adinfo-menu__item--right">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/ad_menu03.png">
                            </div>
                            <div class="adinfo-menu__item adinfo-menu__item--left adinfo-menu__item--text">
                                <p>ジャック広告とはサイト全体をブランドカラーに変更したり、全バナーの設置、動画の配置などご要望に応じて、サイト全体をジャックするプランです。インパクトの強い広告効果があり、多くのダンサーへ訴求を図ります。過去に行ったジャック広告がpdfにて、ダウンロード可能ですので下記をご参考ください。</p>
                            </div>
                        </div>
                    </div>
                    <div class="inner adinfo-menu__wrap">
                        <h3 class="adinfo-menu__subhead">記事広告の事例</h3>
                        <div class="flex adinfo-menu__post">
                            <div class="flex__item--3">
                                <a href="<?php echo get_template_directory_uri(); ?>/pdf/glaceau.pdf" title="" target="_blank">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/ad_post08.jpg">
                                    <h4>Dews × グラソー ビタミンウォーター<br><span>CL : 日本コカ･コーラ株式会社</span></h4>
                                    </a>
                            </div>
                            <div class="flex__item--3">
                                <a href="<?php echo get_template_directory_uri(); ?>/pdf/nissin.pdf" title="" target="_blank">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/ad_post03.jpg">
                                    <h4>Dews × Red Bull “偉大なる小さな一歩”キャンペーン<br><span>CL : レッドブル・ジャパン株式会社</span></h4>
                                    </a>
                            </div>
                            <div class="flex__item--3">
                                <a href="<?php echo get_template_directory_uri(); ?>/pdf/redbull.pdf" title="" target="_blank">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/ad_post01.jpg">
                                    <h4>Dews × カップヌードル<br><span>CL : 日清食品株式会社</span></h4>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="adcontact" class="adinfo-contact adinfo__menu" >
                    <h3 class="adinfo__title">お問合わせ</h3>
                    <p class="adinfo__text">
                       以上3種類の広告をベースに様々な広告を展開しています。<br>お悩みの方、まずお気軽にご相談下さい。御社の商品でダンスシーンを盛り上げましょう！
                    </p>
                    <div class="inner">
                       <?php the_content(); ?>
                    </div>
                </section>
            </div>

            <div class="adinfo-footer animate-fixed">
				<ul class="adinfo-footer__list">
					<li class="adinfo-footer__item">
					<a href="#adabout" title="Dewsについて"><span><img src="<?php echo get_template_directory_uri(); ?>/images/adinfo/icon.png" alt="" width="15px"></span><br>メディアについて</a>
					</li>
                    <li class="adinfo-footer__item">
                    <a href="#admenu" title="広告メニュー"><span><i class="fa fa-file-image-o" aria-hidden="true"></i></span><br>広告メニュー</a>
                    </li>
                    <li class="adinfo-footer__item">
                    <a href="#adcontact" title="お問合わせ"><i class="fa fa-envelope" aria-hidden="true"></i><br>お問合わせ</a>
                    </li>
                </ul>
             </div>
        </div>
        <!--end main -->

<?php get_footer(); ?>