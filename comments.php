
    <div class="comments">
        <?php if( have_comments() ){ ?>
        <div class="section--title">COMMENT<br><span><i class="fa fa-twitter" aria-hidden="true"></i> みんなの声</span></div>
        <ol class="comments__list">
            <?php wp_list_comments(); ?>
        </ol>
        <?php } ?>
    </div>
