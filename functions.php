<?php

/* ok music rss set
---------------------------------------------------- */

//RSSをテーマ内から読み込む

function custom_post_rss_set($query) {
  if ( is_feed() ) {
    $post_type = $query->get('post_type');
    if ( empty($post_type) ){
      $query->set( 'post_type',
        array(
          'post',
          'movie',
          'event'
        )
      );
    }
    return $query;
  }
}

add_filter( 'pre_get_posts', 'custom_post_rss_set' );



function my_replace_amp($content) {
    return str_replace('&#038;', '&', $content);
}
  
add_filter('the_content', 'my_replace_amp');



add_action( 'do_feed_smartnews', 'do_feed_smartnews' );
function do_feed_smartnews() {
$feed_template = get_template_directory() . '/smartnews.php';
load_template( $feed_template );
}

add_action( 'do_feed_okmusic', 'do_feed_okmusic' );
function do_feed_okmusic() {
$feed_template = get_template_directory() . '/okmusic.php';
load_template( $feed_template );
}

add_action( 'do_feed_gunosy', 'do_feed_gunosy' );
function do_feed_gunosy() {
$feed_template = get_template_directory() . '/gunosy.php';
load_template( $feed_template );
}

add_action( 'do_feed_sportsbull', 'do_feed_sportsbull' );
function do_feed_sportsbull() {
$feed_template = get_template_directory() . '/sportsbull.php';
load_template( $feed_template );
}

add_action( 'do_feed_linenews', 'do_feed_linenews' );
function do_feed_linenews() {
$feed_template = get_template_directory() . '/linenews.php';
load_template( $feed_template );
}


remove_filter('do_feed_rss2', 'do_feed_rss2', 10);
function custom_feed_rss2(){
$template_file = '/feed-rss2.php';
load_template(get_template_directory() . $template_file);
}
add_action('do_feed_rss2', 'custom_feed_rss2', 10);




/* ---------------------------------------------------- */


automatic_feed_links();

if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h2 class="widgettitle">',
    'after_title' => '</h2>',

  ));

add_theme_support( 'post-tshumbnails' );



/* ページ送りtitle
---------------------------------------------------- */

function theme_name_wp_title( $title ) {
global $page, $paged;
if ( ( $paged >= 2 || $page >= 2 ) ) {
$title .= "$sep" . sprintf( __( '(ページ%s)' ), max( $paged, $page ) );
}
return $title;
}
add_filter( 'wp_title', 'theme_name_wp_title');




/* エディター削除
---------------------------------------------------- */
function my_print_styles() {
  echo '<style type="text/css">
  #qt_content_strong,
  #qt_content_em,
  #qt_content_block,
  #qt_content_del,
  #qt_content_ins,
  #qt_content_ul,
  #qt_content_ol,
  #qt_content_li,
  #qt_content_code,
  #qt_content_more,
  #qt_content_close,
  #qt_content_fullscreen{display:none !important;}
  </style>';
}
add_action('admin_print_styles', 'my_print_styles', 21);

/* 管理アクセス禁止・adminbar 非表示
---------------------------------------------------- */
add_action( 'auth_redirect', 'subscriber_go_to_home' );
function subscriber_go_to_home( $user_id ) {
  $user = get_userdata( $user_id );
  if ( !$user->has_cap( 'read_private_pages' ) ) {
    wp_redirect( get_home_url() );
    exit();
  }
}

add_action( 'after_setup_theme', 'subscriber_hide_admin_bar' );
function subscriber_hide_admin_bar() {
  $user = wp_get_current_user();
  if ( isset( $user->data ) && !$user->has_cap( 'read_private_pages' ) ) {
    show_admin_bar( false );
  }
}

function custom_login() {
  $files = '<link rel="stylesheet" href="'.get_bloginfo('template_directory').'/login.css" />';
  echo $files;
}
add_action( 'login_enqueue_scripts', 'custom_login' );



/* 固定ページでMOREを使う
---------------------------------------------------- */
add_post_type_support( 'page', 'excerpt' ); 

/* head
---------------------------------------------------- */

remove_action('wp_head', 'feed_links_extra',3,0);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'rel_canonical');

/* 抜粋
---------------------------------------------------- */
function new_excerpt_more($more) {
     return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

/* アイキャッチ画像のURLを取得
---------------------------------------------------- */
function get_eyecatch_url($imgsize) {
  $thumbnail_id = get_post_thumbnail_id($post->ID); 
  $image = wp_get_attachment_image_src( $thumbnail_id, $imgsize);
  return $image[0];
}
function get_eyecatch_alt() {
  $thumbnail_id = get_post_thumbnail_id($post->ID); 
  $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
  return $alt;
}

/* アイキャッチ画像のサイズ削除
---------------------------------------------------- */
add_theme_support( 'post-thumbnails' );

add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );
function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    $html = preg_replace('/class=".*\w+"\s/', '', $html);
    return $html;
};



/* カスタムメニュー
---------------------------------------------------- */
register_nav_menus( array( 'main-nav' => __( 'Main Navigation' ), ) );
register_nav_menus( array( 'footer-nav' => __( 'Footer Navigation' ), ) );
register_nav_menus( array( 'side-nav' => __( 'Side Navigation' ), ) );


/* SEARCHFORM 出力
---------------------------------------------------- */
function my_search_form( $form ) {
    $form = '<form method="get" action="'.home_url( '/' ).'" >
    <input type="text" value="' . get_search_query() . '" name="s" placeholder="SEARCH KEYWORD">
    <input type="submit" id="searchsubmit" value="" >
    </form>';
    return $form;
}

add_filter( 'get_search_form', 'my_search_form' );



/* リンク
---------------------------------------------------- */
function bookmark_shortcode( $attr ) {
    $defaults = array(
        'orderby' => 'name',
        'order' => 'ASC',
        'limit' => -1,
        'category' => '',
        'category_name' => '',
        'hide_invisible' => 1,
        'show_updated' => 0,
        'show_description' => 1,
        'show_images' => 1,
        'show_rating' => 0,
        'hide_invisible' => 1,
        'categorize' => 1,
        'title_li' => '',
        'title_before' => '<h3>',
        'title_after' => '</h3>',
        'category_orderby' => 'name',
        'category_order' => 'ASC',
        'class' => 'linkcat',
        'category_before' => '',
        'category_after' => '',
        'show_private ' => 0,
        'include' => '',
        'exclude' => ''
    );
    $r = shortcode_atts( $defaults, $attr );
    $r['echo'] = false;
    $r['title_before'] = html_entity_decode( $r['title_before'] );
    $r['title_after'] = html_entity_decode( $r['title_after'] );
    return wp_list_bookmarks( $r );
}
add_shortcode('bookmarks', 'bookmark_shortcode');


function remove_hwstring_from_image_tag( $html, $id, $alt, $title, $align, $size ) {
    list( $img_src, $width, $height ) = image_downsize($id, $size);
    $hwstring = image_hwstring( $width, $height );
    $html = str_replace( $hwstring, '', $html );
    return $html;
}
add_filter( 'get_image_tag', 'remove_hwstring_from_image_tag', 10, 6 );  

/* 抜粋
---------------------------------------------------- */
function new_excerpt_mblength($length) { 
  if( is_home()) {
     return 200; 
    }
    else{
      return 200;
    }
}
add_filter('excerpt_mblength', 'new_excerpt_mblength');

/* query post
---------------------------------------------------- */
function customize_mainquery_home( $wp_query ) {
  if ( is_admin() || ! $wp_query->is_main_query() )
    return;
    
   if(is_tax('eventtype','eventinfo') || is_tax('genre')) {
    $currnet_date = date_i18n( 'y.m.d' );
    $wp_query->set('meta_query',
          array(
              array(
          'key' => 'DATE',
          'value' => $currnet_date,
          'compare' => '>=',
          'type' => 'DATE'
          )
        )
    );
    $wp_query->set( 'meta_key'   , 'DATE'      );
    $wp_query->set( 'orderby'    , 'meta_value' );
    $wp_query->set( 'order'      , 'ASC'       );
    $wp_query->set( 'posts_per_page', '15' );
    return;
    }

    elseif(is_post_type_archive( 'audition')) {
    $currnet_date = date_i18n( 'y.m.d' );
    $wp_query->set('meta_query',
          array(
              array(
          'key' => 'DATE',
          'value' => $currnet_date,
          'compare' => '>=',
          'type' => 'DATE'
          )
        )
    );
    $wp_query->set( 'meta_key'   , 'DATE'      );
    $wp_query->set( 'orderby'    , 'meta_value' );
    $wp_query->set( 'order'      , 'ASC'       );
    $wp_query->set( 'posts_per_page', '15' );
    return;
    }
    elseif(is_category() || is_tag()){
      $wp_query->set( 'post_type',array('post','movie','music','fashionsnap','event','column','audition','dancersgram') ); 
      $wp_query->set( 'posts_per_page', '15' );
      return;
    }
    elseif(is_search()){
      $wp_query->set( 'post_type',array('post','movie','music','fashionsnap','event','column','audition','dancersgram') ); 
      $wp_query->set( 'posts_per_page', '15' );
      return;
    }
    elseif(is_author()){
      $wp_query->set( 'post_type', array('post','movie','music','fashionsnap','event','column') ); 
      return;
    }
    else{
      return;
    }
    
  
}
add_action( 'pre_get_posts', 'customize_mainquery_home' );


/* facebook for wordpress ogp 制御
---------------------------------------------------- */
add_filter("fb_meta_tags", "not_facebook_meta");
function not_facebook_meta($meta){
    return '';
}

/* パーマリンク設定
---------------------------------------------------- */
add_action('init', 'myposttype_rewrite');
function myposttype_rewrite() {
    global $wp_rewrite;

    $queryarg = 'post_type=event&p=';
    $wp_rewrite->add_rewrite_tag('%event_id%', '([^/]+)',$queryarg);
    $wp_rewrite->add_permastruct('event', '/event/%event_id%.html', false);

    $queryarg = 'post_type=music&p=';
    $wp_rewrite->add_rewrite_tag('%music_id%', '([^/]+)',$queryarg);
    $wp_rewrite->add_permastruct('music', '/music/%music_id%.html', false);
    
    $queryarg = 'post_type=fashionsnap&p=';
    $wp_rewrite->add_rewrite_tag('%fashionsnap_id%', '([^/]+)',$queryarg);
    $wp_rewrite->add_permastruct('fashionsnap', '/fashionsnap/%fashionsnap_id%.html', false);
    
    $queryarg = 'post_type=studio&p=';
    $wp_rewrite->add_rewrite_tag('%studio_id%', '([^/]+)',$queryarg);
    $wp_rewrite->add_permastruct('studio', '/studio/%studio_id%.html', false);

    $queryarg = 'post_type=university&p=';
    $wp_rewrite->add_rewrite_tag('%university_id%', '([^/]+)',$queryarg);
    $wp_rewrite->add_permastruct('university', '/university/%university_id%.html', false);
    
    $queryarg = 'post_type=wiki&p=';
    $wp_rewrite->add_rewrite_tag('%wiki_id%', '([^/]+)',$queryarg);
    $wp_rewrite->add_permastruct('wiki', '/wiki/%wiki_id%.html', false);

    $queryarg = 'post_type=movie&p=';
    $wp_rewrite->add_rewrite_tag('%movie_id%', '([^/]+)',$queryarg);
    $wp_rewrite->add_permastruct('movie', '/movie/%movie_id%.html', false);
    
    $queryarg = 'post_type=column&p=';
    $wp_rewrite->add_rewrite_tag('%column_id%', '([^/]+)',$queryarg);
    $wp_rewrite->add_permastruct('column', '/column/%column_id%.html', false);
    
    $queryarg = 'post_type=audition&p=';
    $wp_rewrite->add_rewrite_tag('%audition_id%', '([^/]+)',$queryarg);
    $wp_rewrite->add_permastruct('audition', '/audition/%audition_id%.html', false);

    $queryarg = 'post_type=dancersgram&p=';
    $wp_rewrite->add_rewrite_tag('%dancersgram_id%', '([^/]+)',$queryarg);
    $wp_rewrite->add_permastruct('dancersgram', '/dancersgram/%dancersgram_id%.html', false);

    $queryarg = 'post_type=shop&p=';
    $wp_rewrite->add_rewrite_tag('%shop_id%', '([^/]+)',$queryarg);
    $wp_rewrite->add_permastruct('shop', '/shop/%shop_id%.html', false);

    // $queryarg = 'post_type=apporiginal&p=';
    // $wp_rewrite->add_rewrite_tag('%apporiginal_id%', '([^/]+)',$queryarg);
    // $wp_rewrite->add_permastruct('apporiginal', '/apporiginal/%apporiginal_id%.html', false);

}

add_filter('post_type_link', 'myposttype_permalink', 1, 3);

function myposttype_permalink($post_link, $id = 0, $leavename) {
    global $wp_rewrite;
    $post = &get_post($id);
    if ( is_wp_error( $post ) )
        return $post;
    $newlink = $wp_rewrite->get_extra_permastruct($post->post_type);
    $newlink = str_replace('%'.$post->post_type.'_id%', $post->ID, $newlink);
    $newlink = home_url(user_trailingslashit($newlink));
    return $newlink;
}

/* dewsインスタ get like ショートコード (20141027 by kono)
/* 引数 : 出力個数,
---------------------------------------------------- */
function insta_like_code($atts) {
  extract(shortcode_atts(array(
        'count' => 0,
    'mediaid' => null,
    ), $atts));
//$print = $count;

//リクエストURL
$request_url = "https://api.instagram.com/v1/users/self/media/liked";
 
//パラメータを配列形式で指定(その後、配列形式のパラメータを文字列に変換)add_websites_post_type
$params = array(
  'access_token' => "1278101127.1fb234f.9156115cb4124011b0745c7427b82161", //dewsアクセストークン
  'count' => $count,
  'max_like_id' => $mediaid,
);
$query = http_build_query($params);

//JSONデータを取得し、オブジェクト形式に変換
$obj = json_decode(@file_get_contents("{$request_url}?{$query}"));

//個々のメディア情報
foreach($obj->data as $item){

  //ID・リンク・投稿時間・コメント数・ライク数・フィルター・タグ・埋め込み用HTMLコード
  $id = $item->id;
  $link = $item->link;
  $created = date('Y/m/d H:i',$item->created_time);
  $comments = (isset($item->comments->count)) ? $item->comments->count : 0;
  $likes = (isset($item->likes->count)) ? $item->likes->count : 0;
  $like_username = $item->user->username; //str_xyon
  $like_icon =$item->user->profile_picture;
  $like_fullname = $item->user->full_name; // satoru mizuno
  $filter = $item->filter;
  $tags = (isset($item->tags) && $item->tags) ? implode('、',(array)$item->tags) : ' ';
  $embed = "&lt;iframe src=\"{$link}embed/\" width=\"612\" height=\"710\" frameborder=\"0\" scrolling=\"no\" allowtransparency=\"true\"&gt;&lt;/iframe&gt;";

  //メディアファイルのURL(画像・動画)
  $image_file = $item->images->standard_resolution->url;
  $image_width = 150;
  $image_height = 150;
  $movie_file = (isset($item->videos->standard_resolution->url)) ? "<br/>動画ファイルURL：".$item->videos->standard_resolution->url : '';

  $print .= "<div class=\"istaBox\"><div class=\"istaBox--hd\">";
  $print .= "<span class=\"pficon\"><img src=\"{$like_icon}\"></span>";
  $print .= "<span class=\"name\"><a href=\"http://instagram.com/{$like_username}\" target=\"_blank\" rel=\"nofollow\">{$like_username}</a></span>";
  $print .= "<span class=\"like\">{$likes}</span></div>";
  $print .= "<div class=\"istaBox--bd\"><a href=\"{$link}\" target=\"_blank\" rel=\"nofollow\"><img src=\"{$image_file}\" ></a></div>";
  $print .= "<div class=\"istaBox--ft\">{$tags}</div></div><span style=\"display:none\">{$id}</span>";    

}

//出力
return $print;

}
add_shortcode('insta_likes', 'insta_like_code');



/**
 * get_social_avatar
 * ソーシャルログインユーザー用アバター画像
 * @param string $img イメージタグ
 * @param string $id_or_email ユーザーIDもしくはEメールアドレス
 * @param numeric $size 画像サイズ
 * @param string $default デフォルト画像URL
 * @param string $atl alt
 * @return string イメージタグ
 */
function get_social_avatar( $img, $id_or_email, $size, $default, $alt ) {
    $_wpg_facebook_id = get_the_author_meta( '_wpg_facebook_id', $id_or_email );
    $_wpg_twitter_screen_name = get_the_author_meta( '_wpg_twitter_screen_name', $id_or_email );
    // Facebookのとき
    if ( $_wpg_facebook_id ) {
        $img = '<img src="https://graph.facebook.com/' . esc_attr( $_wpg_facebook_id ) . '/picture?width=320&height=320" alt="<?php echo esc_attr( $alt ); ?>" class="avatar photo" />';
    }
    // Twitterのとき
    elseif ( $_wpg_twitter_screen_name ) {
        if ( false === ( $profile_image_url = get_transient( 'twitter_avatar_' . $_wpg_twitter_screen_name ) ) ) {
            if ( class_exists( 'Twitter_Controller' ) ) {
                $wp_gianism_option = get_option( 'wp_gianism_option' );
                $Twitter_Controller = new Twitter_Controller( array(
                    "tw_screen_name" => $id_or_email,
                    "tw_consumer_key" => $wp_gianism_option['tw_consumer_key'],
                    "tw_consumer_secret" => $wp_gianism_option['tw_consumer_secret'],
                    "tw_access_token" => $wp_gianism_option['tw_access_token'],
                    "tw_access_token_secret" => $wp_gianism_option['tw_access_token_secret'],
                ) );
                $t = $Twitter_Controller->request( 'users/show', array(
                    'screen_name' => $_wpg_twitter_screen_name
                ) );
            } else {
                $twitter = \Gianism\Service\Twitter::get_instance();
                $t = $twitter->call_api( 'users/show', array(
                    'screen_name' => $_wpg_twitter_screen_name
                ) );
            }
            $profile_image_url = $t->profile_image_url;
            set_transient( 'twitter_avatar_' . $_wpg_twitter_screen_name, $profile_image_url, 320 * 320 * 1 );
        }
        if ( $profile_image_url ) {
            $img = '<img src="' . esc_url( $profile_image_url ) . '" alt="' . esc_attr( $alt ) . '" class="avatar photo" />';
        }
    }
    return $img;
}
add_filter( 'get_avatar', 'get_social_avatar', 10, 5 );


/**
 * 関連記事の作成
 * Author kouno 
 */

function get_reration_post( $atts ) {
  extract(shortcode_atts(array(
    'head3' => null,
    'reration_id' => 0,
  ), $atts , 'reration_post') );
  $reration_ary = explode(",", $reration_id);

  $disp_html = '<div class="entries reration_post">';
  if($head3){
    $disp_html .= '<div class="headline">'.$head3.'</div>';
  }  
  $disp_html .= '<ul class="entries--list">';
    foreach ($reration_ary as $value) {
      $post = get_post($value);
      $r_title = $post->post_title;
      $r_status = $post->post_status;
      $r_content = mb_substr(strip_tags($post->post_content),0,120).'...';
      $url = get_permalink( $value );
      $thumbnail_id = get_post_thumbnail_id($value);
      $eye_img = wp_get_attachment_image_src( $thumbnail_id , 'medium' );
      $posttime = get_the_time("Y.m.d",$post);

      if ($r_status == 'publish'){
        $disp_data = array($url,$eye_img[0],$r_title,$r_content,$posttime);
        $disp_html .= reration_displist($disp_data);
      }
    }
  $disp_html .= '</ul>';
  $disp_html .= '</div>';

  return $disp_html;

}
add_shortcode( 'reration_post', 'get_reration_post');

// 人物紹介

// function get_wiki_post( $atts ) {
//   extract(shortcode_atts(array(
//     'reration_id' => 0,
//   ), $atts , 'reration_post') );
//   $reration_ary = explode(",", $reration_id);

//   $disp_html = '<div class="author"><div class="author__info"><div class="author__img">';

//     foreach ($reration_ary as $value) {
//       $post = get_post($value);
//       $r_title = $post->post_title;
//       $r_status = $post->post_status;
//       $r_url = $post->guid;
//       $r_content = mb_substr(strip_tags($post->post_content),0,200).'...<a href="'.$r_url.'">(続きを読む)</a>';
//       $url = get_permalink( $value );
//       $thumbnail_id = get_post_thumbnail_id($value);
//       $eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
//       $ctm = get_post_meta($post->ID, 'twitter', true);

//       if ($r_status == 'publish'){
//         $disp_html .= '<img src="'.$eye_img[0].'"/></div>';    
//         $disp_html .= '<div class="author__profile"><a href="'.$r_url.'" title=""><span class="author__name">'.$r_title.'</a></div>';
//         $disp_html .= '<div class="author-sns__item author-sns__item--twitter"><a href="https://twitter.com/<'.post_custom('twitter');.'" title="twitter" taget="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></div>'; 
//         $disp_html .= '<div class="author__text">'.$r_content.'</div>';
//         $disp_html .= '</div></div></div>';   
//       }
//     }
//   $disp_html .= '</tr>';
//   $disp_html .= '</tbody>';
//   $disp_html .= '</table>';

//   return $disp_html;

// }
// add_shortcode( 'reration_wiki', 'get_wiki_post');





/**
 * 関連ダンサースタジオイベントサークルの作成
 * Author kouno 
 *
*/


function reration_displist($disp_data) {
  $url = $disp_data[0];
  $studio_eye_img = $disp_data[1];
  $title = $disp_data[2];
  $content = $disp_data[3];
  $time = $disp_data[4];
    $disp_html = '<li>';
    $disp_html .= '<a href="'.$url.'">';
    $disp_html .= '<div class="imageWrapper">';
    $disp_html .= '<div class="image">';
    $disp_html .= '<img src="'.$studio_eye_img.'" alt="'.$title.'" />';
    $disp_html .= '</div>';
    $disp_html .= '</div>';
    $disp_html .= '<div class="text">';
    $disp_html .= '<time>';
    $disp_html .= $time;
    $disp_html .= '</time>';
    $disp_html .= '<p>';
    $disp_html .= $title;
    $disp_html .= '</p>';
    $disp_html .= '</div>';
    $disp_html .= '</a>';
    $disp_html .= '</li>';

  return $disp_html;

}

function thispage_reration($r_data) {
  $disp_html = '<section class="entries profil">';

  if ($r_data["related_dancer"][0]){
    /*
    $disp_html .= '<div class="headline">ダンサープロフィール</div>';
    $disp_html .= '<ul class="entries--list reration_list">';
      foreach ($r_data["related_dancer"] as $dancer_id) {
        $post = get_post($circle_id);
        $title = 'WAPPER';
        $status = 'publish';
        $content = 'HOUSE OF NINJA / TOKYO FOOTWORKZ House Danceの聖地New Yorkにて、”The House DanceProject”の中心メンバーとして、5年間活動し、”The Carnival”や”SF Hiphop Dance Fest”など数々のShowに招待されるなど、他国でのワークショップやShowを含め、世界的に活動しているダンサーである。';
        $url = 'http://www.dancealive.tv/dictionary/'.$dancer_id;
        $img = 'http://dancealive.tv/wp/wp-content/themes/danceaweb2013/images/dictionary/'.$dancer_id.'/main.jpg';



        if ($status == 'publish'){
          $disp_data = array($url,$img,$title,$content);
          $disp_html .= reration_displist($disp_data);
        }
      }
    $disp_html .= '</ul>';
    */
  }

  if ($r_data["related_circle"][0]){
    $disp_html .= '<div class="headline">関連サークル</div>';
    $disp_html .= '<ul class="entries--list reration_list">';
      foreach ($r_data["related_circle"] as $circle_id) {
        $post = get_post($circle_id);
        $title = $post->post_title;
        $status = $post->post_status;
        $content = mb_substr(strip_tags($post->post_content),0,200).'...';
        $url = 'http://dews365.com/university/'.$circle_id.'.html';
        $thumbnail_id = get_post_thumbnail_id($circle_id);
        $eye_img = wp_get_attachment_image_src( $thumbnail_id , 'medium' );


        if ($status == 'publish'){
          $disp_data = array($url,$eye_img[0],$title,$content);
          $disp_html .= reration_displist($disp_data);
        }
      }
    $disp_html .= '</ul>';
  }

  if ($r_data["related_studio"][0]){
    $disp_html .= '<div class="headline">関連スタジオ</div>';
    $disp_html .= '<ul class="entries--list reration_list">';
      foreach ($r_data["related_studio"] as $studio_id) {
        $post = get_post($studio_id);
        $title = $post->post_title;
        $status = $post->post_status;
        $content = mb_substr(strip_tags($post->post_content),0,200).'...';
        $slug = $post->post_name;
        $url = 'http://dews365.com/studio/'.$studio_id.'.html';
        $home_url = esc_url( home_url( '/' ));
        $studio_eye_img = $home_url.'images/studio/'.$slug.'/main.jpg';


        if ($status == 'publish'){
          $disp_data = array($url,$studio_eye_img,$title,$content);
          $disp_html .= reration_displist($disp_data);
        }
      }
    $disp_html .= '</ul>';
  }


  if ($r_data["related_event"][0]){
    $disp_html .= '<div class="headline">関連イベント</div>';
    $disp_html .= '<ul class="entries--list reration_list">';
      foreach ($r_data["related_event"] as $event_id) {
        $post = get_post($event_id);
        $title = $post->post_title;
        $status = $post->post_status;
        $content = mb_substr(strip_tags($post->post_content),0,200).'...';
        $url = 'http://dews365.com/university/'.$event_id.'.html';
        $thumbnail_id = get_post_thumbnail_id($event_id);
        $eye_img = wp_get_attachment_image_src( $thumbnail_id , 'medium' );


        if ($status == 'publish'){
          $disp_data = array($url,$eye_img[0],$title,$content);
          $disp_html .= reration_displist($disp_data);
        }
      }
    $disp_html .= '</ul>';
  }

  $disp_html .= '</section>';

  return $disp_html;
}


/* カウントダウンバナーの設置 (ない場合はgoogleアドセンスが入る)
dance@webにもあるので変更の際には両方更新！！
add by kouno 20160115
---------------------------------------------------- */
function set_countdownBnr($argument) {
    $start_date=  date("Ymd",strtotime("20160125"));
    $start_10defore = date("Ymd",strtotime('-10 day' ,strtotime($start_date))); /*１０日前*/
    $end_date= date("Ymd",strtotime("20160209"));
    $url='http://www.dancealive.tv/2016japanfinal';
    $img_url= get_template_directory_uri().'/images/adv_banner/';

    $size=$width;
    date_default_timezone_set('Asia/Tokyo'); 
    $date = date("Ymd");

    if($argument && $start_10defore <= $date &&  $date<= $end_date){
        $text;

    /*自動更新バナー */
        
        if($date==$start_10defore):
            $text = '<p><a href="'.$url.'" rel="colleague" target="_blank"><img src="'.$img_url.'CD_top_10.jpg" alt="" style="'.$size.'" /></a></p>';
        elseif($date==date("Ymd",strtotime('-9 day' ,strtotime($start_date)))):
            $text = '<p><a href="'.$url.'" rel="colleague" target="_blank"><img src="'.$img_url.'CD_top_9.jpg" alt="" style="'.$size.'" /></a></p>';
        elseif($date==date("Ymd",strtotime('-8 day' ,strtotime($start_date)))):
            $text = '<p><a href="'.$url.'" rel="colleague" target="_blank"><img src="'.$img_url.'CD_top_8.jpg" alt="" style="'.$size.'" /></a></p>';
        elseif($date==date("Ymd",strtotime('-7 day' ,strtotime($start_date)))):
            $text = '<p><a href="'.$url.'" rel="colleague" target="_blank"><img src="'.$img_url.'CD_top_7.jpg" alt="" style="'.$size.'" /></a></p>';
        elseif($date==date("Ymd",strtotime('-6 day' ,strtotime($start_date)))):
            $text = '<p><a href="'.$url.'" rel="colleague" target="_blank"><img src="'.$img_url.'CD_top_6.jpg" alt="" style="'.$size.'" /></a></p>';
        elseif($date==date("Ymd",strtotime('-5 day' ,strtotime($start_date)))):
            $text = '<p><a href="'.$url.'" rel="colleague" target="_blank"><img src="'.$img_url.'CD_top_5.jpg"  alt="" style="'.$size.'" /></a></p>';
        elseif($date==date("Ymd",strtotime('-4 day' ,strtotime($start_date)))):
            $text = '<p><a href="'.$url.'" rel="colleague" target="_blank"><img src="'.$img_url.'CD_top_4.jpg"  alt="" style="'.$size.'" /></a></p>';
        elseif($date==date("Ymd",strtotime('-3 day' ,strtotime($start_date)))):
            $text = '<p><a href="'.$url.'" rel="colleague" target="_blank"><img src="'.$img_url.'CD_top_3.jpg" alt="" style="'.$size.'" /></a></p>';
        elseif($date==date("Ymd",strtotime('-2 day' ,strtotime($start_date)))):
            $text = '<p><a href="'.$url.'" rel="colleague" target="_blank"><img src="'.$img_url.'CD_top_2.jpg" alt="" style="'.$size.'" /></a></p>';
        elseif($date==date("Ymd",strtotime('-1 day' ,strtotime($start_date)))):
            $text = '<p><a href="'.$url.'" rel="colleague" target="_blank"><img src="'.$img_url.'CD_top_1.jpg" alt="" style="'.$size.'" /></a></p>';
        elseif($date==$start_date):
            $text = '<p><a href="'.$url.'" rel="colleague" target="_blank"><img src="'.$img_url.'CD_top_today.jpg" alt="" style="'.$size.'" /></a></p>';
        endif;
    

    }

    return($text);
}
add_action('disp_countdownBnr', 'set_countdownBnr' );

/*ランキングテスト [http://ateitexe.com/wordpress-archive-sort-popular/] */

function SortArchive( $query ) {
  if ( is_admin() || ! $query->is_main_query() )
    return;
 
  if ( $query->is_category() || $query->is_tag() ) {
    $sortset = (string)filter_input(INPUT_GET, 'sort');
    if ( $sortset !== 'older' && $sortset !== 'popular' && $sortset !== 'unpopular' ) {
      $query->set( 'orderby', 'date' );
    } elseif ( $sortset === 'older' ) {
      $query->set( 'orderby', 'date' );
      $query->set( 'order', 'ASC' );
    } elseif ( $sortset === 'popular' ) {
      $query->set( 'meta_key', 'views' );
      $query->set( 'orderby', 'meta_value_num' );
    } elseif ( $sortset === 'unpopular' ) {
      $query->set( 'meta_key', 'views' );
      $query->set( 'orderby', 'meta_value_num' );
      $query->set( 'order', 'ASC' );
    }
    return;
  }
}
add_action( 'pre_get_posts', 'SortArchive' );

// スマホ条件分岐
function is_mobile() {
  $useragents = array(
    'iPhone',          // iPhone
    'iPod',            // iPod touch
    'Android',         // 1.5+ Android
    'dream',           // Pre 1.5 Android
    'CUPCAKE',         // 1.5+ Android
    'blackberry9500',  // Storm
    'blackberry9530',  // Storm
    'blackberry9520',  // Storm v2
    'blackberry9550',  // Storm v2
    'blackberry9800',  // Torch
    'webOS',           // Palm Pre Experimental
    'incognito',       // Other iPhone browser
    'webmate'          // Other iPhone browser
  );
  $pattern = '/'.implode('|', $useragents).'/i';
  return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
}


// アプリ条件分岐   
// 


// function is_app() {
//   if (preg_match('/^DewsApp/', $_SERVER['HTTP_USER_AGENT'])){
//       return true;
//   } else {
//       return false;
//   }
// }





//管理画面での記事一覧カラムカスタマイズ
//WordpressPoplarPostのビューカウントを表示する

function add_posts_column_wpp_views($columns) {
    $columns['post_wpp_views_num'] = 'ビュー数';
    echo '<style type="text/css"> .fixed .column-post_wpp_views_num { width: 120px; } </style>';
    return $columns;
}
add_filter( 'manage_posts_columns', 'add_posts_column_wpp_views' );
add_filter( 'manage_edit-post_sortable_columns', 'add_posts_column_wpp_views' );

function add_each_posts_column_wpp_views($column_name, $post_id) {
    if( 'post_wpp_views_num' == $column_name ) {
        $post_wpp_views_num = wpp_get_views($post_id, 'all', true);
        echo $post_wpp_views_num;
        $post_wpp_views_num_old = get_post_meta($post_id,'ビュー数',true);
        update_post_meta($post_id,'ビュー数', $post_wpp_views_num, $post_wpp_views_num_old);
    }
}
add_action( 'manage_posts_custom_column', 'add_each_posts_column_wpp_views', 10, 2 );

function add_posts_column_orderby_wpp_views( $vars ) {
    if ( isset( $vars['orderby'] ) && 'ビュー数' == $vars['orderby'] ) {
        $vars = array_merge( $vars, array(
            'meta_key' => 'ビュー数', 
            'orderby' => 'meta_value_num',
        ));
    }
    return $vars;
}
add_filter( 'request', 'add_posts_column_orderby_wpp_views' );



//100×100pxのサムネイルを作成
add_image_size('thumb100', 100, 100, true);

//サイトドメインを取得
function get_this_site_domain(){
  //ドメイン情報を$results[1]に取得する
  preg_match( '/https?:\/\/(.+?)\//i', admin_url(), $results );
  return $results[1];
}

//本文抜粋を取得する関数（綺麗な抜粋文を作成するため）
//使用方法：http://nelog.jp/get_the_custom_excerpt
function get_the_custom_excerpt($content, $length) {
  $length = ($length ? $length : 70);//デフォルトの長さを指定する
  $content =  preg_replace('/<!--more-->.+/is',"",$content); //moreタグ以降削除
  $content =  strip_shortcodes($content);//ショートコード削除
  $content =  strip_tags($content);//タグの除去
  $content =  str_replace("&nbsp;","",$content);//特殊文字の削除（今回はスペースのみ）
  $content =  mb_substr($content,0,$length);//文字列を指定した長さで切り取る
  return $content;
}

//本文中のURLをブログカードタグに変更する
function url_to_blog_card($the_content) {
  if ( is_singular() ) {//投稿ページもしくは固定ページのとき
    //1行にURLのみが期待されている行（URL）を全て$mに取得
    $res = preg_match_all('/^(<p>)?(<a.+?>)?https?:\/\/'.preg_quote(get_this_site_domain()).'\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+(<\/a>)?(<\/p>)?(<br ? \/>)?$/im', $the_content,$m);
    //マッチしたURL一つ一つをループしてカードを作成
    foreach ($m[0] as $match) {
      $url = strip_tags($match);//URL
      $id = url_to_postid( $url );//IDを取得（URLから投稿ID変換）
      if ( !$id ) continue;//IDを取得できない場合はループを飛ばす
      $post = get_post($id);//IDから投稿情報の取得
      $title = $post->post_title;//タイトルの取得
      $date = mysql2date('Y-m-d H:i', $post->post_date);//投稿日の取得
      $excerpt = get_the_custom_excerpt($post->post_content, 90);//抜粋の取得
      $thumbnail = get_the_post_thumbnail($id, 'thumb100', array('style' => 'width:100px;height:100px;', 'class' => 'blog-card-thumb-image'));//サムネイルの取得（要100×100のサムネイル設定）
      if ( !$thumbnail ) {//サムネイルが存在しない場合
        $thumbnail = '<img src="'.get_template_directory_uri().'/images/no-image.png" style="width:100px;height:100px;" />';
      }
      //取得した情報からブログカードのHTMLタグを作成
      $tag = '<div class="blog-card"><div class="blog-card-thumbnail"><a href="'.$url.'" class="blog-card-thumbnail-link">'.$thumbnail.'</a></div><div class="blog-card-content"><div class="blog-card-title"><a href="'.$url.'" class="blog-card-title-link">'.$title.'</a></div><div class="blog-card-excerpt">'.$excerpt.'</div></div><div class="blog-card-footer clear"><span class="blog-card-date">'.$date.'</span></div></div>';
      //本文中のURLをブログカードタグで置換
      $the_content = preg_replace('{'.preg_quote($match).'}', $tag , $the_content, 1);
    }
  }
  return $the_content;//置換後のコンテンツを返す
}
add_filter('the_content','url_to_blog_card');//本文表示をフック

//-------------------Wordpress投稿画面のタイトル文字数をカウントする
function count_title_characters() {?>
<script type="text/javascript">
jQuery(document).ready(function($){
  //in_selの文字数をカウントしてout_selに出力する
  function count_characters(in_sel, out_sel) {
    $(out_sel).html( $(in_sel).val().length );
  }

  //ページ表示に表示エリアを出力
  $('#titlewrap').after('<div style="position:absolute;top:-24px;right:0;color:#666;background-color:#f7f7f7;padding:1px 2px;border-radius:5px;border:1px solid #ccc;">文字数<span class="wp-title-count" style="margin-left:5px;">0</span></div>');

  //ページ表示時に数える
  count_characters('#title', '.wp-title-count');

  //入力フォーム変更時に数える
  $('#title').bind("keydown keyup keypress change",function(){
    count_characters('#title', '.wp-title-count');
  });

});
</script><?php
}
add_action( 'admin_head-post-new.php', 'count_title_characters' );
add_action( 'admin_head-post.php', 'count_title_characters' );
