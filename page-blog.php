<?php /* Template Name: BLOG */ ?>

<?php get_header(); ?>

<div id="maincol">
<div class="content-inner">
	<section class="entries">
		<h1 class="section--title <?php if(is_category('dewmo')) echo 'dewmo' ;?>"><span class="icn icn-<?php echo esc_attr($slug);?>"><?php if(is_author()): echo 'コラム'; else: wp_title(''); endif;?></span></h1>

　　　　　<?php $posts = get_posts('numberposts=30&category=1908'); global $post; ?>
　　　　　<?php if($posts): foreach($posts as $post): setup_postdata($post); ?>
    <div class="main-post entries">
        <div class="imageWrapper">
            <a href="<?php the_permalink() ?>">
                <div class="entries--title-wrapper">
                <time datetime="<?php the_time("c"); ?>"><?php the_time("Y.m.d"); ?></time>
                    <h3 class="entries--title"><?php the_title(); ?></h3>
                </div>
                <div class="image">
                    <?php the_post_thumbnail("full"); ?>
                </div>
            </a>
        </div>
    </div>

    <div id="wpnav">
		<?php if(!(is_home()) && (function_exists('wp_pagenavi'))) { wp_pagenavi(); } ?>
	</div>

　　　　　<?php endforeach;
if(function_exists('wp_pagenavi')) { wp_pagenavi(); } 
endif; ?>


	</section>

	<?php if(is_author()): ?>
	<div id="authorbx" class="bx">
		<div class="authorname"><?php echo get_avatar( get_the_author_id(), $size = '40'); ?><div><span>著者 : </span><?php the_author_meta( dname, $author ); ?><br><span><a href="<?php the_author_meta( user_url, $author ); ?>" target="_blank" rel="nofollow"><?php the_author_meta( user_url, $author ); ?></a></span></div></div>
		<p><?php the_author_meta( description, $author ); ?></p>
	</div>
	<?php endif; ?>

</div><!--end content-inner -->
</div><!--end maincol -->

<?php get_footer(); ?>