<?php if((is_home() || is_archive() || is_single() || is_search() || is_page('music')) && !is_author()): ?>

<aside id="sub2">
	<div class="grid-sp">
	<div class="sub--bx">
	<?php
		 $bm = get_bookmarks( array(
		   'orderby'=> 'rand', 
		   'limit'  => 1, 
		   'category_name'  => 'sub1'
		 ));

		 foreach ($bm as $bookmark){ 
		   $click="'send','event','sub1','click','".$bookmark->link_name."'";
		   echo '<span style="display:inline-block;"><a href="'.$bookmark->link_url.'" onclick="ga('.$click.');"  target="_blank">';
		   echo '<img src="'.$bookmark->link_image.'" alt="'.$bookmark->link_name.'">';
		   echo '</a></span>';
		 }
	?>

	<?php 
	
		wp_list_bookmarks(array(
			'categorize' => 0,
			'title_li' => 0,
			'category_name' => 'sub2',
			'limit' => 1,
			'before' => '<span style="display:inline-block;">',
			'after' => '</span>',
			'show_images' => true,
			'show_description' => 0,
			'orderby' => 'rand'
		)); 
		
	?>

	<?php 
	
		wp_list_bookmarks(array(
			'categorize' => 0,
			'title_li' => 0,
			'category_name' => 'sub3',
			'limit' => 1,
			'before' => '<span style="display:inline-block;">',
			'after' => '</span>',
			'show_images' => true,
			'show_description' => 0,
			'orderby' => 'rand'
		)); 
		
	?>
	</div>


<!-- 		<div class="sub--bx entries">
		<a href="https://dews365.com/adinfo" title="広告掲載について"><img src="<?php echo get_template_directory_uri(); ?>/images/bn_ad.jpg" alt="広告掲載について"></a>
		</div> -->
		<div class="sub--bx entries ads">
			<?php if( wp_is_mobile()) : ?>
				<div class="ads__item">
					<!--ここにスマホ・タブレットのときのHTMLコードを記述-->
					<script type="text/javascript" language="JavaScript">
						yads_ad_ds = '106665_231261';
					</script>
					<script type="text/javascript" language="JavaScript" src="//yads.c.yimg.jp/js/yads.js"></script>
				</div>
			<?php else : ?>
				<div class="ads__item">
					<!--ここにpcのときのHTMLコードを記述-->
					<script type="text/javascript" language="JavaScript">
						yads_ad_ds = '106665_231225';
					</script>
					<script type="text/javascript" language="JavaScript" src="//yads.c.yimg.jp/js/yads.js"></script>
				</div>
				<?php if( ( is_home() || is_front_page() ) && !is_paged() ) : ?>
				<!--  a(300×600)DEWS_PC右サイドバー	-->
				<script type="text/javascript" src="http://js.sprout-ad.com/t/293/574/a1293574.js"></script>
				<?php endif; ?>
			<?php endif; ?>
			
	
		</div>
	</div>
</aside>

<?php endif; ?>

<?php if(!is_page( '125885' )): ?>
</div><!--end content-inner -->
</div><!--end maincol -->
</main><!--end mainContent -->
	<aside id="sub1">
		<div id="sub-navigation">
			<div class="nav-content">
				<?php wp_nav_menu( array('$container' => '', 'fallback_cb' => '', 'theme_location' => 'side-nav', 'depth' => '2') ); ?>
			</div>
		</div>
		<div id="sub-adarea">
			<?php
				$bm = get_bookmarks( array(
					'orderby'=> 'rand', 
					'limit'  => 2, 
					'category_name'  => 'left'
				));
	
				foreach ($bm as $bookmark){ 
					$click="'send','event','left','click','".$bookmark->link_name."'";
		     		$imp = "'send','event','left','imp','".$bookmark->link_name."'";
		     		echo '<div class="sub--bx sub--bx1"><a href="'.$bookmark->link_url.'" onclick="ga('.$click.');"  target="_blank">';
		     		echo '<img src="'.$bookmark->link_image.'" onload="ga('.$imp.');" alt="'.$bookmark->link_name.'">';
		     		echo '</a></div>';
				}
			?>
		</div>
	</aside><!--end aside1-->
</div><!--end content -->
<?php endif; ?>


<?php if( !is_page('96412')) : ?>
<div class="ranking">
	<section class="inner entries">
		<h2 class="section--title">WEEKLY RANKING<br><span>週間ランキング</span></h2>
		<ul class="grid grid-fill cf">
		<?php 
		$posts = wmp_get_popular( array( 'limit' => 6, 'post_type' => array('post','movie','event','fashionsnap','music','column'), 'range' => 'weekly' ) );
		global $post; if ( count( $posts ) > 0 ): foreach ( $posts as $post ): setup_postdata( $post ); 
		?>
			<li class="grid__item--2 has-gutter">
				<a href="<?php the_permalink(); ?>">
					<div class="imageWrapper"><div class="image"><?php if(has_post_thumbnail()): the_post_thumbnail("medium"); else: echo '<img src="' . get_template_directory_uri().'/images/noimg.jpg" alt="no image">'; endif; ?></div></div>
					<div class="text"><p><?php the_title(); ?></p></div>
				</a>
			</li>
		<?php endforeach; endif; wp_reset_postdata();  ?>
		</ul>
	</section>
</div> 
<?php endif; ?>

</div><!--end main -->


<footer id="footer">
    <div id="fttop" style="background-color: #000;">
	    <div class="footer__inner">
	    	<div class="inner cf">
	    		<h4><img src="<?php echo get_template_directory_uri(); ?>/images/logo-ft.png" alt="Dews ロゴ"></h4>
	    		<p>ダンスの情報ならダンスニュースメディア Dews（デュース）。ダンサー情報やダンス動画、ダンスミュージック、ダンスイベントバトル、コンテスト、オーディション情報をいち早く配信します。また、Dewsでは広告出稿を募集しております。広告出稿以外（タイアップ企画、WEBプロデュース等々）も随時受け付けております。お気軽にお問い合わせください。<br>詳しくはこちら</p>
	    		<a href="<?php echo esc_url( home_url( '/' ) ); ?>about"><?php echo esc_url( home_url( '/' ) ); ?>about</a>
	    	</div>
	    </div>
    </div>

<div id="ftmiddle">
	<div class="inner cf">
		<div class="ftnav">
		<h5 class="section--title"><span>Footer Navigation</span></h5>
			<ul>
				<li><a href="<?php echo esc_url( home_url() ); ?>">トップページ</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>about">Dewsとは？</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>adinfo">広告掲載について</a></li>

			</ul>
			<ul>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>member_regist">新規会員登録</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>mypage">ログイン</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>eventform">イベント掲載フォーム</a></li>		
			</ul>
		</div>

		<h5 class="section--title"><span>Follow us</span></h5>
        <ul class="socialLink flex flex--center">
        	<li><a href="http://dews365.com/feed" rel="nofollow" class="fb"><i class="fas fa-rss"></i></a></li>
        	<li><a href="https://www.facebook.com/dews365" target="_blank" rel="nofollow" class="fb"><i class="fab fa-facebook-f"></i></a></li>
        	<li><a href="https://twitter.com/Dews365" target="_blank" rel="nofollow"><i class="fab fa-twitter"></i></a></li>
        	<li><a href="http://instagram.com/dews365" target="_blank" rel="nofollow"><i class="fab fa-instagram"></i></a></li>
        	<li><a href="https://line.me/R/ti/p/%40ryh9583h?utm_campaign=single_bnr_lineat&utm_source=dews&utm_medium=dews" class="line"><i class="fab fa-line"></i></a></li>
        	<li><a href="https://www.youtube.com/user/DANCEHEROjp" target="_blank"><i class="fab fa-youtube"></i></a></li>
        </ul> 
 		

		<!-- <h5 class="section--title"><span>Share</span></h5>
		<div class="socialBtn"> 
			<ul>
			<li><a href="https://twitter.com/share" class="twitter-share-button" data-lang="en" data-count="vertical" data-text="ダンスの情報ならダンスニュースメディア Dews（デュース）" data-url="http://dews365.com" rel="nofollow">ツイート</a></li>
			<li><div class="fb-like" data-href="http://dews365.com" data-send="false" data-layout="box_count" data-show-faces="false" data-colorscheme="light"></div></li>
			<li><div id="gplusone" class="g-plusone" data-size="tall" data-href="http://dews365.com"></div></li>
			<li><a href="http://b.hatena.ne.jp/entry/http://dews365.com" class="hatena-bookmark-button" data-hatena-bookmark-title="ダンスの情報ならダンスニュースメディア Dews（デュース）" data-hatena-bookmark-layout="vertical-balloon" data-hatena-bookmark-lang="ja" title="このエントリーをはてなブックマークに追加" rel="nofollow">はてブする</a></li>
			</ul>
		</div> -->
	</div>
</div>

<div id="ftbottom">
	<div class="inner cf">
		<?php wp_nav_menu( array('$container' => '', 'fallback_cb' => '', 'theme_location' => 'footer-nav', 'depth' => '1') ); ?>
		<address>Copyright ©Dews All Rights Reserved.</address>
	</div>
</div>

</footer>

</div><!--wrapper-->

<!-- TOPスライド -->
<script src="<?php echo get_template_directory_uri(); ?>/js/swiper.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/vertical-timeline.js"></script>
<!-- ボックステキスト揃え (satoru)-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/autoheight.js"></script>
<!-- app popup-->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cookie.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.layerBoard.js"></script>
<!-- タブ-->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.hashchange.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.easytabs.min.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>


<!-- Twitter widgets Code -->
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '234562050228768');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=613251155538367&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.9&appId=613251155538367";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script> 
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-50144969-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
</script>

<!-- Google Tag Manager -->
<!-- <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PPTXGQ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PPTXGQ');</script> -->
<!-- End Google Tag Manager -->


<script type="text/javascript">
  var _fout_queue = _fout_queue || {}; if (_fout_queue.segment === void 0) _fout_queue.segment = {};
  if (_fout_queue.segment.queue === void 0) _fout_queue.segment.queue = [];

  _fout_queue.segment.queue.push({
    'user_id': 6145
  });

  (function() {
    var el = document.createElement('script'); el.type = 'text/javascript'; el.async = true;
    el.src = (('https:' == document.location.protocol) ? 'https://' : 'http://') + 'js.fout.jp/segmentation.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(el, s);
  })();
</script>

<!-- gunosy ad タグ -->
    <script src="//assets.gunosy.com/adnet/GunosyAdsSDK.async.js"></script>
    <script>
        GunosyAdsSDK.bid({
            mediaId: 11333,
            frameId: 29335,
        });
    </script>

<!-- fontawesome -->
<script>
    window.FontAwesomeConfig = {
        searchPseudoElements: true
    }
</script>

<!-- インスタ埋め込み用-->
<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>

<?php wp_footer(); ?>
</body>
</html>