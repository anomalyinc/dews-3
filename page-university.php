<?php /* Template Name: university */ ?>
<?php 
get_header(); 
the_post(); 
$page = get_page(get_the_ID());
$pageslug = $page->post_name;

$GENRE = $_GET['GENRE'];
$REGION = $_GET['REGION'];
$AREA = $_GET['AREA'];
$OPTION = $_GET['OPTION'];


if(!$GENRE && !$REGION && !$AREA && !$OPTION ){
query_posts( array(
	'post_type' => 'university', 
	'posts_per_page' => 22 , 
	'order' => ASC, 
	'orderby' => name , 
	'paged' => get_query_var( 'paged' )
)); 

$h2 = 'ストリートダンスサークル検索';
$GENRE = array();
$REGION = array();
$AREA = array();
$OPTION = array();

}else{

$h2 = 'ストリートダンスサークル 検索結果';

$arg = array(
'post_type' => 'university' ,
'posts_per_page' => 22 ,
'order' => ASC,
'orderby' => name ,
'paged' => get_query_var( 'paged' )
);

if($GENRE){
$arggenre = array('tax_query' => array( array(
	'key' => 'genre',
	'value' => $GENRE,
	'compare'=>'IN'
	)));
$arg = array_merge_recursive($arg,$arggenre );
}else{
	$GENRE = array();
};

if($REGION){
$argregion = array('meta_query' => array(
	array(
	'key' => 'REGION',
	'value' => $REGION,
	'compare'=>'IN'
	)
	)
	);
$arg = array_merge_recursive($arg,$argregion );
}else{
	$REGION = array();
};

if($AREA){
$argarea = array('meta_query' => array(
	array(
	'key' => 'AREA',
	'value' => $AREA,
	'compare'=>'IN'
	)
	)
	);
$arg = array_merge_recursive($arg,$argarea );
}else{
	$AREA = array();
};

if($OPTION){
$argoption = array('meta_query' => array(
	array(
	'key' => 'OPTION',
	'value' => $OPTION,
	'compare'=>'IN'
	)
	)
	);
$arg = array_merge_recursive($arg,$argoption );
}else{
	$OPTION = array();
};

query_posts( $arg );
	
}
?>

<section>
	<h1 class="section--title"><span class="icn icn-studio"><?php echo esc_html($h2); ?></span></h1>

	<form role="search" method="get" id="search-bar" class="cf"  action="<?php echo home_url(); ?>/university/">
		<div id="searchMap" class="formContent">
			<h3>地域</h3>
			<div class="formContent--wd grid cf">

				<div id="kanto" class="grid__item--3 has-gutter">
					<span><input type="checkbox" name="REGION[]" value="関東" class="region"  <?php if (in_array("関東", $REGION)) { echo 'checked="checked"'; } ?> /><label >関東</label></span>
					<div class="tokyo">
						<span><input type="checkbox" name="AREA[]" value="東京都" class="region" <?php if (in_array("東京", $AREA)) { echo 'checked="checked"'; } ?>/><label >東京都</label></span>
						<span><input type="checkbox" name="AREA[]" value="渋谷" /><label >渋谷</label></span>
						<span><input type="checkbox" name="AREA[]" value="新宿" /><label >新宿</label></span>
						<span><input type="checkbox" name="AREA[]" value="銀座・新橋" /><label >銀座・新橋</label></span>
						<span><input type="checkbox" name="AREA[]" value="池袋" /><label >池袋</label></span>
						<span><input type="checkbox" name="AREA[]" value="青山・六本木" /><label >青山・六本木</label></span>
						<span><input type="checkbox" name="AREA[]" value="目黒・恵比寿" /><label >目黒・恵比寿</label></span>
						<span><input type="checkbox" name="AREA[]" value="東京その他" /><label >その他</label></span>
						<span><input type="checkbox" name="AREA[]" value="23区外" /><label >23区外</label></span>
					</div>
					<div id="kanagawa">
						<span><input type="checkbox" name="AREA[]" value="神奈川県"  class="region" <?php if (in_array("神奈川", $AREA)) { echo 'checked="checked"'; } ?>/><label>神奈川県</label></span><br>
						<span><input type="checkbox" name="AREA[]" value="横浜・川崎" /><label>横浜・川崎</label></span>
						<span><input type="checkbox" name="AREA[]" value="神奈川その他" /><label>その他</label></span>
					</div>
					<span><input type="checkbox" name="AREA[]" value="茨城県" /><label>茨城県</label></span>
					<span><input type="checkbox" name="AREA[]" value="栃木県" /><label>栃木県</label></span>
					<span><input type="checkbox" name="AREA[]" value="群馬県" /><label>群馬県</label></span>
					<span><input type="checkbox" name="AREA[]" value="埼玉県" /><label>埼玉県</label></span>
					<span><input type="checkbox" name="AREA[]" value="千葉県" /><label>千葉県</label></span>
				</div>

				<div class="grid__item--3 has-gutter">
					<div id="hokkaido">
						<span><input type="checkbox" name="AREA[]" value="北海道" class="region" <?php if (in_array("北海道", $AREA)) { echo 'checked="checked"'; } ?> /><label>北海道</label></span><br>
						<span><input type="checkbox" name="AREA[]" value="札幌" /><label>札幌</label></span>
						<span><input type="checkbox" name="AREA[]" value="北海道その他" /><label>その他</label></span>
					</div>
					
					<div id="tohoku">
						<span><input type="checkbox" name="REGION[]" value="東北" class="region" <?php if (in_array("東北", $REGION)) { echo 'checked="checked"'; } ?>/><label>東北</label></span><br>
						<span><input type="checkbox" name="AREA[]" value="青森県" /><label>青森県</label></span>
						<span><input type="checkbox" name="AREA[]" value="秋田県" /><label>秋田県</label></span>
						<span><input type="checkbox" name="AREA[]" value="岩手県" /><label>岩手県</label></span>
						<span><input type="checkbox" name="AREA[]" value="山形県" /><label>山形県</label></span>
						<span><input type="checkbox" name="AREA[]" value="宮城県" /><label>宮城県</label></span>
						<span><input type="checkbox" name="AREA[]" value="福島県" /><label>福島県</label></span>
					</div>
					
					<div id="koushinetsu" >
						<span><input type="checkbox" name="REGION[]" value="甲信越" class="region" <?php if (in_array("甲信越", $REGION)) { echo 'checked="checked"'; } ?>/><label>甲信越</label></span><br>
						<span><input type="checkbox" name="AREA[]" value="山梨県" /><label>山梨県</label></span>
						<span><input type="checkbox" name="AREA[]" value="富山県" /><label>富山県</label></span>
						<span><input type="checkbox" name="AREA[]" value="長野県" /><label>長野県</label></span>
						<span><input type="checkbox" name="AREA[]" value="石川県" /><label>石川県</label></span>
						<span><input type="checkbox" name="AREA[]" value="新潟県" /><label>新潟県</label></span>
						<span><input type="checkbox" name="AREA[]" value="福井県" /><label>福井県</label></span>
					</div>
				</div>
				
				<div class="grid__item--3 has-gutter">
					<div id="tokai" >
						<span><input type="checkbox" name="REGION[]" value="東海" class="region" <?php if (in_array("東海", $REGION)) { echo 'checked="checked"'; } ?>/><label>東海</label></span><br>
						<span><input type="checkbox" name="AREA[]" value="岐阜県" /><label>岐阜県</label></span>
						<span><input type="checkbox" name="AREA[]" value="静岡県" /><label>静岡県</label></span>
						<span><input type="checkbox" name="AREA[]" value="三重県" /><label>三重県</label></span>
						<span><input type="checkbox" name="AREA[]" value="愛知県" /><label>愛知県</label></span>
					</div>
					
					<div id="kinki" >
						<span><input type="checkbox" name="REGION[]" value="近畿" class="region" <?php if (in_array("近畿", $REGION)) { echo 'checked="checked"'; } ?>/><label>近畿</label></span><br>
						<span><input type="checkbox" name="AREA[]" value="滋賀県" /><label>滋賀県</label></span>
						<span><input type="checkbox" name="AREA[]" value="京都府" /><label>京都府</label></span>
						<span><input type="checkbox" name="AREA[]" value="兵庫県" /><label>兵庫県</label></span>
						<span><input type="checkbox" name="AREA[]" value="奈良県" /><label>奈良県</label></span>
						<span><input type="checkbox" name="AREA[]" value="和歌山県" /><label>和歌山県</label></span>
						<span><input type="checkbox" name="AREA[]" value="大阪府" /><label>大阪府</label></span>
					</div>
					
					<div id="chugoku" >
						<span><input type="checkbox" name="REGION[]" value="中国" class="region" <?php if (in_array("中国", $REGION)) { echo 'checked="checked"'; } ?>/><label>中国</label></span><br>
						<span><input type="checkbox" name="AREA[]" value="鳥取県" /><label>鳥取県</label></span>
						<span><input type="checkbox" name="AREA[]" value="岡山県" /><label>岡山県</label></span>
						<span><input type="checkbox" name="AREA[]" value="島根県" /><label>島根県</label></span>
						<span><input type="checkbox" name="AREA[]" value="山口県" /><label>山口県</label></span>
						<span><input type="checkbox" name="AREA[]" value="広島県" /><label>広島県</label></span>
					</div>
				</div>

				<div class="grid__item--3 has-gutter">
					<div id="shikoku" >
						<span><input type="checkbox" name="REGION[]" value="四国" class="region" <?php if (in_array("四国", $REGION)) { echo 'checked="checked"'; } ?>/><label>四国</label></span><br>
						<span><input type="checkbox" name="AREA[]" value="香川県" /><label>香川県</label></span>
						<span><input type="checkbox" name="AREA[]" value="愛媛県" /><label>愛媛県</label></span>
						<span><input type="checkbox" name="AREA[]" value="徳島県" /><label>徳島県</label></span>
						<span><input type="checkbox" name="AREA[]" value="高知県" /><label>高知県</label></span>
					</div>
					
					<div id="kyushu " >
						<span><input type="checkbox" name="REGION[]" value="九州"  class="region" <?php if (in_array("福岡", $REGION)) { echo 'checked="checked"'; } ?>/><label>九州</label></span><br>
						<span><input type="checkbox" name="AREA[]" value="福岡県" /><label>福岡県</label></span>
						<span><input type="checkbox" name="AREA[]" value="熊本県" /><label>熊本県</label></span>
						<span><input type="checkbox" name="AREA[]" value="佐賀県" /><label>佐賀県</label></span>
						<span><input type="checkbox" name="AREA[]" value="大分県" /><label>大分県</label></span>
						<span><input type="checkbox" name="AREA[]" value="長崎県" /><label>長崎県</label></span>
						<span><input type="checkbox" name="AREA[]" value="鹿児島県" /><label>鹿児島県</label></span>
						<span><input type="checkbox" name="AREA[]" value="宮崎県" /><label>宮崎県</label></span>
					</div>
					
					<div id="okinawa" >
					<span><input type="checkbox" name="AREA[]" value="沖縄県" class="region" <?php if (in_array("沖縄県", $AREA)) { echo 'checked="checked"'; } ?>/><label>沖縄県</label></span>
					</div>
				</div>
			
			</div><!-- end map-content -->

		</div>
		
		<div id="searchStyle" class="formContent">
			<h3>ジャンル</h3>
			<div class="formContent--wd">
				<span><input type="checkbox" name="GENRE[]" value="HIPHOP" <?php if (in_array("HIPHOP", $GENRE)) { echo 'checked="checked"'; } ?> ><label>HIPHOP</label></span>
				<span><input type="checkbox" name="GENRE[]" value="HOUSE" <?php if (in_array("HOUSE", $GENRE)) { echo 'checked="checked"'; } ?> ><label>HOUSE</label></span>
				<span><input type="checkbox" name="GENRE[]" value="LOCK" <?php if (in_array("LOCK", $GENRE)) { echo 'checked="checked"'; } ?> ><label>LOCK</label></span>
				<span><input type="checkbox" name="GENRE[]" value="BREAKIN" <?php if (in_array("BREAKIN", $GENRE)) { echo 'checked="checked"'; } ?> ><label>BREAKIN</label></span>
				<span><input type="checkbox" name="GENRE[]" value="POP" <?php if (in_array("POP", $GENRE)) { echo 'checked="checked"'; } ?> ><label>POP</label></span>
				<span><input type="checkbox" name="GENRE[]" value="ANIMATION" <?php if (in_array("ANIMATION", $GENRE)) { echo 'checked="checked"'; } ?> ><label>ANIMATION</label></span>
				<span><input type="checkbox" name="GENRE[]" value="BOOGALOO" <?php if (in_array("BOOGALOO", $GENRE)) { echo 'checked="checked"'; } ?> ><label>BOOGALOO</label></span>
				<span><input type="checkbox" name="GENRE[]" value="KRUMP" <?php if (in_array("KRUMP", $GENRE)) { echo 'checked="checked"'; } ?> ><label>KRUMP</label></span>
				<span><input type="checkbox" name="GENRE[]" value="JAZZ" <?php if (in_array("JAZZ", $GENRE)) { echo 'checked="checked"'; } ?> ><label>JAZZ</label></span>
				<span><input type="checkbox" name="GENRE[]" value="PUNKING" <?php if (in_array("PUNKING", $GENRE)) { echo 'checked="checked"'; } ?> ><label>PUNKING</label></span>
				<span><input type="checkbox" name="GENRE[]" value="WAACK" <?php if (in_array("WAACK", $GENRE)) { echo 'checked="checked"'; } ?> ><label>WAACK</label></span>
				<span><input type="checkbox" name="GENRE[]" value="BE-BOP" <?php if (in_array("BE-BOP", $GENRE)) { echo 'checked="checked"'; } ?> ><label>BE-BOP</label></span>
				<span><input type="checkbox" name="GENRE[]" value="VOUGUE" <?php if (in_array("VOUGUE", $GENRE)) { echo 'checked="checked"'; } ?> ><label>VOUGUE</label></span>
				<span><input type="checkbox" name="GENRE[]" value="TAP" <?php if (in_array("TAP", $GENRE)) { echo 'checked="checked"'; } ?> ><label>TAP</label></span>
				<span><input type="checkbox" name="GENRE[]" value="SOUL" <?php if (in_array("SOUL", $GENRE)) { echo 'checked="checked"'; } ?> ><label>SOUL</label></span>
				<span><input type="checkbox" name="GENRE[]" value="FREESTYLE" <?php if (in_array("FREESTYLE", $GENRE)) { echo 'checked="checked"'; } ?> ><label>FREESTYLE</label></span>
				<span><input type="checkbox" name="GENRE[]" value="LATIN" <?php if (in_array("LATIN", $GENRE)) { echo 'checked="checked"'; } ?> ><label>LATIN</label></span>
				<span><input type="checkbox" name="GENRE[]" value="YOGA" <?php if (in_array("YOGA", $GENRE)) { echo 'checked="checked"'; } ?> ><label>YOGA</label></span>
				<span><input type="checkbox" name="GENRE[]" value="SALSA" <?php if (in_array("SALSA", $GENRE)) { echo 'checked="checked"'; } ?> ><label>SALSA</label></span>
				<span><input type="checkbox" name="GENRE[]" value="HULA" <?php if (in_array("HULA", $GENRE)) { echo 'checked="checked"'; } ?> ><label>HULA</label></span>
				<span><input type="checkbox" name="GENRE[]" value="REGGAE" <?php if (in_array("REGGAE", $GENRE)) { echo 'checked="checked"'; } ?> ><label>REGGAE</label></span>
				<span><input type="checkbox" name="GENRE[]" value="BALLET" <?php if (in_array("BALLET", $GENRE)) { echo 'checked="checked"'; } ?> ><label>BALLET</label></span>
				<span><input type="checkbox" name="GENRE[]" value="CAPOEIRA" <?php if (in_array("CAPOEIRA", $GENRE)) { echo 'checked="checked"'; } ?> ><label>CAPOEIRA</label></span>
			</div>
		</div>

		<div id="searchOption" class="formContent">
			<h3>システム</h3>
			<div class="formContent--wd">
				<span><input type="checkbox" name="OPTION[]" value="連盟" <?php if (in_array("連盟加盟", $OPTION)) { echo 'checked="checked"'; } ?>/><label>連盟加盟</label></span>
				<span><input type="checkbox" name="OPTION[]" value="インカレ" <?php if (in_array("インカレ", $OPTION)) { echo 'checked="checked"'; } ?>/><label>インカレ</label></span>
			</div>
		</div>

		<div id="searchSubmit"  class="formContent">
			<input type="submit" value="検索する" />
		</div>
	</form>


	<?php 
	$genreImp  = implode(" , ", $GENRE); 
	$regionImp  = implode(" , ", $REGION); 
	$areaImp  = implode(" , ", $AREA); 
	$optionImp  = implode(" , ", $OPTION); 
	?>
	
	<?php if($GENRE || $REGION || $AREA || $OPTION ): ?>

	<div class="search-result">
		<p><span style="font-size:25px;vertical-align:baseline"><?php echo $wp_query->found_posts ?></span> 件のストリートダンスサークルがヒットしました</p>
		<?php if($genreImp){echo '<p>GENRE : ' . esc_html($genreImp) .'</p>'; }?>
		<?php if($regionImp || $areaImp):?>
			<?php if($regionImp){echo '<p> 地区 : ' . esc_html($regionImp).'</p>'; }?>
			<?php if($areaImp){echo '<p> 地域 : ' . esc_html($areaImp).'</p>'; }?>
		<?php endif;?>
		<?php if($optionImp){echo '<p>オプション : ' . esc_html($optionImp) .'</p>'; }?>
	</div>

	<?php endif;?>

	<div class="pageBx entries cf">
	<?php 
		$cont = 1 ;
		if(have_posts()): while(have_posts()): the_post(); 
		$slug = $post->post_name;
		$costumf  = get_post_custom( $post->ID );
		$region	= $costumf['REGION'];
		if($cont ==1):
	?>
	<div class="main-post">
		<div class="imageWrapper"><div class="image">
				<?php if(has_post_thumbnail()): the_post_thumbnail("medium"); else: echo '<img src="' . get_template_directory_uri().'/images/noimg.jpg" alt="no image">'; endif; ?>
		</div></div>
		<h2 class="entries--title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<?php the_excerpt(); ?>
	</div>
	<ul class="entries--list">
	<?php else: ?>
		<li <?php if($cont > 6): echo 'class="grid__item--3 has-gutter"'; endif;?> >
			<a href="<?php the_permalink(); ?>">
				<div class="imageWrapper"><div class="image">
				<?php if(has_post_thumbnail()): the_post_thumbnail("medium"); else: echo '<img src="' . get_template_directory_uri().'/images/noimg.jpg" alt="no image">'; endif; ?>
				</div></div>	
				<div class="text"><p><?php the_title(); ?></p></div>
			</a>
		</li>
		<?php if($cont == 6): echo '</ul></div><div class="entries"><ul class="grid grid-fill cf" style="margin-top:24px">'; endif; ?>

	<?php endif; $cont++; endwhile; ?>

	</ul>
	</div>

	<div id="wpnav">
		<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	</div>

	<?php endif; wp_reset_query(); ?>



</section>

<?php get_footer(); ?>