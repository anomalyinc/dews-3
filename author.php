<?php 

get_header(); 
the_post(); 

$pageslug = $page->post_name;
$user_data = get_userdata($author);
$page = get_page(get_the_ID());
?>

<section>
	<h1 class="section--title">PROFILE<br><span>プロフィール</span></h1>

	<div id="profDiv">
		<div class="image"><?php echo get_avatar($author, 144); ?></div>
		<div class="profile">
			<table>
				<caption>プロフィール</caption>
				<tr>
					<th>名前</th>
					<td><?php the_author(); ?>
					</td>
				</tr>
				<tr>
					<th>プロフィール</th>
					<td class="description"><?php echo $user_data->description ?><br>
						<div class="author-sns">
            	<?php if(get_the_author_meta('twitter',$post->post_author)): ?>
                <div class="author-sns__item author-sns__item--twitter">
                    <a href="https://twitter.com/<?php the_author_meta( 'twitter', $post->post_author ); ?>" target="_blank">
                        <i class="fab fa-twitter" aria-hidden="true"></i>
                    </a>
                </div>
            	<?php endif; ?>
                <?php if(get_the_author_meta('facebook',$post->post_author)): ?>
                <div class="author-sns__item author-sns__item--facebook">
                    <a href="https://www.facebook.com/<?php the_author_meta( 'facebook', $post->post_author ); ?>" target="_blank">
                        <i class="fab fa-facebook-f" aria-hidden="true"></i>
                    </a>
                </div>
                <?php endif; ?>
                <?php if(get_the_author_meta('Instagram',$post->post_author)): ?>
                 <div class="author-sns__item author-sns__item--instagram">
                    <a href="https://www.instagram.com/<?php the_author_meta( 'Instagram', $post->post_author ); ?>" target="_blank">
                        <i class="fab fa-instagram" aria-hidden="true"></i>
                    </a>
                </div>
                <?php endif; ?>
            </div>
					</td>

				</tr>
			</table>
		</div>
	</div>

</section>

<?php 
	$args = array( 'post_type' => array('post','movie','music','fashionsnap','event','column'), 'posts_per_page' => 12 ,'paged' => get_query_var( 'paged' ), author => $author );
	$my_query = new WP_Query($args); if ($my_query->have_posts()) :
?>
<section class="entries">
	<h2 class="section--title"><?php the_author(); ?>さんの記事</h2>
	<ul class="grid grid-fill cf">
	<?php while($my_query->have_posts()): $my_query->the_post(); ?>
	<li class="grid__item--3 has-gutter">
		<a href="<?php the_permalink(); ?>">

			<div class="imageWrapper"><div class="image">
				<?php if(has_post_thumbnail()): ?>
				<?php the_post_thumbnail("medium"); ?>
				<?php elseif(get_post_type() == 'movie'): ?>
				<img src="http://img.youtube.com/vi/<?php echo esc_html(post_custom('VIDEOID')); ?>/0.jpg" alt="<?php the_title(); ?>" >
				<?php elseif(get_post_type() == 'music'): ?>
				<img src="<?php echo esc_url( home_url( '/' )).'images/music/'.esc_html($post->post_name).'/00.jpg'; ?>" alt="<?php the_title() ?>">
				<?php else: ?>
				<img src="<?php echo get_template_directory_uri(); ?>/images/noimg.jpg" alt="no img">
				<?php endif; ?>
			</div></div>
			<div class="text"><time datetime="<?php the_time("c"); ?>"><?php the_time("Y.m.d"); ?></time><p><?php the_title(); ?></p></div>
		</a>
	</li>
	<?php endwhile; ?>
	</ul>

</section>

<div id="wpnav">
	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(array('query' => $my_query)); } ?>
</div>

<?php endif; wp_reset_postdata(); ?>

<?php get_footer(); ?>