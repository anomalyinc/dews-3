<?php /* Template Name: MOVIE */ ?>
<?php 
get_header(); 
the_post(); 
$pageslug = $page->post_name;
$paged = get_query_var('paged') ? get_query_var('paged') : 1;
?>

<section>
	<h1 class="section--title">PICK UP<br><span><?php the_title(); ?></span></h1>

	<div class="pageBx entries cf">
	<?php 
		$cont = 1 ;
		$args = array( 'post_type' => array('movie'), 'posts_per_page' => 22,'paged' => $paged);
		$my_query = new WP_Query($args); if ($my_query->have_posts()) :
		while($my_query->have_posts()): $my_query->the_post();

		$slug = $post->post_name;
		$costumf  = get_post_custom( $post->ID );
		
		$date = post_custom("DATE");
		$t_date = mb_strimwidth ($date, 0, 10);
		$t_date = explode(".", $t_date);

		if($cont ==1):
	?>
	<div class="main-post">

		<div class="imageWrapper">
			<?php if(get_post_type() == 'movie'): ?>
			<div class="iframeWrap"><iframe width="560" height="315" src="//www.youtube.com/embed/<?php echo esc_html(get_post_meta($post->ID,'VIDEOID',true)); ?>?rel=0" frameborder="0" allowfullscreen></iframe></div>
			<?php else: ?>
			<div class="image"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail("large"); ?></a></div>
			<?php endif; ?>
		</div>
		<time datetime="<?php the_time("c"); ?>"><?php the_time("Y.m.d"); ?></time>
		<h2 class="entries--title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<?php the_excerpt(); ?>
	</div>
	<ul class="entries--list">
	<?php else: ?>
		<li <?php if($cont > 6): echo 'class="grid__item--3 has-gutter"'; endif;?> >
			<a href="<?php the_permalink(); ?>">
				<div class="imageWrapper"><div class="image">
					<?php if(has_post_thumbnail()): ?>
					<?php the_post_thumbnail("medium"); ?>
					<?php elseif(get_post_type() == 'movie'): ?>
					<img src="http://img.youtube.com/vi/<?php echo esc_html(post_custom('VIDEOID')); ?>/0.jpg" alt="<?php the_title(); ?>" >
					<?php else: ?>
					<img src="images/noimg.jpg" alt="<?php the_title(); ?>">
					<?php endif; ?>
				</div></div>	
				<div class="text"><time datetime="<?php the_time("c"); ?>"><?php the_time("Y.m.d"); ?></time><p><?php the_title(); ?></p></div>
			</a>
		</li>
		<?php if($cont == 6): echo '</ul></div><div class="entries"><ul class="grid grid-fill cf" style="margin-top:24px">'; endif; ?>

	<?php endif; $cont++; endwhile; ?>

	</ul>
	<?php endif;  ?>

	</div>

</section>

<div id="wpnav">
	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(array('query' => $my_query)); } ?>
</div>

<?php wp_reset_query(); ?>

<?php get_footer(); ?>