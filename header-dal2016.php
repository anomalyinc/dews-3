<!DOCTYPE HTML>
<html lang="ja">
<head>
<script src="//j.wovn.io/1" data-wovnio="key=9pj5L" async></script>
<meta charset="utf-8">
<meta name = "viewport" content = "width = device-width">
<title><?php wp_title( '|', true, 'right' ); ?></title>

<link rel="shortcut icon" href="<?php echo esc_url( home_url( '/' ) ); ?>images/favicon.ico">
<link rel="apple-touch-icon" href="<?php echo esc_url( home_url( '/' ) ); ?>images/apple-touch-icon.png">
<link type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" >
<link type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/glaceausnap.css" rel="stylesheet" >



<?php
wp_deregister_script('jquery');
wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js', array(), '1.11.2');
wp_enqueue_script('jqueryui', 'http://code.jquery.com/ui/1.10.2/jquery-ui.js', array(), '1.11.1');
?>
<?php wp_head(); ?>

<!--[if lte IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/selectivizr-min.js"></script>
<![endif]-->

<!-- インスタグラムフィード	-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/instafeed.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.simplyscroll.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.simplyscroll.min.js"></script>

<script type="text/javascript">
var userFeed = new Instafeed({
  get: 'tagged', //タグから取得します
  tagName: 'dancealive', //好きなハッシュタグを設定
  accessToken: '1278101127.467ede5.8eda399a564b4792949af44a2cc35257', //アクセストークンを指定
  limit: 10, //取得投稿の上限を設定
  sortBy: "most-liked", //ランダムで並び替え
  template: '<li><a href="{{link}}" target="_blank"><img src="{{image}}" alt="{{caption}}"></a></li>',
  after: function() {
    $('#scroller ul').simplyScroll(); //Instafeed実行後、simplyScrollを実行して横スクロールさせる
  },
});
userFeed.run();
</script>
<!-- インスタグラムフィード	-->

<!-- グーグルコンソール連携-->
<meta name="google-site-verification" content="45safGZijfme1hAY4Wnra0DHhKPsPxee5swXLAdlEuk" />

 <!-- タブアコーディオン -->
    <script>
    $(function() {
        var rwdTab = $('#tabAccordion'),
            switchPoint = 768,
            fadeSpeed = 500,
            slideSpeed = 500;

        var btnElm = rwdTab.children('dl').children('dt'),
            contentsArea = rwdTab.children('dl').children('dd');

        btnElm.on('click', function() {
            if (!$(this).hasClass('btnAcv')) {
                btnElm.removeClass('btnAcv');
                $(this).addClass('btnAcv');

                if (window.innerWidth > switchPoint) {
                    contentsArea.fadeOut(fadeSpeed);
                    $(this).next().fadeIn(fadeSpeed);
                } else {
                    contentsArea.slideUp(slideSpeed);
                    $(this).next().slideDown(slideSpeed);
                }
            }
        });

        btnElm.first().click();
    });
    </script>
    
</head>

<body role="document" itemscope="itemscope" itemtype="http://schema.org/WebPage" <?php if(is_single()){ echo' class="single"'; } ?> >
<div id="fb-root"></div>
<div id="wrapper" <?php if(is_home()) echo 'class="animating"'; ?>>

<!-- CMerスマホタグ-->
<script language="JavaScript" type="text/javascript">
//<![CDATA[
(function() {
  var random = new Date();
  
  var so_src = ('https:' == document.location.protocol ? 'https://ssl.socdm.com' : 'http://tg.socdm.com') +
  '/adsv/v1?posall=Dews150731pv&id=26899&t=js&rnd='+ random.getTime() + '&tp=' + encodeURIComponent(document.location.href) + '&pp=' + encodeURIComponent(document.referrer);
  
   document.write('<sc'+'ript language="JavaScript" type="text/javascript" src="'+so_src+'"></sc'+'ript>');
})();
//]]>
</script>

<header id="header" role="banner" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
	<div class="inner cf">
		<?php if(is_home()): ?>
		<h1 id="logo"><span>ダンスニュースメディアサイト “デュース”</span>
		<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img itemprop="image" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php bloginfo('name'); ?>"></a>
		</h1>
		<?php else: ?>
		<div id="logo"><span>ダンスニュースメディアサイト “デュース”</span>
		<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img itemprop="image" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php bloginfo('name'); ?>"></a>
		</div>
		<?php endif; ?>

		<div id="header__adsense">
		
	<?php 
		wp_list_bookmarks(array(
			'categorize' => 0,
			'title_li' => 0,
			'category_name' => 'toptb',
			'limit' => 1,
			'before' => '<div class="tbhidden">',
			'after' => '</div>',
			'show_images' => true,
			'show_description' => 0,
			'orderby' => 'rand'
		)); 
	?>
	
		
			<?php 
		wp_list_bookmarks(array(
			'categorize' => 0,
			'title_li' => 0,
			'category_name' => 'toppc',
			'limit' => 1,
			'before' => '<div class="pchidden">',
			'after' => '</div>',
			'show_images' => true,
			'show_description' => 0,
			'orderby' => 'rand'
		)); 
	?>
	
		</div>	
		
	</div>

	<div id="header--top" >
		<div class="inner cf">
			<ul class="socialLink tbhidden">
				<li class="fb"><a href="https://www.facebook.com/dews365" target="_blank" rel="nofollow">FACEBOOK</a></li>
				<li class="tw"><a href="https://twitter.com/Dews365" target="_blank" rel="nofollow">TWITTER</a></li>
				<li class="rss"><a href="http://dews365.com/feed" target="_blank">RSS</a></li>
				<li class="insta"><a href="http://instagram.com/dews365" target="_blank" rel="nofollow">INSTAGRAM</a></li>
				<li class="linea"><a href="http://accountpage.line.me/danceatv" target="_blank" rel="nofollow">LINE@</a></li>
			</ul>

			<div id="search" class="sphidden">
				<form method="get" id="searchform" action="<?php bloginfo ('url'); ?>">
			    	<input type="text" value=""  name="s" id="s" />
			   	 	<input type="image" src="<?php echo get_template_directory_uri(); ?>/images/icon-search.png" alt="検索" id="searchsubmit"  value="" />
			    </form>
			</div>

			<ul id="menbernav">
				<li class="beginner sphidden" ><a href="<?php echo esc_url( home_url( '/' ) ); ?>beginner">はじめての方へ</a></li>
				<?php if (!is_user_logged_in()) : ?><li class="sphidden"><a href="<?php echo esc_url( home_url( '/' ) ); ?>member_regist">無料会員登録</a></li><?php endif; ?>
				<?php if (is_user_logged_in()) : global $current_user; get_currentuserinfo();?>
				<li id="login" ><span id="login--btn"><?php echo $current_user->nickname; ?> さん </span>
					<div id="login--wd" class="loginwd">
						<div class="loginwd--hd">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>mypage">
							<?php echo get_avatar( $current_user->id, 30); ?> 
							<em><?php echo $current_user->nickname; ?></em>
							<em>マイページ</em>
							</a>
						</div>
						<span><a href="<?php echo wp_logout_url($_SERVER['REQUEST_URI']); ?>">ログアウトする</a></span>
					</div>
				</li>
				<?php else: ?>
					<li id="login">
					<span id="login--btn">LOGIN</span>
					<div id="login--wd">
					<form id="loginform" method="post" action="<?php echo esc_url( home_url( '/' ) ); ?>mypage">
					<dl>
					<dt>ユーザー名:</dt><dd><input name="log" type="text" id="log" value="" class="username" /></dd>
					<dt>パスワード:</dt><dd><input type="password" name="pwd" id="login_password" value="" /></dd>
					</dl>
					<input name="redirect_to" type="hidden" value="<?php echo esc_attr($_SERVER['REQUEST_URI']) ?>" />
					<input name="a" type="hidden" value="login" />
					<input type="submit" value="ログイン" />
					<?php
					if(function_exists('gianism_login')){
					    gianism_login();
					}
					?>
					<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>mypage?a=pwdreset">パスワードを忘れた場合はこちら</a></p>
					</form>
					</div>

				</li>

				<?php endif; ?>

			</ul>
			
		</div>
	</div>
	
	<nav id="navigation" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
		<ul id="menu" class="inner">
			<li id="newsbtn"><a href="<?php echo esc_url( home_url( '/' ) ); ?>category/news">ニュース</a></li>
			<li id="videobtn"><a href="<?php echo esc_url( home_url( '/' ) ); ?>movie">ダンス動画</a></li>
			<li id="eventbtn"><a href="<?php echo esc_url( home_url( '/' ) ); ?>event">イベントインフォ</a></li>
			<li id="fashionbtn"><a href="<?php echo esc_url( home_url( '/' ) ); ?>fashion">ファッションスナップ</a></li>
			<li id="dewmobtn"><a href="<?php echo esc_url( home_url( '/' ) ); ?>dewmo">Dewmo</a></li>
			<li id="dewzanbtn"><a href="<?php echo esc_url( home_url( '/' ) ); ?>dewzan">Dewzan</a></li>
			<!-- <li id="shopbtn-sp"><a href="http://www.danceashop.com" target="_blank">DANCE@SHOP</a></li> -->
			<li id="shopbtn" class="sphidden"><a href="http://www.danceashop.com" target="_blank"><span class="tbhidden">ダンスの衣装なら </span>DANCE@SHOP</a></li>
		</ul>
	</nav>

</header>

<div id="main">
<div class="inner entries">
<h1 class="section--title"><span class="icn icn-special"><?php the_title(); ?></span></h1>
</div>
<div class="dal2016-top">
<div class="dal2016-logo"><img src="<?php echo get_template_directory_uri(); ?>/images/dal2016-logo.png" alt=""></div>
</div>
<div id="special">
	<div class="inner entries">
<div id="scroller">
  <p><span class="icn-instagram">ハッシュタグ#dancealiveでInstagramにつぶやくと画面上に表示されるのでお試しください！</span>
</p>
  <ul id="instafeed" class="clearfix"></ul>
</div>
			<div class="socialBtn-dal2016">
			<ul>
				<li class="twitter"><a href="https://twitter.com/share" class="twitter-share-button" data-lang="en" data-count="vertical" data-text="" data-url="<?php the_permalink(); ?>" rel="nofollow">ツイート</a></li>
				<li><div class="fb-like" data-href="<?php the_permalink(); ?>" data-send="false" data-layout="button_count" data-show-faces="false" data-colorscheme="light"></div></li>
				<li><div class="fb-share-button" data-href="<?php the_permalink(); ?>" data-layout="button_count"></div></li>
			</ul>
		</div>
	</div>
</div>

<div id="content" class="inner cf">
<main id="mainContent"  role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/WebPageElement">
<div class="" >